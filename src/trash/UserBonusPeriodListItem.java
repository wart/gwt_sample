package trash;

import com.google.gwt.user.client.rpc.IsSerializable;

public class UserBonusPeriodListItem implements IsSerializable
{
	public String id;
	public String name;
	public UserBonusPeriodListItem(){};
	public UserBonusPeriodListItem(String _id, String _name)
	{
		id=_id;
		name=_name;
	};	
}
