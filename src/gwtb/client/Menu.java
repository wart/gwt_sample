package gwtb.client;

import gwtb.client.modules.BonusesUse;
import gwtb.client.modules.BrobPort;
import gwtb.client.modules.Change09;
import gwtb.client.modules.Contacts;
import gwtb.client.modules.CorpContacts;
import gwtb.client.modules.CorpQuestions;
import gwtb.client.modules.DVO;
import gwtb.client.modules.InetTP;
import gwtb.client.modules.Journal;
import gwtb.client.modules.NewsShower;
import gwtb.client.modules.Questions;
import gwtb.client.modules.SearchAbon;
import gwtb.client.modules.block_apus;
import gwtb.client.modules.block_mgmn;
import gwtb.client.modules.call_lg;
import gwtb.client.modules.changePhoneTP;
import gwtb.client.modules.checktexh;
import gwtb.client.modules.cmn_sprvk;
import gwtb.client.modules.corpreport;
import gwtb.client.modules.emailinf;
import gwtb.client.modules.emailinfCORP;
import gwtb.client.modules.inetPB;
import gwtb.client.modules.lkpwd;
import gwtb.client.modules.radiuspwdch;
import gwtb.client.modules.raz_post_usl;
import gwtb.client.modules.tp_calc;

import java.util.TreeMap;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Menu extends DecoratedStackPanel {

  private static TreeMap<String, String> msgs3 = null;
  public static Menu menu = null;
  // =======================================================================
  public static void Auth() {
	menu.Reset(true);
  }
  // =======================================================================
  public static void Run()
  {
  		GSControl.ShowWait();
  		GSControl.GetGS().getMsgList(3, new AsyncCallback<TreeMap<String, String>>()
  		{
  			public void onFailure(Throwable caught)
  			{
  				GSControl.HideWait();
  				Window.alert(caught.getMessage());
  			}
  			public void onSuccess(TreeMap<String, String> res)
  			{
  				msgs3 = res;
  				GSControl.HideWait();
  				Create();
  			}
  		});
  }
  // =======================================================================
  private static Menu Create() {
    if (menu == null) {
      menu = new Menu();
    }
    
    return menu;
  }

  // =======================================================================
  HTML vopr;
  VerticalPanel comm;
  VerticalPanel acc;
  VerticalPanel phn;
  VerticalPanel inet;
  VerticalPanel oper;
  
  VerticalPanel Cfin;
  VerticalPanel Crasx;
  VerticalPanel Copts;
  VerticalPanel Cserv;
  VerticalPanel Ccomm;
  // =======================================================================
  public VerticalPanel invp(){ VerticalPanel v=new VerticalPanel(); v.setSpacing(4);return v;};
  
  
  public Menu() {
    this.setWidth("300px");
    oper = invp();	    acc  = invp();
    phn  = invp();	    inet = invp();    
    comm = invp();	    Cfin = invp();
    Crasx= invp();	    Copts= invp();
    Cserv= invp();	    Ccomm= invp();
    RootPanel.get("Menu_pane").add(this);
    this.Reset(false);
  }
  // =======================================================================
  public void Reset(boolean auth)
  {
	  //if (auth) 
		  this.clear();
	  if (GSControl.IsOperator)	b_oper();
	  b_comm(auth);
	  if (auth)
	  {
		//  if (GSControl.myInfo.IsCorp.equalsIgnoreCase("Y"))
		  {
			 // b_Ccomm();			  
			  b_Copts();
			  b_Crasx();
			  b_Cfin();
			  b_Csrv();
		//  }else{
			  b_phn();
			  b_inet();
			  b_acc();
		  };
	  };
  }
  // =======================================================================
  public void b_comm(boolean auth)
  {
	  addp (comm,"m_oth"        ,"m_oth_icon");
	  addl (comm,"m_oth_news"   ,new ClickHandler() { public void onClick(ClickEvent event) {NewsShower.Run();}});
	  addl (comm,"m_oth_adslyes",new ClickHandler() { public void onClick(ClickEvent event) {checktexh .Run();}});

	  addl (comm,"m_comm_tp_calc",new ClickHandler() { public void onClick(ClickEvent event) {tp_calc.Run();}});	  

	  addlb(comm,msgs3.get("m_comm_oborud"),msgs3.get("m_comm_oborud_link"));
	  addlb(comm,msgs3.get("m_comm_faq"),msgs3.get("m_comm_faq_link"));	  
	  if (auth)
	  {
	//	  if (GSControl.myInfo.IsCorp.equalsIgnoreCase("Y"))
		  {
			  addl(comm,"m_Coth_voprosi"      , new ClickHandler() { public void onClick(ClickEvent event) {CorpQuestions.Run();}});
		//  }else{
			  addl (comm,"m_oth_voprosi",new ClickHandler() { public void onClick(ClickEvent event) {Questions .Run();}});
			  addlb(comm,msgs3.get("m_acc_srv_cap"),GSControl.myInfo.url_lc_asr);
		  }
		  		  
	  }
  };
  // =======================================================================
  public void b_oper()
  {
	  addp(oper,"m_oper","m_oper_icon");
	  addl(oper,"m_oper_search",new ClickHandler() { public void onClick(ClickEvent event) {SearchAbon.Run();}});
  };
  // =======================================================================
  public void b_Cfin()
  {
//	  addp(Cfin , "m_Cfin" , "m_Cfin_icon" );
//	  addl(Cfin , "m_Cfin_reps", new ClickHandler() { public void onClick(ClickEvent event) {corpreport   .Run();}});
  };
  // =======================================================================
  public void b_Crasx()
  {
	  addp(Crasx, "m_Crasx", "m_Crasx_icon");
	  addlb(Crasx,msgs3.get("m_Crasx_srv_stat_cap"),GSControl.myInfo.url_lc_sip);
	  addl(Crasx,"m_Crasx_spravk"      , new ClickHandler() { public void onClick(ClickEvent event) {cmn_sprvk    .Run();}});
//	  addl(Crasx,"m_Crasx_rzpuls"      , new ClickHandler() { public void onClick(ClickEvent event) {raz_post_usl .Run();}});      
	  addl(Crasx,"m_Crasx_dtl_mgn"     , new ClickHandler() { public void onClick(ClickEvent event) {call_lg      .Run();}});      
  };
  // =======================================================================
  public void b_Copts()
  {
	  addp(Copts, "m_Copts", "m_Copts_icon");
	  addl(Copts,"m_acc_jrnl"            , new ClickHandler() {public void onClick(ClickEvent event) {Journal       .Run();}});
	  addl(Copts,"m_Copts_corpcont"    , new ClickHandler() { public void onClick(ClickEvent event) {CorpContacts .Run();}});      
      addl(Copts,"m_Copts_corpemailinf", new ClickHandler() { public void onClick(ClickEvent event) {emailinfCORP .Run();}});
      addl(Copts,"m_Copts_lkpwd"       , new ClickHandler() { public void onClick(ClickEvent event) {lkpwd        .Run();}});
      
  };
  // =======================================================================
  public void b_acc()
  {
	  addp(acc,"m_acc","m_acc_icon"  );
      addl(acc,"m_acc_contact"         , new ClickHandler() {public void onClick(ClickEvent event) {Contacts      .Run();}});
      addl(acc,"m_acc_jrnl"            , new ClickHandler() {public void onClick(ClickEvent event) {Journal       .Run();}});
      addl(acc,"m_acc_einf"            , new ClickHandler() {public void onClick(ClickEvent event) {emailinf      .Run();}});
      addl(acc,"m_acc_bonus"           , new ClickHandler() {public void onClick(ClickEvent event) {BonusesUse    .Run();}});
  };
    // =======================================================================
  public void b_Ccomm()
  {
      addp(Ccomm,"m_Coth", "m_Coth_icon" );
      addl(Ccomm,"m_Coth_voprosi"      , new ClickHandler() { public void onClick(ClickEvent event) {CorpQuestions.Run();}});
  };
  // =======================================================================
  public void b_inet()
  {
	  if (GSControl.myInfo.isInetAbon())
	  {
		  addp(inet,"m_inet","m_inet_icon");
		  if (GSControl.PayBalMode)
		  {
			  addl(inet,"m_inet_PB", new ClickHandler() {public void onClick(ClickEvent event) {inetPB.Run();}});
			  inetPB.Run();
		  }else
		  {
			  addl (inet,"m_inet_TP"  , new ClickHandler() { public void onClick(ClickEvent event) {InetTP     .Run();}});
			  addl (inet,"m_inet_bron", new ClickHandler() { public void onClick(ClickEvent event) {BrobPort   .Run();}});
			  addl (inet,"m_inet_pwd" , new ClickHandler() { public void onClick(ClickEvent event) {radiuspwdch.Run();}});
			  addl (inet,"m_inet_PB"  , new ClickHandler() {public void onClick(ClickEvent event)  {inetPB.Run();}});
			  addlb(inet,msgs3.get("m_inet_srv_stat_cap"),GSControl.myInfo.url_lc_sip);
		  };
	  };
  };
  // =======================================================================
  public void b_phn()
  {
	  if ((!GSControl.PayBalMode)&&(GSControl.myInfo.isPhoneAbon()))
	  {
		  addp(phn, "m_ph" ,"m_ph_icon");
		  if(GSControl.myInfo.havePhones(1))  addl(phn, "m_ph_TP"   , new ClickHandler() {public void onClick(ClickEvent event) {changePhoneTP.Run();}});
//		  if(GSControl.myInfo.havePhones(2))  addl(phn, "m_ph_DVO"  , new ClickHandler() {public void onClick(ClickEvent event) {DVO          .Run();}});
		  addl(phn, "m_ph_09"     , new ClickHandler() {public void onClick(ClickEvent event) {Change09    .Run();}});
	//	  addl(phn, "m_ph_blkaps" , new ClickHandler() {public void onClick(ClickEvent event) {block_apus  .Run();}});
	//	  addl(phn, "m_ph_blkmgmn", new ClickHandler() {public void onClick(ClickEvent event) {block_mgmn  .Run();}});
	  };
  };
  // =======================================================================
  
  
  // =======================================================================
  public void b_Csrv()
  {
	  addp(Cserv,"m_Cserv","m_Cserv_icon"  );
	  if(GSControl.myInfo.havePhones(1))  addl(Cserv, "m_Cserv_ph_TP"   , new ClickHandler() {public void onClick(ClickEvent event) {changePhoneTP.Run();}});
	  if(GSControl.myInfo.isInetAbon())   addl(Cserv,"m_Cserv_inet_TP"  , new ClickHandler() { public void onClick(ClickEvent event) {InetTP     .Run();}});
  }
  // =======================================================================
  
  
  
  
  
  
  
  
  
  
  // =======================================================================
  private String getHeaderString(String text, String imgpath) {
	HorizontalPanel hPanel = new HorizontalPanel();
    hPanel.setSpacing(0);
    hPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
    Image img = new Image();
    img.setUrl(msgs3.get(imgpath));
    hPanel.add(img);
    hPanel.add(new HTML("&nbsp;&nbsp;&nbsp;&nbsp;"));
    HTML headerText = new HTML(msgs3.get(text));
    headerText.setStyleName("cw-StackPanelHeader");
    hPanel.add(headerText);
    return hPanel.getElement().getString();
  }
  // =======================================================================
  public HTML buildLink(String Cap, ClickHandler ch) {
    HTML Link = new HTML("<a href=\"javascript:undefined;\">" + msgs3.get(Cap) + "</a>");
    Link.addClickHandler(ch);
    return Link;
  }
  // =======================================================================
  public HTML buildLinkB(String Cap, String lnk) {
	    HTML Link=new HTML("<a href=\"" + lnk + "\" target=blank>" + Cap+ "</a>") ;
	    return Link;
  }
  // =======================================================================
  public void addp(VerticalPanel vp,String nam,String icn)
  {
	  
	  add(vp, getHeaderString(nam,icn), true);
	  vp.clear();
	  vp.setVisible(true);

  };
  // =======================================================================
  public HTML addl(VerticalPanel vp,String nam,ClickHandler ch)
  {
	  HTML h=buildLink(nam,ch);
	  vp.add(h);
	  return h;
  }
  // =======================================================================
  public HTML addlb(VerticalPanel vp,String nam,String lnk)
  {
	  HTML h=buildLinkB(nam,lnk);
	  vp.add(h);
	  return h;
  }
  // =======================================================================
}
// =========================================================================


/*
 * lp.add(buildLink(msgs3.get("m_acc_pays" ),new ClickHandler(){public void onClick(ClickEvent event)
 * {Window.alert(msgs3.get("m_acc_pays" ));}})); lp.add(buildLink(msgs3.get("m_acc_all" ),new ClickHandler(){public
 * void onClick(ClickEvent event) {Window.alert(msgs3.get("m_acc_all" ));}}));
 * lp.add(buildLink(msgs3.get("m_acc_postoya"),new ClickHandler(){public void onClick(ClickEvent event)
 * {Window.alert(msgs3.get("m_acc_postoya" ));}})); lp.add(buildLink(msgs3.get("m_acc_raz" ),new
 * ClickHandler(){public void onClick(ClickEvent event) {Window.alert(msgs3.get("m_acc_raz" ));}}));
 * lp.add(buildLink(msgs3.get("m_acc_dolg" ),new ClickHandler(){public void onClick(ClickEvent event)
 * {Window.alert(msgs3.get("m_acc_dolg" ));}})); lp.add(buildLink(msgs3.get("m_acc_peni" ),new ClickHandler(){public
 * void onClick(ClickEvent event) {Window.alert(msgs3.get("m_acc_peni" ));}})); /*
 */

// lp.add(buildLink(msgs3.get("m_ph_MTS" ),new ClickHandler(){public void onClick(ClickEvent event)
// {Window.alert(msgs3.get("m_ph_MTS" ));}}));
// lp.add(buildLink(msgs3.get("m_ph_MG_con" ),new ClickHandler(){public void onClick(ClickEvent event)
// {Window.alert(msgs3.get("m_ph_MG_con" ));}}));
