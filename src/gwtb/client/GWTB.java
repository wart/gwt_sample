package gwtb.client;

import gwtb.client.modules.ADM_TRACE;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Cookies;

public class GWTB implements EntryPoint
{
	public void onModuleLoad()
	{
	
		Object[] cs=Cookies.getCookieNames().toArray();
		if (cs!=null)
			for (int i=0;i<cs.length;++i)
				if(cs[i]!=null)
				  Cookies.removeCookie((String)cs[i]);
		GSControl.LoadMsgs1();
		
		if (GSControl.IsAdmTrace)
		{
			ADM_TRACE.Run();	
		}else{
			Menu.Run();
		
		}
	
	};
}


