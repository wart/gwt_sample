package gwtb.client;

import gwtb.client.modules.AuthForm;
import gwtb.client.modules.OperAuthForm;
import gwtb.client.rpcdata.MDModelData;
import gwtb.client.rpcdata.abon_profile;
import gwtb.client.rpcdata.user_phone;
import gwtb.client.rpcdata.wrkitem;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;

public class GSControl {

  private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
  private static MessageBox Waitbox = null;

  public static GreetingServiceAsync GetGS() {
    return greetingService;
  }

  public static abon_profile myInfo = null;
  public static boolean formsmodal = true;
  public static boolean formsanim = false;
  public static boolean JournalNeedReFresh = true;
  public static boolean PayBalMode = false;
  
  public static boolean IsAdmTrace = false;
  public static boolean IsOperator = false;
  public static String  OperatorName = "";
  
  

  public static void SetAbon(abon_profile a)
  {
	  myInfo=a;
	  JournalNeedReFresh=true;
	  Desctop.Update(a);
	  Menu.Auth();
  };
  
  
  
  //public static TreeMap<String, Boolean> stages;
  public static TreeMap<String, String> msgs1;

  public static void ShowWait() {
    if (Waitbox == null) {
      Waitbox = MessageBox.wait("Progress", "Waiting Server Responce...", "wait");
    }
    else {
      Waitbox.show();
    }
  }

  public static void HideWait() {
    if (Waitbox != null) {
      Waitbox.close();
    }
  }

  public static void ShowProcessing() {
    if (Waitbox == null) {
      Waitbox = MessageBox.wait("Progress", "Waiting Server Responce...", "wait");
    }
    else {
      Waitbox.show();
    }
  }

  public static void HideProcessing() {
    if (Waitbox != null) {
      Waitbox.close();
    }
  }

  public static void askdothis(final ClickHandler onyes, ClickHandler onno, String msgg, String dlt) {
    MessageBox.confirm("Confirm", (msgg == null ? msgs1.get("opreq_text") : msgg)
        + (dlt != null ? "<BR><BR>" + dlt : ""), new Listener<MessageBoxEvent>() {
      ClickHandler ch = onyes;

      public void handleEvent(MessageBoxEvent ce) {
        if (ce.getButtonClicked().getText().equals("Yes")) {
          ch.onClick(null);
        }
        // Info.display("MessageBox", "The '{0}' button was pressed", ce.getButtonClicked().getText());
      }
    });
  }

  public static ListBox buildWRKListBox(TreeMap<Integer, wrkitem> wl) {
    if (wl == null)
      return null;
    ListBox lb = new ListBox(false);
    int sz = wl.size();
    for (int j = 0; j < sz; ++j) {
      lb.addItem(wl.get(j).cap);
    }
    return lb;
  }

  
  public static ListBox buildConnListBox() {
	  ListBox lb = new ListBox(false);
	  return buildConnListBox(lb);
  }
  
  public static ListBox buildConnListBox(ListBox lb) {
	  if (lb==null) return null;
	  lb.clear();
    int sz = myInfo.connections.size();
    if (sz <= 0)
      return null;
    for (int j = 0; j < sz; ++j) {
      lb.addItem(myInfo.connections.get(j).connection);
    }
    return lb;
  }
  
  /*
   * 0 full list
   * 1 allow TP Change
   * 2 allow DVO change
   */
  public static ListBox buildPhoneListBox(int mode) {
	  ListBox lb = new ListBox(false);
	  return buildPhoneListBox(lb,mode);
  }
  public static ListBox buildPhoneListBox(ListBox lb,int mode)
  {
	  if (lb==null) return null;
  	  lb.clear();	  
      int sz = myInfo.phones.size();
      if(!myInfo.havePhones(mode)) return null;
	  switch (mode)
	  {
		  case 0:
			  for (int j = 0; j < sz; ++j)
				  lb.addItem(myInfo.phones.get(j).phone);
		      break;
		  case 1:
			  for (int j = 0; j < sz; ++j)
			  {
				  user_phone up=myInfo.phones.get(j);
				  if(up.tp>0)
					lb.addItem(up.phone);
			  };
			  break;
		  case 2:
			  for (int j = 0; j < sz; ++j)
			  {
				  user_phone up=myInfo.phones.get(j);
 				  if(up.allowDVO>0)
					lb.addItem(up.phone);
			  };
			  break;
	  };

    return lb;
  }

  final public static String[] phone_columns(){final String[] rt=MDModelData.AH("phone"); return rt;};
  
  public static ListStore<MDModelData> buildPhoneListBoxLS(int mode)
  {
	  ListStore<MDModelData> ls=new ListStore<MDModelData>();
	  
      int sz = myInfo.phones.size();
      if(!myInfo.havePhones(mode)) return ls;
      user_phone up=null;
	  switch (mode)
	  {
		  case 0:
			  for (int j = 0; j < sz; ++j)
				  if((up=myInfo.phones.get(j))!=null)
					  ls.add(new MDModelData(phone_columns(),up.phone));
		      break;
		  case 1:
			  for (int j = 0; j < sz; ++j)
			  {
				  if((up=myInfo.phones.get(j))!=null)
					  if(up.tp>0)
						  ls.add(new MDModelData(phone_columns(),up.phone));
			  };
			  break;
		  case 2:
			  for (int j = 0; j < sz; ++j)
			  {
				  if((up=myInfo.phones.get(j))!=null)
					  if(up.allowDVO>0)
						  ls.add(new MDModelData(phone_columns(),up.phone));
			  };
			  break;
	  };
	  
	  OkMsg(Integer.toString(ls.getCount()));
    return ls;
  }
  
  
  
  
  
  
  
  public static void LoadMsgs1() {
    GSControl.ShowWait();
    GSControl.GetGS().getMsgList(1, new AsyncCallback<TreeMap<String, String>>() {
      public void onFailure(Throwable caught) {
        GSControl.HideWait();
        MessageBox.alert("Request Error", caught.getMessage(), null);
      }

      public void onSuccess(TreeMap<String, String> res) {
        msgs1 = res;
        if (!IsAdmTrace)
        	if (GSControl.IsOperator)
        	{
        		OperAuthForm.Run();
        	}else
        	{
        		AuthForm.Run();	
        	};
      };
    });
  }

  public static void ErrMsg(String msg) {
    MessageBox.alert(msgs1.get("message_error"), msg, null);
  }

  public static void ErrMsg(Throwable caught) {
    MessageBox.alert(msgs1.get("message_error"), caught.getMessage(), null);
  }

  public static void OkMsg(String msg) {
    MessageBox.alert(msgs1.get("message_title"), msg, null);
  }

}
