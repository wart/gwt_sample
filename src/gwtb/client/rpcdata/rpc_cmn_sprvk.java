package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class rpc_cmn_sprvk  implements IsSerializable
{
	public String f1;
	public String f2;
	public String f3;
	public String f4;
	public int	  frm;
	public rpc_cmn_sprvk(){};
	public rpc_cmn_sprvk(String _f1,String _f2,String _f3,String _f4,
			int _frm)
	{
		f1=_f1;
		f2=_f2;
		f3=_f3;
		f4=_f4;
		frm=_frm;
	};	
}
