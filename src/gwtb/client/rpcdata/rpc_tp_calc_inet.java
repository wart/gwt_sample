package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class rpc_tp_calc_inet implements IsSerializable{
	public String tech;
	public String name;
	public Float cost;
	public Float un_cost;
	public Integer speed;
	public rpc_tp_calc_inet() {};
	public rpc_tp_calc_inet(String _tech,String _name,Float _cost,Float _un_cost,Integer _speed)
	{
		tech=_tech;
		name=_name;
		cost=_cost;
		un_cost=_un_cost;
		speed=_speed;
	}
}
