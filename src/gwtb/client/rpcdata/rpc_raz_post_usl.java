package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class rpc_raz_post_usl  implements IsSerializable
{

	public String dev;
	public String svc;
	public Double summ;
	public int    frm;
	public rpc_raz_post_usl()
	{
		
	}
	
	public rpc_raz_post_usl(String _dev,	String _svc,	Double _summ,int _frm)
	{
		dev=_dev;
		svc=_svc;
		summ=_summ;
		frm=_frm;
	}
	
}
