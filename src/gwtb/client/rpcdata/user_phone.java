package gwtb.client.rpcdata;

import java.util.Vector;

import com.google.gwt.user.client.rpc.IsSerializable;

public class user_phone implements IsSerializable {
  public String phone;
  public int tp;
  public int inbrone;
  public double speed;
  public String addr;
  public int in09;
  public int costed;
  public long allowDVO;
  public int apus_bloked;
  public int mgmn_bloked;

  public Vector<DVOclipart> dvo;

  public user_phone() {};

  public user_phone(String _ph, int _tp, double _sp, int _inbr, String _addr, int _in09, int _costed,int _aDVO,int apus,int mgmn)
      throws ServiceExcept {
    phone = _ph;
    tp = _tp;
    speed = _sp;
    inbrone = _inbr;
    addr = _addr;
    in09 = _in09;
    costed = _costed;
    allowDVO=_aDVO;
    apus_bloked=apus;
    mgmn_bloked=mgmn;
  }

}
