package gwtb.client.rpcdata;

import java.util.Vector;

import com.google.gwt.user.client.rpc.IsSerializable;

public class UserPeriodBonusInfo  implements IsSerializable {
	public Vector<Bonuses> data;
	public String prev; 
	public String used;
	public String Total;
	public Long bid;
	public UserPeriodBonusInfo(){}
	public UserPeriodBonusInfo(Vector<Bonuses> _data,String _prev,String _used,String _Total,Long _bid)
	{
		data=_data;
		prev=_prev;
		used=_used;
		Total=_Total;
		bid=_bid;
	}	
	
	
}
