package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ADM_TRACE_ITEM   implements IsSerializable
{
	public String src;
	public String mess;
	public String note;
	public String date;
	public ADM_TRACE_ITEM(){};
	public ADM_TRACE_ITEM(String src_,String mess_,String note_,String date_)
	{
		src=src_;
		mess=mess_;
		note=note_;
		date=date_;
		
	};
}
