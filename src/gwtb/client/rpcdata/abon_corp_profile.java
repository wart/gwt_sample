package gwtb.client.rpcdata;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

public class abon_corp_profile implements IsSerializable {
	public String f1;
	public String f2;
	public String f3;
	public String d1;
	public String d2;
	public String d3;
	public String e1;
	public String e2;
	public String e3;
	public String p1;
	public String p2;
	public String p3;
	public String x1;
	public String x2;
	public String x3;
	public String m1;
	public String m2;
	public String m3;
	public Date bday;
	public abon_corp_profile(){};
	
	public abon_corp_profile(String _f1,String _f2,String _f3,
	   String _d1,String _d2,String _d3,
	   String _e1,String _e2,String _e3,
	   String _p1,String _p2,String _p3,
	   String _x1,String _x2,String _x3,
	   String _m1,String _m2,String _m3,Date _bday)
	{
		f1=_f1;f2=_f2;f3=_f3;
		d1=_d1;d2=_d2;d3=_d3;
		e1=_e1;e2=_e2;e3=_e3;
		p1=_p1;p2=_p2;p3=_p3;
		x1=_x1;x2=_x2;x3=_x3;bday=_bday;
		m1=_m1;m2=_m2;m3=_m3;
		
	};
}
