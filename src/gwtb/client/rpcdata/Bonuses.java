package gwtb.client.rpcdata;



import com.google.gwt.user.client.rpc.IsSerializable;

public class Bonuses implements IsSerializable
{	public int 	  id;
	public String Name;
	public String Count;
	public String Used;

	public Bonuses() {};
	public Bonuses( int    _id,
			        String _Name,					
					String _Count,
					String _Used
					)
	{
		id=_id;
		Name=_Name;
		Count=_Count;
		Used=_Used;
	}	
}