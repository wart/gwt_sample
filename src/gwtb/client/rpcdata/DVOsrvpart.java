package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;


public class DVOsrvpart implements IsSerializable{
		public int	  id;
		public String dNAME;
		public int	  rsvc_gts;
		public int	  rsvc_sts;
		public int	  csvc_gts;
		public int	  csvc_sts;
		public int	  csvc_gtsnm;
		public int	  csvc_stsnm;
		public  DVOsrvpart(){};
		
		public  DVOsrvpart(int _id,String name,int rg,int rs,int cg,int cs,int cgn, int csn)
		{
			id=_id;
			dNAME=name;
			rsvc_gts=rg;
			rsvc_sts=rs;
			csvc_gts=cg;
			csvc_sts=cs;
			csvc_gtsnm=cgn;
			csvc_stsnm=csn;
		};
		
		
}
