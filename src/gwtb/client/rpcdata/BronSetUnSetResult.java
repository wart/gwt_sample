package gwtb.client.rpcdata;

import java.util.TreeMap;

import com.google.gwt.user.client.rpc.IsSerializable;


public class BronSetUnSetResult implements IsSerializable{
	public int r1;
	public int r2;
	public String retdesc;
	public BronSetUnSetResult(){r1=0;r2=0;retdesc=null;//d1=null;d2=null;
	
	}
	//--------------------------------------------------------------------------------
	public BronSetUnSetResult(int int1, int int2) {
		r1=int1;
		r2=int2;
	};
	//--------------------------------------------------------------------------------
	public void FillDescr(TreeMap<Integer, String> errmlst,int mode)
	{
		// 1 - set
		// 0 or other - unset
		String d1="",d2="";
		int j=0;
		if(r1<0){d1=(errmlst!=null)?errmlst.get(r1):"";j++;};
		if(r2<0){d2=(errmlst!=null)?errmlst.get(r2):"";j++;};
		if(j>0)
		{
			retdesc=d1+" "+d2;
		};
	};
	//--------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------------