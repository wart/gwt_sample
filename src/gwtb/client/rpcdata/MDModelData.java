package gwtb.client.rpcdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import com.extjs.gxt.ui.client.data.ModelData;
import com.google.gwt.user.client.rpc.IsSerializable;

public class MDModelData implements ModelData,IsSerializable
{
	private Object[]data;
	private String[]headers;
	/*
	public MDModelData(Object [] h,Object [] d)
	{
		headers=h;
		data=d;
	}/**/
	public MDModelData(String [] h,Object ... d)
	{
		headers=h;
		data=d;
	}	
	public MDModelData()
	{
		
	}
	public static Object[] AR(Object... args)
	{
		return args;
	}
	public static String[] AH(String... args)
	{
		return args;
	}	
	public void setData(Object ...args)
	{
		if(args!=null)
		{
			data=args;
		}
	}
	
	private int getidx(String S)
	{
		if ((S!=null)&(S!="")&(headers!=null))
		{
			int sz=headers.length;
			for (int i=0;i<sz;++i)
			{
				if (headers[i].equals(S))
				{
					return i;
				}
			};
		};
		return -1;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <X> X get(String property) {
		int i=getidx(property);
		if((i>=0)&(data!=null)&(data.length>i))
		{
			return (X)data[i];
		}
		return null;
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> m= new HashMap<String, Object>();
		if ((data!=null)&(headers!=null)&(headers.length==data.length))
		{
			int sz=headers.length;
			for (int i=0;i<sz;++i)
			{
				m.put((String)headers[i], data[i]);		
			};
		}
		return m;
	}

	@SuppressWarnings("serial")
	@Override
	public Collection<String> getPropertyNames() {
		Collection<String> c =  new ArrayList<String>(){
		};
		
		if(headers!=null)
		{
			int sz=headers.length;
			for (int i=0;i<sz;++i)
			{
				c.add((String)headers[i]);		
			}
		}
		return c;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <X> X remove(String property) {
		Object s;
		int i=getidx(property);
		if((i>=0)&(data!=null)&(data.length>i))
		{
			s=data[i];
			data[i]=null;
			return (X)s;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <X> X set(String property, X value) {
		Object s;
		int i=getidx(property);
		if((i>=0)&(data!=null)&(data.length>i))
		{
			s=data[i];
			data[i]=value;
			return (X)s;
		}
		return null;
	}

}
