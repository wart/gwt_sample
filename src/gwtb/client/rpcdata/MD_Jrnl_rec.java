package gwtb.client.rpcdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import com.extjs.gxt.ui.client.data.ModelData;
public class MD_Jrnl_rec implements ModelData{
	public String dt;
	public String dev;
	public String type;
	public String prms;
	public String ret;
	
	
	public MD_Jrnl_rec() {};
	public MD_Jrnl_rec(String _dt,
					String _type,
					String _dev,					
					String _prms,
					String _ret
					)
	{
		dt=_dt;
		dev=_dev;
		type=_type;
		prms=_prms;
		ret=_ret;
	}
	@SuppressWarnings("unchecked")
	@Override
	public <X> X get(String property) {
		if (property.equals("date")){ return (X)dt;};
		if (property.equals("device")){ return (X)dev;};
		if (property.equals("type")){ return (X) type;};
		if (property.equals("params")){ return (X)prms;};
		if (property.equals("return")){ return (X)ret;};
		
		return null;
	}
	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> m= new HashMap<String, Object>();
		m.put("date", dt);
		m.put("device", dev);
		m.put("type", type);
		m.put("params", prms);
		m.put("return", ret);
		return m;
	}
	@Override
	public Collection<String> getPropertyNames() {
		Collection<String> c =  new ArrayList<String>();
		c.add("date");
		c.add("device");
		c.add("type");
		c.add("params");
		c.add("return");
		return c;
	}
	@SuppressWarnings("unchecked")
	@Override
	public <X> X remove(String property) {
		String s;
		if (property.equals("date")){  s=dt;dt=null;return (X)s;};
		if (property.equals("device")){s=dev;dev=null;return (X)s;};
		if (property.equals("type")){ s=type;type=null;return (X)s;};
		if (property.equals("params")){s=prms; prms=null;return (X)s;};
		if (property.equals("return")){s=ret; ret=null;return (X)s;};		
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public <X> X set(String property, X value) {
		String s;
		if (property.equals("date"))  {s=dt;dt=(String)value;return (X)s;};
		if (property.equals("device")){s=dev;dev=(String)value;return (X)s;};
		if (property.equals("type"))  {s=type;type=(String)value;return (X)s;};
		if (property.equals("params")){s=prms; prms=(String)value;return (X)s;};
		if (property.equals("return")){s=ret; ret=(String)value;return (X)s;};		
		return null;
	};
}
