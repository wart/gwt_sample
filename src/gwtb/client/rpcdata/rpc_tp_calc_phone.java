package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class rpc_tp_calc_phone implements IsSerializable{
	public String categor;
	public String name;
	public Float cost;
	public Float un_cost;
	public int	typ;
	public rpc_tp_calc_phone() {};
	public rpc_tp_calc_phone(String _categor,String _name,Float _cost,Float _un_cost,int _typ)
	{
		categor=_categor;
		name=_name;
		cost=_cost;
		un_cost=_un_cost;
		typ=_typ;
	}
}
