package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class period_call_l   implements IsSerializable
{
	public Long   dev ;
	public String tp  ;
	public Long   cnt ;
	public String len ;
	public Double summ;
	
	public period_call_l(){};
	public period_call_l(Long   _dev,String _tp,Long   _cnt,String _len,Double _summ)
	{
		dev=_dev;
		tp=_tp;
		cnt=_cnt;
		len=_len;
		summ=_summ;
	};
}
