package gwtb.client.rpcdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.extjs.gxt.ui.client.data.ModelData;

public class MD_search_userData implements ModelData{

	public Long user_id;
	public String account;
	public String name;
	public Long dev_id;
	
	public MD_search_userData(){};
	public MD_search_userData(Long _user_id,String _account,String _name,Long _dev_id)
	{
		user_id=_user_id;
		account=_account;
		name=_name;
		dev_id=_dev_id;
	};
	
	@SuppressWarnings("unchecked")
	@Override
	public <X> X get(String arg0) {
		if (arg0.equals("user_id")){ return (X)user_id;};
		if (arg0.equals("account")){ return (X)account;};
		if (arg0.equals("name")){ return (X) name;};
		if (arg0.equals("dev_id")){ return (X)dev_id;};

		return null;
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> m= new HashMap<String, Object>();
		m.put("user_id", user_id);
		m.put("account", account);
		m.put("name", name);
		m.put("dev_id", dev_id);
		return m;
	}

	@Override
	public Collection<String> getPropertyNames() {
		Collection<String> c =  new ArrayList<String>();
		c.add("user_id");
		c.add("account");
		c.add("name");
		c.add("dev_id");
		return c;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <X> X remove(String arg0) {
		String s;Long g;
		if (arg0.equals("user_id")){  g=user_id;user_id=null;return (X)g;};
		if (arg0.equals("account")){s=account;account=null;return (X)s;};
		if (arg0.equals("name")){ s=name;name=null;return (X)s;};
		if (arg0.equals("dev_id")){g=dev_id; dev_id=null;return (X)g;};
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <X> X set(String arg0, X arg1) {
		String s;Long g;
		if (arg0.equals("user_id"))  {g=user_id;user_id=(Long)arg1;return (X)g;};
		if (arg0.equals("account")){s=account;account=(String)arg1;return (X)s;};
		if (arg0.equals("name"))  {s=name;name=(String)arg1;return (X)s;};
		if (arg0.equals("dev_id")){g=dev_id; dev_id=(Long)arg1;return (X)g;};
		return null;
	}

}
