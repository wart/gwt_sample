package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class search_userData implements IsSerializable{
	public Long user_id;
	public String account;
	public String name;
	public Long dev_id;
	
	public search_userData(){};
	public search_userData(Long _user_id,String _account,String _name,Long _dev_id)
	{
		user_id=_user_id;
		account=_account;
		name=_name;
		dev_id=_dev_id;
	};
	
}
