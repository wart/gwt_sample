package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.user.client.ui.ListBox;

import gwtb.client.GSControl;

import java.util.Date;
import java.util.TreeMap;
import java.util.Vector;

public class abon_profile implements IsSerializable  {
	public Long usr_id;
    public Long usr_id_sip;
    public String name;
    public String account;
    public String addr;
    public Float bal;
    public String email;
    public String smsphone;
    public String voicephone;
    public Integer sms_on;
    public Integer email_on;
    public Integer voice_on;
    public Integer afert_on;
    public String date_today;
    public String date_tomorrow;
    public String date_addmonth;
    public String url_lc_sip;
    public String url_lc_asr;
    public Integer einf_bo; 
    public Integer einf_schet; 
    public String IsCorp;
    public Date bday;
    public TreeMap<Integer, user_connection> connections;
	public TreeMap<Integer, user_phone> phones; 
	public abon_corp_profile corpData;
	public Vector<BillPeriod> billData;
	//--------------------------------------------------------------------------------	
    public abon_profile(){};
    public abon_profile(Long uid,Long sipuid,String nm,String adr,//String devid,
    		String eml,Float b,String sms,Integer smson,Integer emailon,String acc,String vp,Integer vpon,Integer afon,
    		String today,String tomorrow,String addmonth,Integer _einf_bo,Integer _einf_schet,String _IsCorp,Date _bday )
    {
        usr_id=uid;
        usr_id_sip=sipuid;
        name=nm;
        addr=adr;
        email=eml;
        bal=b;
        smsphone=sms;
        sms_on=smson;
        email_on=emailon;
        connections=null;
        phones=null;
        account=acc;
        voicephone=vp;
        voice_on=vpon;
        afert_on=afon;
        date_today   =today;
        date_tomorrow=tomorrow;
        date_addmonth=addmonth;
        einf_bo=_einf_bo;
        einf_schet=_einf_schet;
        IsCorp=_IsCorp;
        bday=_bday;
        
        url_lc_sip=null;
        url_lc_asr=null;
        corpData=null;
        billData=null;
    }
	//--------------------------------------------------------------------------------	    
	public boolean canSendQuestion() {
		  return (((email!=null)&&(email.length()>7)&&(email_on!=null)&&(email_on==1))||
		  ((smsphone!=null)&&(smsphone.length()==10)&&(sms_on!=null)&&(sms_on==1)))? true:false;
	};
	//--------------------------------------------------------------------------------
	private boolean cDi(String s1) {return (s1!=null)&(s1.length()>5);};
	private boolean cD(String s1,String s2,String s3){return cDi(s1)&cDi(s2)&cDi(s3);};
	public boolean CorpCanSendQuestion(int u)
	{
		if (corpData!=null)
			switch(u)
			{
				case 1: return cD(corpData.f1,corpData.e1,corpData.p1);
				case 2: return cD(corpData.f2,corpData.e2,corpData.p2);
				case 3: return cD(corpData.f3,corpData.e3,corpData.p3);
			};
		return false;
	}
	//--------------------------------------------------------------------------------	
	public boolean isInetAbon()
	{
		boolean ret=false;
		if (connections!=null) ret=connections.size()>0;
		return ret;
	};
	//--------------------------------------------------------------------------------	
	public boolean isPhoneAbon()
	{
		return this.havePhones(0);
	};
	//--------------------------------------------------------------------------------	
	public BillPeriod getBillPeriodById(Long bid)
	{
		if (billData!=null)
		{
			int sz=billData.size();
			for (int i =0;i<sz;++i)
			{
				BillPeriod bp=billData.get(i);
				if(bp!=null)
				{
					if (bp.id.compareTo(bid)==0)
						return bp;
				}
			}
		}
		return null;
	}
	//--------------------------------------------------------------------------------	
	public BillPeriod getBillPeriodByName(String bname)
	{
		if (billData!=null)
		{
			int sz=billData.size();
			for (int i =0;i<sz;++i)
			{
				BillPeriod bp=billData.get(i);
				if(bp!=null)
				{
					if (bp.name.equalsIgnoreCase(bname))
						return bp;
				}
			}
		}
		return null;
	}	
	//--------------------------------------------------------------------------------
	public ListBox CreateBillPerListBox(ListBox lb,boolean onlyBonused)
	{
		ListBox wlb=(lb!=null)?lb:new ListBox(false);
		if (billData!=null)
		{
			wlb.clear();
			int sz=billData.size();
			for (int i =0;i<sz;++i)
			{
				BillPeriod bp=billData.get(i);
				if(bp!=null)
				{
					if ((!onlyBonused)||((onlyBonused)&(bp.isBonused==1)))
						wlb.addItem(bp.name);
				}
			};
			return wlb;
		};
		return null;
	}
  	//--------------------------------------------------------------------------------
	  /*
	   * 0 full list
	   * 1 allow TP Change
	   * 2 allow DVO change
	   */	
	public boolean havePhones(int mode)
	{
		if (phones!=null)
		{
			int sz = phones.size();
			switch (mode)
			{
				case 0: return (sz>0);
				case 1:
					  for (int j = 0; j < sz; ++j)
						  	if(phones.get(j).tp>0) return true;
					  break;
				case 2:
					  for (int j = 0; j < sz; ++j)
						  	if(phones.get(j).allowDVO>0) return true;
					  break;					  
			};
		};
		 return false;
	}
  	//--------------------------------------------------------------------------------
	public boolean BlokedAPUS(String phn)
	{
		if (phones!=null)
		{
			user_phone up=null;
			int sz = phones.size();
			for (int j = 0; j < sz; ++j)
			{
				up=phones.get(j);
				if(up.phone.equalsIgnoreCase(phn))
					return up.apus_bloked>0;
			}
		};
		return false;
	}
  	//--------------------------------------------------------------------------------
	public boolean BlokedMGMN(String phn)
	{
		if (phones!=null)
		{
			user_phone up=null;
			int sz = phones.size();
			for (int j = 0; j < sz; ++j)
			{
				up=phones.get(j);
				if(up.phone.equalsIgnoreCase(phn))
					return up.mgmn_bloked>0;
			}
		};
		return false;
	}
  	//--------------------------------------------------------------------------------
	
	
}


