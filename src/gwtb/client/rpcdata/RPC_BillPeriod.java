package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class RPC_BillPeriod implements IsSerializable
{
	public Long id;
	public String name;
	public int isBonused;
	public RPC_BillPeriod()
	{
		
	}
	public RPC_BillPeriod(Long _id,String _name,int _isBonused)
	{
		id=_id;
		name=_name;
		isBonused=_isBonused;
	}
}
