package gwtb.client.rpcdata;


import com.extjs.gxt.ui.client.store.GroupingStore;

public class MDUserPeriodBonusInfo {
	public GroupingStore<MD_Bonuses> data;
	public String prev; 
	public String used;
	public String Total;
	public MDUserPeriodBonusInfo(){}
	public MDUserPeriodBonusInfo(GroupingStore<MD_Bonuses> _data,String _prev,String _used,String _Total)
	{
		data=_data;
		prev=_prev;
		used=_used;
		Total=_Total;
	}
	public static MDUserPeriodBonusInfo Genereate(UserPeriodBonusInfo src)
	{
		MDUserPeriodBonusInfo dst= new MDUserPeriodBonusInfo();
		dst.prev=src.prev;
		dst.used=src.used;
		dst.Total=src.Total;
		if (src.data!=null)
		{
			dst.data= new GroupingStore<MD_Bonuses>();
			dst.data.groupBy("bid");
			int sz=src.data.size();
			int j;
			Bonuses b;
			for (j=0;j<sz;++j)
			{
				b= src.data.get(j);
				
				if (b!=null)
					dst.data.add(new  MD_Bonuses(b.id,b.Name,b.Count,b.Used,src.bid));
			};
			
		};
		return dst;
	}
}
