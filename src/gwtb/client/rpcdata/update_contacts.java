package gwtb.client.rpcdata;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;


public class update_contacts implements IsSerializable  {
	public String email;
	public String smsphone;
	public String voicephone;
	public Integer sms_on;
	public Integer email_on;
	public Integer voice_on;
	public Integer aferta_on;
	public Date   bday;
	
	 public update_contacts(){};
	 public update_contacts(String em,String ph,String phv,Integer son,Integer eon,Integer pon,
			 Integer afon,Date   _bday)
	 {
		 email=em;
		 smsphone=ph;
		 voicephone=phv;
		 sms_on=son;
		 email_on=eon;
		 voice_on=pon;
		 aferta_on=afon;
		 bday=_bday;
	 };
}
