package gwtb.client.rpcdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.extjs.gxt.ui.client.data.ModelData;

public class MD_DVOclipart  implements ModelData
{
	public int ID;
	public String NAME;
	public int ISON;
	public int SRVID;
	public boolean chst;
	public boolean chst_old;
	public String chdate;
	public  MD_DVOclipart(){};
	public  MD_DVOclipart(int _ID,String _NAME,int _ISON,	int _SRVID,int _chst,String _chdate)
	{
		ID    = _ID;
		NAME  = _NAME;
		ISON  = _ISON;
		SRVID = _SRVID;
		switch (_chst)
		{
			case 1: chst= false;break;
			case 2: chst= true;break;
			default:chst= (SRVID!=0)?true:false;
	    }
		chst_old=chst;
		chdate=_chdate;
	};

	
	@SuppressWarnings("unchecked")
	@Override
	public <X> X get(String property) {
		if (property.equals("ID")){ return (X)new Integer(ID);};
		if (property.equals("NAME")){ return (X)NAME;};
		if (property.equals("ISON")){ return (X) new Integer(ISON);};
		if (property.equals("SRVID")){ return (X)new Integer(SRVID);};
		if (property.equals("chst")){ return (X) new Boolean(chst);};
		if (property.equals("chst_old")){ return (X) new Boolean(chst_old);};
		if (property.equals("chdate")){ return (X)chdate;};
		return null;
	}
	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> m= new HashMap<String, Object>();
		m.put("ID", ID);
		m.put("NAME", NAME);
		m.put("ISON", ISON);
		m.put("SRVID", SRVID);
		m.put("chst", chst);
		m.put("chst_old", chst_old);
		
		m.put("chdate", chdate);
		return m;
	}
	@Override
	public Collection<String> getPropertyNames() {
		Collection<String> c =  new ArrayList<String>();
		c.add("ID");
		c.add("NAME");
		c.add("ISON");
		c.add("SRVID");
		c.add("chst");
		c.add("chst_old");
		
		c.add("chdate");
		return c;
	}
	@SuppressWarnings("unchecked")
	@Override
	public <X> X remove(String property) {
		String s;Integer i;Boolean b;
		if (property.equals("ID")    ){i=new Integer(ID)   ;ID=0     ;return (X)i;};
		if (property.equals("NAME")  ){s=NAME              ;NAME=null;return (X)s;};
		if (property.equals("ISON")  ){i=new Integer(ISON) ;ISON=0   ;return (X)i;};
		if (property.equals("SRVID") ){i=new Integer(SRVID);SRVID=0  ;return (X)i;};
		if (property.equals("chst")  ){b=new Boolean(chst) ;chst=false   ;return (X)b;};
		if (property.equals("chst_old")  ){b=new Boolean(chst_old) ;chst_old=false   ;return (X)b;};
				
		if (property.equals("chdate")){s=chdate            ;chdate=null;return (X)s;};
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public <X> X set(String property, X value) {
		String s;Integer i;Boolean b;
		if (property.equals("ID")    ){i=new Integer(ID)   ;ID=(Integer)value;return (X)i;};
		if (property.equals("NAME")  ){s=NAME              ;NAME=(String)value;return (X)s;};
		if (property.equals("ISON")  ){i=new Integer(ISON) ;ISON=(Integer)value;return (X)i;};
		if (property.equals("SRVID") ){i=new Integer(SRVID);SRVID=(Integer)value;return (X)i;};
		if (property.equals("chst")  ){b=new Boolean(chst) ;chst=(Boolean)value;return (X)b;};
		
		if (property.equals("chst_old")  ){b=new Boolean(chst_old) ;chst_old=(Boolean)value;return (X)b;};
		
		if (property.equals("chdate")){s=chdate            ;chdate=(String)value;return (X)s;};
		return null;
	};		
	
}
