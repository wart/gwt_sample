package gwtb.client.rpcdata;

import java.util.Vector;

import com.extjs.gxt.ui.client.store.ListStore;

public class BillPeriod 
{
	public Long id;
	public String name;
	public int isBonused;
	
	public MDUserPeriodBonusInfo  bonusinf;

	public ListStore<MDModelData> raz_svc;
	public ListStore<MDModelData> cnst_svc;
	
	public ListStore<MDModelData> sld;
	public ListStore<MDModelData> nach;
	public ListStore<MDModelData> pay;
	
	public ListStore<MDModelData> call_l;
	public ListStore<MDModelData> call_g;
	public double				  raz_total;
	public double				  const_total;

	//============================================================================//
/*
	final static public String[] sld_heads=(String[])MDModelData.AR("usl","nach","nds","total");
	final static public String[] pay_heads=(String[])MDModelData.AR("date","summ","note");
	final static public String[] nach_heads=(String[])MDModelData.AR("usl","nach","nds","total");
	
	final static public String[] cnst_raz_svc_heads=(String[])MDModelData.AR("dev","svc","summ");
	
	final static public String[] call_l_heads=(String[])MDModelData.AR("dev","tp","cnt","len","summ");
	final static public String[] call_g_heads=(String[])MDModelData.AR("dev","cod","mgmn","dest","datatime","len","summ");                         
/**/
	//============================================================================//
	
	final public static String[] sld_heads         (){final String[] rt=MDModelData.AH("usl","nach","nds","total"); return rt;};
	final public static String[] pay_heads         (){final String[] rt=MDModelData.AH("date","summ","note"); return rt;};
	final public static String[] nach_heads        (){final String[] rt=MDModelData.AH("usl","nach","nds","total"); return rt;};
	final public static String[] cnst_raz_svc_heads(){final String[] rt=MDModelData.AH("dev","svc","summ"); return rt;};
	final public static String[] call_l_heads      (){final String[] rt=MDModelData.AH("dev","tp","cnt","len","summ"); return rt;};
	final public static String[] call_g_heads      (){final String[] rt=MDModelData.AH("dev","cod","mgmn","dest","datatime","len","summ"); return rt;};
	
	
	private void initZero()
	{
		raz_svc=null;
		cnst_svc=null;
		sld=null;
		nach=null;
		pay=null;
		bonusinf=null;
	}
	//============================================================================//
	public BillPeriod()
	{
		initZero();
	};
	//============================================================================//
	public BillPeriod(Long _id,String _name,int _isBonused)
	{
		initZero();
		id=_id;
		name=_name;
		isBonused=_isBonused;
	}
	//============================================================================//	
	public static Vector<BillPeriod> convert(Vector<RPC_BillPeriod> src)
	{
		if(src!=null)
		{
			int sz=src.size();
			if (sz>0)
			{
				Vector<BillPeriod> dst=new Vector<BillPeriod>();
				for(int i=0;i<sz;++i)
				{
					RPC_BillPeriod s=src.get(i);
					if (s!=null)
						dst.add(new BillPeriod(s.id,s.name,s.isBonused));
				};
				return dst;
			}
		}
		return null;
	}
	//============================================================================//
	public void fill(Vector<rpc_cmn_sprvk> res){
		sld= new ListStore<MDModelData>();
		nach=new ListStore<MDModelData>();
		pay=new ListStore<MDModelData>();
		
		int sz=res.size();
		for (int i=0;i<sz;++i)
		{
			rpc_cmn_sprvk r= res.get(i);
			if (r!=null)
			{
				switch (r.frm)
				{
					case 0: sld .add(new MDModelData(sld_heads() ,r.f1,r.f2,r.f3,r.f4));break;
					case 1: nach.add(new MDModelData(nach_heads(),r.f1,r.f2,r.f3,r.f4));break;
					case 2: pay .add(new MDModelData(pay_heads() ,r.f1,r.f2,r.f3));break;
				}
			}
		};
	};
	//============================================================================//
	public void fill_SVC(Vector<rpc_raz_post_usl> res)
	{
		raz_svc= new ListStore<MDModelData>();
		cnst_svc=new ListStore<MDModelData>();
		raz_total=0;
		const_total=0;
		int sz=res.size();
		for (int i=0;i<sz;++i)
		{
			rpc_raz_post_usl r= res.get(i);
			if (r!=null)
			{
				switch (r.frm)
				{
					case 0: raz_svc .add(new MDModelData(cnst_raz_svc_heads(),r.dev,r.svc,r.summ));raz_total+=r.summ;break;
					case 1: cnst_svc.add(new MDModelData(cnst_raz_svc_heads(),r.dev,r.svc,r.summ));const_total+=r.summ;break;
				};
			}
		}
	}
	//============================================================================//	
	public void fill_calls(period_calls_lg_data res)
	{
		
		call_l=new ListStore<MDModelData>();
		call_g=new ListStore<MDModelData>();
		if (res!=null)
		{
			if(res.locals!=null)
			{
				int sz=res.locals.size();
				for (int i=0;i<sz;++i)
				{
					period_call_l r= res.locals.get(i);
					if (r!=null)
						call_l.add(new MDModelData(call_l_heads(),r.dev,r.tp,r.cnt,r.len,r.summ));
				}
			};
			if(res.globals!=null)
			{
				int sz=res.globals.size();
				for (int i=0;i<sz;++i)
				{
					period_call_g r= res.globals.get(i);
					if (r!=null)
						call_g.add(new MDModelData(call_g_heads(),r.dev,r.cod,r.mgmn,r.dest,r.date,r.len,r.summ));
				}
			};			
		}
	}
	//============================================================================//	

	//============================================================================//	

	
	
	

	
	
	
	
	
	
	
	
	
}
