package gwtb.client.rpcdata;

import com.extjs.gxt.ui.client.data.ModelData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
public class MD_ADM_TRACE_ITEM implements ModelData{
	public String src;
	public String mess;
	public String note;
	public String date;
	public MD_ADM_TRACE_ITEM(){};
	public MD_ADM_TRACE_ITEM(String src_,String mess_,String note_,String date_)
	{
		src=src_;
		mess=mess_;
		note=note_;
		date=date_;
		
	};
	public MD_ADM_TRACE_ITEM(ADM_TRACE_ITEM it)
	{
		src=it.src;
		mess=it.mess;
		note=it.note;
		date=it.date;
		
	};	
	@SuppressWarnings("unchecked")
	@Override
	public <X> X get(String property) {
		if (property.equals("src")){ return (X)src;};
		if (property.equals("mess")){ return (X)mess;};
		if (property.equals("note")){ return (X)note;};
		if (property.equals("date")){ return (X)date;};
		return null;
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> m= new HashMap<String, Object>();
		m.put("src", src);
		m.put("mess", mess);
		m.put("note", note);
		m.put("date", date);
		return m;
	}

	@Override
	public Collection<String> getPropertyNames() {
		Collection<String> c =  new ArrayList<String>();
		c.add("src");
		c.add("mess");
		c.add("note");
		c.add("date");
		return c;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <X> X remove(String property) {
		String s;
		if (property.equals("src")){  s=src;src=null;return (X)s;};
		if (property.equals("mess")){s=mess;mess=null;return (X)s;};
		if (property.equals("note")){ s=note;note=null;return (X)s;};
		if (property.equals("date")){s=date; date=null;return (X)s;};
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <X> X set(String property, X value) {
		String s;
		if (property.equals("src"))  {s=src;src=(String)value;return (X)s;};
		if (property.equals("mess")){s=mess;mess=(String)value;return (X)s;};
		if (property.equals("note"))  {s=note;note=(String)value;return (X)s;};
		if (property.equals("date")){s=date; date=(String)value;return (X)s;};
		return null;
	}

}
