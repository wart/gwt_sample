package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;


public class DVOclipart  implements IsSerializable{
	public int ID;
	public String NAME;
	public int ISON;
	public int SRVID;
	public int chst;
	public String chdate;
	
	public  DVOclipart(){};
	public  DVOclipart(int _ID,String _NAME,int _ISON,	int _SRVID,int _chst,String _chdate)
	{
		ID    = _ID;
		NAME  = _NAME;
		ISON  = _ISON;
		SRVID = _SRVID;
		chst = _chst;	
		chdate=_chdate;
	};
	public boolean getLastState()
	{
		switch (chst)
		{
			case 1: return false;
			case 2: return true;
	    }
		return (SRVID!=0)?true:false;
	};
}
