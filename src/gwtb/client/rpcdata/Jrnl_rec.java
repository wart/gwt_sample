package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;


public class Jrnl_rec implements IsSerializable{
	public String dt;
	public String dev;
	public int type;
	public String prms;
	public String ret;
	
	
	public Jrnl_rec() {};
	public Jrnl_rec(String _dt,
					int _type,
					String _dev,					
					String _prms,
					String _ret
					)
	{
		dt=_dt;
		dev=_dev;
		type=_type;
		prms=_prms;
		ret=_ret;
	}
}
