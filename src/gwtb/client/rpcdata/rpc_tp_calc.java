package gwtb.client.rpcdata;
import java.util.Vector;

import com.google.gwt.user.client.rpc.IsSerializable;

public class rpc_tp_calc   implements IsSerializable
{
	public Vector<rpc_tp_calc_phone> fphone;
	public Vector<rpc_tp_calc_inet> finet;
	public Vector<rpc_tp_calc_iptv> fiptv;
	public Vector<rpc_tp_calc_iptv_pack> fiptv_pack;
	public rpc_tp_calc()
	{
		fphone     = null;	
		finet      = null;	
		fiptv      = null;	
		fiptv_pack = null;	
		/*
		fphone     = new Vector<phone>();	
		finet      = new Vector<inet>();	
		fiptv      = new Vector<iptv>();	
		fiptv_pack = new Vector<iptv_pack>();	
		/**/
	}
}

