package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class period_call_g   implements IsSerializable
{
	public Long   dev ;
	public String cod ;
	public String mgmn;
	public String dest;
	public String date;
	public String len ;
	public Double summ;	
	public period_call_g(){};
	public period_call_g(Long   _dev,String _cod,String _mgmn,String _dest,String _date,String _len,Double _summ)
	{
		dev=_dev;
		cod=_cod;
		mgmn=_mgmn;
		dest=_dest;
		date=_date;
		len=_len;
		summ=_summ;
	};
}
