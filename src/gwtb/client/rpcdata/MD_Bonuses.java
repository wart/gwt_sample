package gwtb.client.rpcdata;




import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import com.extjs.gxt.ui.client.data.ModelData;


public class MD_Bonuses implements ModelData{
	Integer id;
	String Name;
	String Count;
	String Used;
	Long bid;
	

	public MD_Bonuses() {};
	public MD_Bonuses(
						int  _id,
						String _Name,					
					String _Count,
					String _Used,
					Long _bid
					)
	{
		Name=_Name;
		Count=_Count;
		Used=_Used;
		id=_id;
		bid=_bid;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <X> X get(String property) {
		if (property.equals("id")){ return (X)id;};
		if (property.equals("Name")){ return (X)Name;};
		if (property.equals("Count")){ return (X)Count;};
		if (property.equals("Used")){ return (X) Used;};
		if (property.equals("bid")){ return (X) bid;};		
		return null;
	}
	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> m= new HashMap<String, Object>();
		m.put("id", id);
		m.put("Name", Name);
		m.put("Count", Count);
		m.put("Used", Used);
		m.put("bid", bid);
		return m;
	}
	@SuppressWarnings("serial")
	@Override
	public Collection<String> getPropertyNames() {
		Collection<String> c =  new ArrayList<String>(){
			
		};
		c.add("id");
		c.add("Name");
		c.add("Count");
		c.add("Used");
		c.add("bid");
		return c;
	}
	@SuppressWarnings("unchecked")
	@Override
	public <X> X remove(String property) {
		String s;Integer i;Long l;
		if (property.equals("id")){  i=id;id=null;return (X)i;};
		if (property.equals("Name")){  s=Name;Name=null;return (X)s;};
		if (property.equals("Count")){s=Count;Count=null;return (X)s;};
		if (property.equals("Used")){ s=Used;Used=null;return (X)s;};
		if (property.equals("bid")){ l=bid;bid=null;return (X)l;};
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public <X> X set(String property, X value) {
		String s;;Integer i;Long l;
		if (property.equals("id")){  i=id;id=(Integer)value;return (X)i;};
		if (property.equals("Name"))  {s=Name;Name=(String)value;return (X)s;};
		if (property.equals("Count")){s=Count;Count=(String)value;return (X)s;};
		if (property.equals("Used"))  {s=Used;Used=(String)value;return (X)s;};
		if (property.equals("bid"))  {l=bid;bid=(Long)value;return (X)l;};
		return null;
	};	
}
