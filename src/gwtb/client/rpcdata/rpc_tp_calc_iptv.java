package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class rpc_tp_calc_iptv implements IsSerializable{
	public String tech;
	public String name;
	public Float cost;
	public String descr;
	public rpc_tp_calc_iptv() {};
	public rpc_tp_calc_iptv(String _tech,String _name,Float _cost, String _descr)
	{
		tech=_tech;
		name=_name;
		cost=_cost;
		descr=_descr;
	}
}
