package gwtb.client.rpcdata;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;




public class ServiceExcept extends Throwable implements IsSerializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	  public static void Log(String str) {
		    System.err.println(new Date() + " >> " + str);
		  }	
	String msg;
	public ServiceExcept(String s)
	{
		msg=s;
		
	}
	
	public ServiceExcept(String s,Exception ex)
	{
		msg=s;
		Log(s+((ex==null)?"":ex.getMessage()));
	}
	
	public ServiceExcept(){ };
	
	public	String getMessage()
	{
		return msg;
		
	}
}
