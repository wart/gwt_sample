package gwtb.client.rpcdata;

import com.google.gwt.user.client.rpc.IsSerializable;

public class rpc_tp_calc_iptv_pack implements IsSerializable{

	public Integer id;
	public String name;
	public Float cost;
	public String descr;
	public rpc_tp_calc_iptv_pack() {};
	public rpc_tp_calc_iptv_pack(Integer _id,String _name,Float _cost,String _descr)
	{
		id=_id;
		name=_name;
		cost=_cost;
		descr=_descr;
	}
}
