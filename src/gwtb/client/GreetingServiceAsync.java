package gwtb.client;

import gwtb.client.rpcdata.ADM_TRACE_ITEM;
import gwtb.client.rpcdata.BronSetUnSetResult;
import gwtb.client.rpcdata.BronState;
import gwtb.client.rpcdata.DVOclipart;
import gwtb.client.rpcdata.Jrnl_rec;
import gwtb.client.rpcdata.News;
import gwtb.client.rpcdata.RPC_BillPeriod;
import gwtb.client.rpcdata.UserPeriodBonusInfo;
import gwtb.client.rpcdata.abon_corp_profile;
import gwtb.client.rpcdata.abon_profile;
import gwtb.client.rpcdata.period_calls_lg_data;
import gwtb.client.rpcdata.rpc_cmn_sprvk;
import gwtb.client.rpcdata.rpc_raz_post_usl;
import gwtb.client.rpcdata.rpc_tp_calc;
import gwtb.client.rpcdata.search_userData;
import gwtb.client.rpcdata.update_contacts;
import gwtb.client.rpcdata.wrkitem;

import java.util.Date;
import java.util.TreeMap;
import java.util.Vector;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface GreetingServiceAsync
{

	void auth        (String Login, String pwd, AsyncCallback<abon_profile> callback);
	void getMsgList  (int listid,               AsyncCallback<TreeMap<String,String>> callback);
	void getBronState(                          AsyncCallback<BronState> asyncCallback);
	void BronUnSet   (                          AsyncCallback<BronSetUnSetResult> callback);
	void BronSet     (int mon,                  AsyncCallback<BronSetUnSetResult> callback);
	void CheckADSLTech(String phone, AsyncCallback<String> callback);
	void ChangeRadiusPwd(String login, String opwd, String npwd, String npwd2,	AsyncCallback<String> callback);
	void getWRKList(int listid, AsyncCallback<TreeMap<Integer, wrkitem>> callback);
	void ChangePhoneTP(String devid, int tp, AsyncCallback<String> callback);
	void ChangeInetTP(String connID, int tp, AsyncCallback<String> callback);
	void DVOChange(String devid, String DVOLIST,AsyncCallback<Vector<DVOclipart>> callback);
	void InetPB(int summ, int days, AsyncCallback<String> callback);
	void Update_contacts(String email,String Phone,String Phonevoice,Integer e_on,Integer s_on,Integer p_on,Integer af_on,Date bday, AsyncCallback<update_contacts> callback);
	void getNewsTitles(AsyncCallback<Vector<News>> callback);
	void getNewsBody(int id, AsyncCallback<String> callback);
	void SendQuestion(String theme, String body,int svc,int typ, AsyncCallback<String> callback);
	void getJournal(AsyncCallback<Vector<Jrnl_rec>> callback);
	void getUserPeriodBonusInfo(Long BID,AsyncCallback<UserPeriodBonusInfo>callback);
	void getBalIP(AsyncCallback<Float> callback);
	void Change09State(String devid, boolean state,	AsyncCallback<String> callback);
	void UseBonuses(int count,	AsyncCallback<String> callback);
	void getADMTrace(AsyncCallback<Vector<ADM_TRACE_ITEM>> callback);
	void getModelPlans(String model , AsyncCallback<Vector<wrkitem>>callback);
	void update_einf(int bo, int schet, AsyncCallback<Void> callback);
	void search_user(String account, String name, String devid,	AsyncCallback<Vector<search_userData>> callback);
	void SelectUser(Long uid, AsyncCallback<abon_profile> callback);
	void authOper(String Login, String pwd, AsyncCallback<String> callback);
	void Update_CORPcontacts(String f1, String f2, String f3, String d1,
			String d2, String d3, String e1, String e2, String e3, String p1,
			String p2, String p3, String x1, String x2, String x3, String m1,
			String m2, String m3, Date bday,
			AsyncCallback<abon_corp_profile> callback);

	void update_CORP_einf(String email, int schet, AsyncCallback<Void> callback);
	void corpreport(String period, int a1, int a2, int s1, int s2,	AsyncCallback<Void> callback);
	void SendCorpQuestion(String theme, String body, int typ,AsyncCallback<String> callback);
	void LK_PWD_CHANGE(String login, String opwd, String npwd,String npwd2, AsyncCallback<String> asyncCallback);
	void getcmn_sprvk(Long bid, AsyncCallback<Vector<rpc_cmn_sprvk>> callback);
	void getBillPeriods(AsyncCallback<Vector<RPC_BillPeriod>> callback);
	void get_period_calls_lg_data(Long bid,	AsyncCallback<period_calls_lg_data> callback);
	void get_raz_post_usl(Long bid,	AsyncCallback<Vector<rpc_raz_post_usl>> callback);
	void block_mgmn_set(Long dev_id, int len, AsyncCallback<String> callback);
	void block_apus_set(Long dev_id, int len, AsyncCallback<String> callback);
	void block_mgmn_reset(Long dev_id, AsyncCallback<String> callback);
	void block_apus_reset(Long dev_id, AsyncCallback<String> callback);
	void Recovery_LKPass(String account, String email,AsyncCallback<String> callback);
	void get_tp_calc(AsyncCallback<rpc_tp_calc> callback);

}
