package gwtb.client;

import gwtb.client.rpcdata.abon_profile;

import com.extjs.gxt.ui.client.event.IconButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.button.ToolButton;
import com.extjs.gxt.ui.client.widget.custom.Portal;
import com.extjs.gxt.ui.client.widget.custom.Portlet;
import com.extjs.gxt.ui.client.widget.flash.FlashComponent;
import com.google.gwt.user.client.ui.RootPanel;

public class Desctop
{

  private static Portal portal = null;
  private static Portlet userInfo=null;
  private static Portlet volgaInfo=null;
  private static Portlet welcome=null;
  private static Portlet obyava=null;
  private static Portlet speed=null;
//---------------------------------------------------------
  public static void Run() {
    init();
  }
//---------------------------------------------------------
  static private Portlet CreatePortlet(String titleId)
  {
	  Portlet p= new Portlet();
	    p.setHeading(GSControl.msgs1.get(titleId));
	    p.setCollapsible(true);
	    p.setAnimCollapse(false);
	    p.setAutoWidth(true);
	    p.setVisible(true);
	    p.getHeader().addTool(new ToolButton("x-tool-close", new DesctopRML(p)));
	    return p;
  };
//---------------------------------------------------------
  private static void init()
  {
    portal = new Portal(1);
    portal.setStyleAttribute("backgroundColor", "white");
    RootPanel.get("widgets_place").add(portal);
    //--------------------------------------
    welcome =CreatePortlet("welcome_title");
    welcome.addText(GSControl.msgs1.get("welcome"));
    //--------------------------------------
    //volgaInfo= CreatePortlet("");  //TODO
    //--------------------------------------    
    obyava = CreatePortlet("obyava_title");
    obyava.addText(GSControl.msgs1.get("obyava"));
    //--------------------------------------  
    speed = CreatePortlet("speed_test_title");
    try {
      FlashComponent fc = new FlashComponent("http://www.speedtest.net/mini/speedtest.swf");
      fc.setFlashVersion("8.0.0");
      fc.setVisible(true);
      fc.setExpressInstall("flash/expressInstall.swf");
      fc.setSize(450, 250);
      fc.setSwfHeight("250");
      fc.setSwfWidth("450");
      speed.add(fc);
    } catch (Exception ex) {}
    
    
    Update(null);
  }
//---------------------------------------------------------
  public static void Update(abon_profile ap)
  {
	  if(userInfo!=null)
	  {
		  welcome.removeFromParent();
		  obyava.removeFromParent();
		  speed.removeFromParent();
		  userInfo.removeFromParent();
	  };
	  if(ap!=null)
	  {
		    userInfo = CreatePortlet("userinf_title");
		    Label useracc= new Label();
		    Label username= new Label();
		    Label useraddr= new Label();    
			useracc.setText(GSControl.msgs1.get("userinf_acc")+"\n"+ap.account);
		    username.setText((ap.IsCorp=="Y"?GSControl.msgs1.get("userinf_name2"):GSControl.msgs1.get("userinf_name1"))+"\n"+ap.name);
		    useraddr.setText(GSControl.msgs1.get("userinf_addr")+"\n"+ap.addr);
		    userInfo.setHeading(GSControl.msgs1.get("userinf_title"));
		    userInfo.removeAll();
		    userInfo.add(useracc);
		    userInfo.add(new Html("<br>"));
			userInfo.add(username);
			userInfo.add(new Html("<br>"));
			userInfo.add(useraddr);
			userInfo.add(new Html("<br>"));
			portal.add(userInfo, 0);
	  }
	    //portal.add(volgaInfo, 0);
	    portal.add(welcome, 0);
	    portal.add(obyava, 0);
	    portal.add(speed, 0);
  }
  //------------------------------------------------------
}
