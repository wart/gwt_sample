package gwtb.client;



import gwtb.client.rpcdata.ADM_TRACE_ITEM;
import gwtb.client.rpcdata.BronSetUnSetResult;
import gwtb.client.rpcdata.BronState;
import gwtb.client.rpcdata.DVOclipart;
import gwtb.client.rpcdata.Jrnl_rec;
import gwtb.client.rpcdata.News;
import gwtb.client.rpcdata.RPC_BillPeriod;
import gwtb.client.rpcdata.ServiceExcept;
import gwtb.client.rpcdata.UserPeriodBonusInfo;
import gwtb.client.rpcdata.abon_profile;
import gwtb.client.rpcdata.period_calls_lg_data;
import gwtb.client.rpcdata.rpc_cmn_sprvk;
import gwtb.client.rpcdata.rpc_raz_post_usl;
import gwtb.client.rpcdata.rpc_tp_calc;
import gwtb.client.rpcdata.search_userData;
import gwtb.client.rpcdata.abon_corp_profile;
import gwtb.client.rpcdata.update_contacts;
import gwtb.client.rpcdata.wrkitem;

import java.util.Date;
import java.util.TreeMap;
import java.util.Vector;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("greet")
public interface GreetingService 
       extends RemoteService
{
	public abon_profile                 auth(String Login,String pwd) throws ServiceExcept;
	public String 						authOper(String Login,String pwd) throws ServiceExcept;
	public TreeMap<String,String>       getMsgList(int listid)        throws ServiceExcept;
	public TreeMap<Integer,wrkitem>     getWRKList(int listid)        throws ServiceExcept;
	public BronState 					getBronState()                throws ServiceExcept;
	public BronSetUnSetResult           BronSet(int mon)              throws ServiceExcept;
	public BronSetUnSetResult           BronUnSet()                   throws ServiceExcept;
	
	public String CheckADSLTech(String phone) throws ServiceExcept;
	public String ChangeRadiusPwd(String login,String opwd,String npwd,String npwd2)throws ServiceExcept;
	public String ChangePhoneTP(String devid,int tp) throws ServiceExcept;
	public String Change09State(String devid,boolean state) throws ServiceExcept;
	public String InetPB(int summ,int days) throws ServiceExcept;
	public String ChangeInetTP (String connID,int tp) throws ServiceExcept;
	public Vector<DVOclipart> DVOChange (String devid,String DVOLIST) throws ServiceExcept;
	public update_contacts Update_contacts(String email,String Phone,String Phonevoice,Integer e_on,
			Integer s_on,Integer p_on,Integer af_on,Date bday)throws ServiceExcept;
	
	public abon_corp_profile Update_CORPcontacts(String f1,String f2,String f3,
												   String d1,String d2,String d3,
												   String e1,String e2,String e3,
												   String p1,String p2,String p3,
												   String x1,String x2,String x3,
												   String m1,String m2,String m3,Date bday)throws ServiceExcept;
			
	
	public Vector<News> getNewsTitles()throws ServiceExcept;
	public String getNewsBody(int id)throws ServiceExcept;
	public String SendQuestion(String theme, String body,int svc,int typ)throws ServiceExcept;
	public String SendCorpQuestion(String theme, String body,int typ)throws ServiceExcept;
	public Vector<Jrnl_rec> getJournal()throws ServiceExcept;
	
	public Vector<RPC_BillPeriod> getBillPeriods()throws ServiceExcept;
	
	
	public Vector<rpc_cmn_sprvk> getcmn_sprvk(Long bid)throws ServiceExcept;
	public Vector<rpc_raz_post_usl> get_raz_post_usl(Long bid)throws ServiceExcept;
	public period_calls_lg_data get_period_calls_lg_data(Long bid)throws ServiceExcept;
	
	
	
	public Vector<search_userData> search_user(String account,String name,String devid)throws ServiceExcept;	
	
	public abon_profile                 SelectUser(Long uid) throws ServiceExcept;
	
	
	public UserPeriodBonusInfo getUserPeriodBonusInfo(Long BID)throws ServiceExcept;


	
//	public Vector<UserBonusPeriodListItem> 	getUserBonusPeriodList()throws ServiceExcept;
	
	public Vector<ADM_TRACE_ITEM> getADMTrace()throws ServiceExcept;
	
	public String UseBonuses(int count) throws ServiceExcept;
	
	public void   update_einf(int bo,int schet) throws ServiceExcept;
	public void   update_CORP_einf(String email,int schet) throws ServiceExcept;
	
	public void   corpreport(String period,int a1,int a2,int s1,int s2) throws ServiceExcept;
	
	public	float getBalIP()throws ServiceExcept;
	public Vector<wrkitem>				getModelPlans(String Model)throws ServiceExcept;
	public String LK_PWD_CHANGE(String login, String opwd, String npwd, String npwd2)throws ServiceExcept;
	
	public String Recovery_LKPass(String account,String email)throws ServiceExcept;
	public String block_apus_set(Long dev_id,int len)throws ServiceExcept;
	public String block_mgmn_set(Long dev_id,int len)throws ServiceExcept;
	public String block_apus_reset(Long dev_id)throws ServiceExcept;
	public String block_mgmn_reset(Long dev_id)throws ServiceExcept;
	public rpc_tp_calc get_tp_calc()throws ServiceExcept;
}
