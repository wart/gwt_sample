package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.ADM_TRACE_ITEM;
import gwtb.client.rpcdata.MD_ADM_TRACE_ITEM;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.extjs.gxt.ui.client.event.IconButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.ToolButton;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.tips.ToolTipConfig;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class ADM_TRACE extends Window
{
	static ADM_TRACE form=null;
	
	public static void Run()
	{
		if (form==null)
		{
			form= new ADM_TRACE();
		}
		form.show();
		form.RefreshData();
	}
	private ListStore<MD_ADM_TRACE_ITEM> JournalData=null;
	private ColumnModel cm ;
	private Grid<MD_ADM_TRACE_ITEM> grid;	
	public ADM_TRACE()
	{
		this.setSize(800, 600);   
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading("ADM Trace");
		this.setMaximizable(true);
	
		JournalData=new ListStore<MD_ADM_TRACE_ITEM>();
		
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();   
	    ColumnConfig column = new ColumnConfig("date","date", 100);configs.add(column);
	    column = new ColumnConfig("src","src", 100);configs.add(column);
	    column = new ColumnConfig("mess","mess", 100);configs.add(column);
	    column = new ColumnConfig("note","note", 100);configs.add(column);
	    
	    cm = new ColumnModel(configs);

	    grid = new Grid<MD_ADM_TRACE_ITEM>(JournalData, cm);
	    grid.setStyleAttribute("borderTop", "none");   
	    grid.setAutoExpandColumn("mess");   
	    grid.setBorders(true);   
	    grid.setStripeRows(true);		
		
	    
	    
	    
	    
	    
	    ToolButton brefresh= new ToolButton("x-tool-refresh");
	    brefresh.setToolTip(new ToolTipConfig("","Refresh"));
    
	    brefresh.addSelectionListener( 
				 new SelectionListener<IconButtonEvent>() {   
					      @Override  
						    public void componentSelected(IconButtonEvent ce)
							{
					    	  form.RefreshData();
							}
							});
	    
	    this.getHeader().addTool(brefresh);
	    
	    
	    setLayout(new FitLayout());   
	    
	    FormPanel panel = new FormPanel();   
	    panel.setBorders(false);   
	    panel.setBodyBorder(false);   
	    panel.setLabelWidth(55);   
	    panel.setPadding(5);   
	    panel.setHeaderVisible(false);   
	    panel.add(grid, new FormData("100% 100%"));  
	    add(panel);	    
	}
	protected void RefreshData() {
		GSControl.ShowProcessing();	
		
        GSControl.GetGS().getADMTrace(
				new AsyncCallback<Vector<ADM_TRACE_ITEM>>()
				{
					public void onFailure(Throwable caught)
					{
	        			GSControl.HideProcessing();
	        			GSControl.ErrMsg(caught);
					}
					public void onSuccess(Vector<ADM_TRACE_ITEM> res)
					{
						GSControl.HideProcessing();
						form.JournalData.removeAll();
						int sz=res.size();
					//	Info.display("Size","{0}",new Integer(sz).toString());
						
						ADM_TRACE_ITEM b;
						for (int j=0;j<sz;++j)
						{
							b= res.get(j);
							if (b!=null)
								form.JournalData.add(new MD_ADM_TRACE_ITEM(b));
						};
					//	form.grid.recalculate();
					};
				});
		
	}
}
