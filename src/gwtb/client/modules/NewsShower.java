package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.News;

import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.widget.Window;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalSplitPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class NewsShower extends Window
{
	private static TreeMap<String,String> msgs12=null;
	private static Vector<News>  news=null;
	private static NewsShower form=null;
	
	public static void Run()
	{
		if (msgs12==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(12,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught);
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs12=res;
						GSControl.HideWait();
						Load_News();
					};
				});
		}else
		{Load_News();};
	}		
	//=======================================================================
	private static void Load_News()
	{
		if (news==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getNewsTitles(new AsyncCallback<Vector<News>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught);
					}
					public void onSuccess(Vector<News> res)
					{
						news=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{Create();};
	}		
	//=======================================================================

	private static  void Create()
	{
		if (form==null)
		{
			form= new NewsShower();
		};
		form.show();
		form.RefreshBody();
	}	
	//============================================================================//
		HorizontalSplitPanel hSplit;
		ListBox multiBox;
		VerticalPanel vpL;
		VerticalPanel vpR;
		VerticalPanel vpM;
		News WaitedNews;
		HTML body;
		public NewsShower()
		{
			this.setSize(500, 300);   
			this.setPlain(true);   
			this.setModal(GSControl.formsmodal);   
			this.setBlinkModal(true);   
			this.setHeading(msgs12.get("oth_news_title")); 
			
			
			vpL= new VerticalPanel();vpL.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
			vpR= new VerticalPanel();//vpR.setHorizontalAlignment(VerticalPanel);
			vpM= new VerticalPanel();vpM.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
			vpL.setWidth("100%");
			vpR.setWidth("100%");
			vpM.setWidth("100%");
			vpL.add(new Label(msgs12.get("oth_news_ntitle")));vpL.add(new HTML("<hr>"));
			vpR.add(new Label(msgs12.get("oth_news_nbody")));vpR.add(new HTML("<hr>"));
			body=new HTML();
			vpR.add(body);
		    multiBox = new ListBox(false);
//		    multiBox.ensureDebugId("cwListBox-multiBox");
		    multiBox.setWidth("100%");
		    multiBox.setVisibleItemCount(14);
		    
		    
		    
		    if(news!=null)
		    {
		    	int sz=news.size();
		    	News n=null;
		    	for(int i=0;i<sz;i++)
		    	{
		    		n=news.get(i);
		    		if(n!=null)
		    		{
		    			multiBox.addItem(n.title,n.id+"");
		    		};
		    	};
		    };
		    vpL.add(multiBox);
		    multiBox.addChangeHandler(new ChangeHandler(){

				@Override
				public void onChange(ChangeEvent event) {
					form.RefreshBody();
				};
		    	
		    });
		    
		    hSplit = new HorizontalSplitPanel();
		    hSplit.ensureDebugId("cwHorizontalSplitPanel");
		    hSplit.setSize("600px", "350px");
		    hSplit.setSplitPosition("30%");
		    hSplit.setLeftWidget(vpL);
		    hSplit.setRightWidget(vpR);
		    vpM.add(hSplit);
		    vpM.add(new HTML("<hr>"));
		    
		    this.add(vpM);
		    
		    if(multiBox.getItemCount()>0)
		    {
		    	multiBox.setSelectedIndex(0);
		    };
		}
		//===================================================================		
		public News getNews(int id)
		{
		    if(news!=null)
		    {
		    	int sz=news.size();
		    	News n=null;
		    	for(int i=0;i<sz;i++)
		    	{
		    		n=news.get(i);
		    		if(n!=null)
		    		{
		    			if (n.id==id) return n;
		    		};
		    	};
		    };
		    return null;
		}
		//===================================================================		
		public void RefreshBody()
		{
			int sel=multiBox.getSelectedIndex();
			if (sel>=0)
			{
				int idd=Integer.parseInt(multiBox.getValue(sel));
				WaitedNews=getNews(idd);
				if(WaitedNews!=null)
				{
					if (WaitedNews.body==null)
					{
					
						form.multiBox.setEnabled(false);
						GSControl.ShowProcessing();
						
			        GSControl.GetGS().getNewsBody(WaitedNews.id, new AsyncCallback<String>()
							{
								public void onFailure(Throwable caught)
								{
									form.multiBox.setEnabled(true);
									GSControl.HideProcessing();
									GSControl.ErrMsg(caught);
								}
								public void onSuccess(String res)
								{
									WaitedNews.body=res;
									form.multiBox.setEnabled(true);
									GSControl.HideProcessing();
									form.ShowNews();
								};
							});
					}else{
						form.ShowNews();
					};
				};
			}
		}
		//===================================================================		
		public void ShowNews()
		{
			if(WaitedNews!=null)
			{
				if (WaitedNews.body!=null)
				{
					
					body.setHTML(WaitedNews.body);
				};
				WaitedNews=null;
			};	
		};
		//===================================================================
		
	}