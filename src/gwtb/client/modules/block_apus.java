package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.MDModelData;

import java.util.TreeMap;

import org.apache.naming.java.javaURLContextFactory;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.ListView;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class block_apus extends Window
{
	private static TreeMap<String,String> msgs32=null;
	static block_apus form=null;
	//=======================================================================	
	public static  void Run()
	{
		if (msgs32==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(32,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs32=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{Create();};
	}		
	//=======================================================================	
	public static void Create()
	{
		if (form==null)
		{
			form= new block_apus();
		};
		form.show();
		form.phones.setValue(form.LS.getAt(0));
	}	
	//============================================================================//
	public ComboBox<MDModelData> phones;
	public NumberField len;
	ListStore<MDModelData> LS;
	Button bsubmit;
	public boolean blocked;
	public block_apus()
	{
		   this.setHeading(msgs32.get("blkaps_title"));
		   this.setResizable(false);
  		   this.setSize(470, 300);
		   this.setPlain(true);
		   this.setModal(GSControl.formsmodal);
		   this.setBlinkModal(true);
		   FormLayout lay = new FormLayout();
		   lay.setLabelWidth(200);
		   this.setLayout(lay);
		   
		   
		   FormData formData = new FormData("-24");
		   
		   LS=GSControl.buildPhoneListBoxLS(0);
		   
		   phones= new ComboBox<MDModelData>();
		   phones.setFieldLabel(msgs32.get("blkaps_phone"));
		   phones.setValueField("phone");
		   phones.setDisplayField("phone"); 
		   phones.setName("phone"); 
		   
		   phones.setStore(LS);
		   phones.setEditable(false);
		   phones.setForceSelection(true);
	       phones.addSelectionChangedListener(new SelectionChangedListener<MDModelData>(){

			@Override
			public void selectionChanged(SelectionChangedEvent<MDModelData> se) {
				MDModelData mm=se.getSelectedItem();
				bsubmit.setEnabled(false);
				if(mm!=null)
				{
					String s=(String)mm.get("phone");
					len.setVisible(!(blocked=GSControl.myInfo.BlokedAPUS(s)));
					bsubmit.setEnabled(true);
				}				
			}});
		   len = new NumberField();
		   len.setPropertyEditorType(Integer.class);

		   
		   len.setFieldLabel(msgs32.get("blkaps_len")); 
		   len.setAutoValidate(true);
		   len.setAllowBlank(false);
		   len.setValidator(new Validator()
		   {
			@Override
			public String validate(Field<?> arg0, String arg1)
			{
				Number n;
				if((n=form.len.getValue())!=null)
				{
					if ((n.intValue()<1)||(n.intValue()>12))
						return msgs32.get("blkaps_len_lenerr");
					return null;
				};
				return msgs32.get("blkaps_len_lenerr");
			}
		   });
		   
		   
		   this.add(new Html("<br>"));
		   this.add(new Label(msgs32.get("blkaps_txt")));
		   this.add(new Html("<br>"));
		   this.add(phones,formData);
		   this.add(len,formData);
		 bsubmit= new Button(msgs32.get("blkaps_submit"));
		 this.addButton(bsubmit);
	     bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>()
	    		 {
	         		@Override
	         		public void componentSelected(ButtonEvent ce)
	         		{
	         			
	         			if(form.blocked)  // Unblock
	         			{
	         			
	        			GSControl.askdothis(new ClickHandler()  //yes
											{
					          					public void onClick(ClickEvent event)
					          					{
					          						GSControl.ShowProcessing();	
					          						//	GSControl.GetGS().block_apus_set(dev_id, len, callback)
					          						//new AsyncCallback<Void>()
					          					}
											},null,msgs32.get("blkaps_quest")+" 111111111"+"?",null);
	         			}else{// block
	         			
	         			};
	         		}
	         	});
	};
};	
	
