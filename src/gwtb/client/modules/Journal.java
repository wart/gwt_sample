package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.Jrnl_rec;
import gwtb.client.rpcdata.MD_Jrnl_rec;
import gwtb.client.rpcdata.wrkitem;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.IconButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.ToolButton;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.tips.ToolTipConfig;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class Journal extends Window
{
	private static TreeMap<String,String> msgs14=null;
	private static TreeMap<Integer,wrkitem> wrklst5=null;
	private static Journal form=null;
	private ListStore<MD_Jrnl_rec> JournalData=null;
	private ColumnModel cm ;
	private Grid<MD_Jrnl_rec> grid;	

	
	public  static void Run()
	{
		if (msgs14==null)
		{
			GSControl.ShowWait();
	        GSControl.GetGS().getMsgList(14,
			new AsyncCallback<TreeMap<String,String>>()
			{
				public void onFailure(Throwable caught)
				{
					GSControl.HideWait();
					GSControl.ErrMsg(caught); 
				}
				public void onSuccess(TreeMap<String,String> res)
				{
					GSControl.HideWait();
					msgs14=res;
					Load();
				};
			});
		}else
		{
			Load();
		}
	}  
	//=======================================================================
	private static void Load()
	{
		//Window.alert("LoadMsgs4_bron_form()");
		if (wrklst5==null)
		{
			GSControl.ShowWait();
	        GSControl.GetGS().getWRKList(5,
	        		new AsyncCallback<TreeMap<Integer,wrkitem>>()
	        		{
						public void onFailure(Throwable caught)
						{
							GSControl.HideWait();
							GSControl.ErrMsg(caught); 
						}
						public void onSuccess(TreeMap<Integer,wrkitem> res)
						{
							GSControl.HideWait();
							wrklst5=res;
							Create();
						};
			});
		}
		else
		{
			Create();
		}
	}				
	//=======================================================================
	public static void Create()
	{
		if (form==null)
		{
			form= new Journal();
		};
		form.show();
		form.RefreshData(false);
	}	
	//	=======================================================================
	
	public Journal()
	{
		this.setSize(500, 300);   
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading(msgs14.get("acc_jrnl_title"));
		this.setMaximizable(true);
	
		JournalData=new ListStore<MD_Jrnl_rec>();
		
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();   
		  
	    ColumnConfig column = new ColumnConfig();   
	    column.setId("date");   
	    column.setHeader(msgs14.get("acc_jrnl_dt"));   
	    column.setWidth(25);   
	    configs.add(column);   
	  
	    column = new ColumnConfig();   
	    column.setId("type");   
	    column.setHeader(msgs14.get("acc_jrnl_ztype"));   
	    column.setWidth(100);   
	    configs.add(column);   
	  
	    column = new ColumnConfig();   
	    column.setId("device");   
	    column.setHeader(msgs14.get("acc_jrnl_dev"));   
	    //column.setAlignment(HorizontalAlignment.RIGHT);   
	    column.setWidth(75);   
//	    column.setRenderer(gridNumber);   
	    configs.add(column);   
	  
	    column = new ColumnConfig("params", msgs14.get("acc_jrnl_in"), 100);   
	    column.setAlignment(HorizontalAlignment.RIGHT);   
//	    column.setRenderer(change);   
	    configs.add(column);   
	  
	    column = new ColumnConfig("return", msgs14.get("acc_jrnl_out"), 150);   
	    column.setAlignment(HorizontalAlignment.RIGHT);   
//	    column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
   	    configs.add(column);
 
	    /**/
	    

	    cm = new ColumnModel(configs);

	    grid = new Grid<MD_Jrnl_rec>(JournalData, cm);
	    grid.setStyleAttribute("borderTop", "none");   
	    grid.setAutoExpandColumn("return");   
	    grid.setBorders(true);   
	    grid.setStripeRows(true);		
		
	    
	    
	    
	    
	    
	    ToolButton brefresh= new ToolButton("x-tool-refresh");
	    brefresh.setToolTip(new ToolTipConfig("",msgs14.get("acc_jrnl_refresh")));
    
	    brefresh.addSelectionListener( 
				 new SelectionListener<IconButtonEvent>() {   
					      @Override  
						    public void componentSelected(IconButtonEvent ce)
							{
					    	  form.RefreshData(true);
							}
							});
	    
	    this.getHeader().addTool(brefresh);
	    
	    
	    setLayout(new FitLayout());   
	    
	    FormPanel panel = new FormPanel();   
	    panel.setBorders(false);   
	    panel.setBodyBorder(false);   
	    panel.setLabelWidth(55);   
	    panel.setPadding(5);   
	    panel.setHeaderVisible(false);   
	    panel.add(grid, new FormData("100% 100%"));  
	    add(panel);	    
	};
	//-------------------------------------------------------------------
	public void RefreshData(boolean force)
	{
		if ((form.JournalData==null)||(GSControl.JournalNeedReFresh)||(force))
		{
			
			GSControl.ShowProcessing();	
			
	        GSControl.GetGS().getJournal(
					new AsyncCallback<Vector<Jrnl_rec>>()
					{
						public void onFailure(Throwable caught)
						{
		        			GSControl.HideProcessing();
		        			GSControl.ErrMsg(caught);
						}
						public void onSuccess(Vector<Jrnl_rec> res)
						{
							GSControl.HideProcessing();
							form.JournalData.removeAll();
							int sz=res.size();
						//	Info.display("Size","{0}",new Integer(sz).toString());
							
							Jrnl_rec b;
							for (int j=0;j<sz;++j)
							{
								b= res.get(j);
								if (b!=null)
									form.JournalData.add(new MD_Jrnl_rec(b.dt,wrklst5.get(b.type-1).cap,b.dev,b.prms,b.ret));
							};
						//	form.grid.recalculate();
						};
					});
	        GSControl.JournalNeedReFresh=false;
		};
	}
	//-------------------------------------------------------------------
}
