package gwtb.client.modules;

import gwtb.client.GSControl;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class emailinf extends Window
{
	private static TreeMap<String,String> msgs21=null;
	static emailinf form=null;
	//=======================================================================	
	public static  void Run()
	{
		if (msgs21==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(21,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs21=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{Create();};
	}		
	//=======================================================================	
	public static void Create()
	{
		if (form==null)
		{
			form= new emailinf();
		};
		form.c1.setValue(GSControl.myInfo.einf_bo==1?true:false);
		form.c2.setValue(GSControl.myInfo.einf_schet==1?true:false);
		form.l1.setText(GSControl.myInfo.account + ": " + GSControl.myInfo.name);
		form.l2.setText(msgs21.get("emailinf_txt1") + ": " + GSControl.myInfo.email);
		form.show();
	}	
	//============================================================================//
	public Label l1; 
	public Label l2;
	public CheckBox c1;
	public CheckBox c2;
	public Button bsubmit;
	public emailinf()
	{
		
		 //this.setWidth("700px");
		 this.setSize(470, 190);
		 this.setPlain(true);
		 this.setModal(GSControl.formsmodal);
		 this.setBlinkModal(true);
		 this.setHeading(msgs21.get("emailinf_title"));		
		 l1 = new Label();//l1.setStyleName("mytxt3");
	     l2 = new Label();//l2.setStyleName("mytxt3");
	     
	     c1 = new CheckBox();c1.setBoxLabel(msgs21.get("emailinf_ok_bal_otkl"));//c1.setStyleName("mytxt3");	     
	     c2 = new CheckBox();c2.setBoxLabel(msgs21.get("emailinf_ok_schet"));//c2.setStyleName("mytxt3");	     
	     bsubmit= new Button(msgs21.get("emailinf_commit"));
	     
	     bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>() {
	         @Override
	         public void componentSelected(ButtonEvent ce) {
	        
	        			GSControl.askdothis(new ClickHandler()  //yes
						{
					          public void onClick(ClickEvent event)
					          {
					        	  GSControl.ShowProcessing();	
					        	  GSControl.GetGS().update_einf(form.c1.getValue()?1:0,form.c2.getValue()?1:0,
									new AsyncCallback<Void>()
									{
						        		public void onFailure(Throwable caught)
						        		{
						        			GSControl.HideProcessing();
						        			GSControl.JournalNeedReFresh=true;
						        			GSControl.ErrMsg(caught); 
										}
										public void onSuccess(Void res )
										{
											GSControl.HideProcessing();
						        			GSControl.JournalNeedReFresh=true;
						        			GSControl.myInfo.einf_bo   =form.c1.getValue()?1:0;
											GSControl.myInfo.einf_schet=form.c2.getValue()?1:0;
  					        			    GSControl.OkMsg(msgs21.get("emailinf_commit_ok"));   
										};
									});
					          };
		},null,null,null);}});
	     
	     
	     VerticalPanel wd= new VerticalPanel();
		//	wd.setHorizontalAlign(HorizontalAlignment.CENTER);	     

	     wd.add(l1);
	     wd.add(l2);
	     wd.add(new Html("<br>"));
	     
	     wd.add(c1);
	     wd.add(c2);
	     
	     this.add(wd);
	     
	     this.addButton(bsubmit);
	     
//	    wd.add(l3);
	
	}
	//============================================================================//
	
	
	
	
	
	
	
	
	
	
	
	
}
