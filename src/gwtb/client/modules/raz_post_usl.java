package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.BillPeriod;
import gwtb.client.rpcdata.MDModelData;
import gwtb.client.rpcdata.RPC_BillPeriod;
import gwtb.client.rpcdata.rpc_raz_post_usl;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;

public class raz_post_usl extends Window
{
	private static TreeMap<String, String> msgs29 = null;
	private static raz_post_usl form = null;

	public static void Run()
	{
		if (msgs29 == null)
		{
			GSControl.ShowWait();
			GSControl.GetGS().getMsgList(29, new AsyncCallback<TreeMap<String, String>>()
			{
				public void onFailure(Throwable caught)
				{
					GSControl.HideWait();
					GSControl.ErrMsg(caught);
				}
				public void onSuccess(TreeMap<String, String> res)
				{
					msgs29 = res;
					GSControl.HideWait();
					Create();
				}
			});
		}
	    else
	    {
	    	Create();
	    }
	}
	// =======================================================================
	private static void Create()
	{
		if (form == null)
	    {
			form = new raz_post_usl();
			if (GSControl.myInfo.billData==null)
			{
				GSControl.ShowWait();
				GSControl.GetGS().getBillPeriods(new AsyncCallback<Vector<RPC_BillPeriod>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(Vector<RPC_BillPeriod> res)
					{
						GSControl.HideWait();
						GSControl.myInfo.billData=BillPeriod.convert(res);
						form.showForm();
					};
				});
			}else{
				form.showForm();
			};
			return;	      
	    }
	    form.showForm();
	  }
	  // =======================================================================
	  private ColumnModel raz_cm;
	  private ListStore<MDModelData> raz;
	  private List<ColumnConfig> raz_configs;
	  private Grid<MDModelData> raz_grid;
	  
	  private ColumnModel cnst_cm;
	  private ListStore<MDModelData> cnst;
	  private List<ColumnConfig> cnst_configs;
	  private Grid<MDModelData> cnst_grid;

	  public ListBox PeriodList;
	  public ChangeHandler ch;
	  public Label raz_total;
	  public Label const_total;
	  
	  public raz_post_usl()
	  {
		  this.setSize(600, 600);   
			this.setPlain(true);   
			this.setModal(GSControl.formsmodal);   
			this.setBlinkModal(true);   
			this.setHeading(msgs29.get("rzpusl_title"));
			this.setMaximizable(true);
			
			
			raz=new ListStore<MDModelData>();
			raz_configs = new ArrayList<ColumnConfig>();   
		    ColumnConfig 
		    column = new ColumnConfig(BillPeriod.cnst_raz_svc_heads()[0]  ,msgs29.get("rzpusl_phone"	), 100);raz_configs.add(column);
		    column = new ColumnConfig(BillPeriod.cnst_raz_svc_heads()[1]  ,msgs29.get("rzpusl_usl"  	), 100);raz_configs.add(column);
		    column = new ColumnConfig(BillPeriod.cnst_raz_svc_heads()[2]  ,msgs29.get("rzpusl_summ"  	), 100);raz_configs.add(column);
		    raz_cm = new ColumnModel (raz_configs);
		    raz_grid = new Grid<MDModelData>(raz, raz_cm);
		    raz_grid.setStyleAttribute("borderTop", "none");   
		    raz_grid.setAutoExpandColumn(BillPeriod.cnst_raz_svc_heads()[2]);   
		    raz_grid.setBorders(true);   
		    raz_grid.setStripeRows(true);				  
		    raz_grid.setHeight(200);
//		    raz_grid.setWidth(700);		    
		    
		    
		    
		    
		    cnst=new ListStore<MDModelData>();
		    cnst_configs = new ArrayList<ColumnConfig>();   
		    column = new ColumnConfig(BillPeriod.cnst_raz_svc_heads()[0]  ,msgs29.get("rzpusl_phone"), 100);cnst_configs.add(column);
		    column = new ColumnConfig(BillPeriod.cnst_raz_svc_heads()[1]  ,msgs29.get("rzpusl_usl"  ), 100);cnst_configs.add(column);
		    column = new ColumnConfig(BillPeriod.cnst_raz_svc_heads()[2]  ,msgs29.get("rzpusl_summ" ), 100);cnst_configs.add(column);
		    cnst_cm = new ColumnModel(cnst_configs);
		    cnst_grid = new Grid<MDModelData>(cnst, cnst_cm);
		    cnst_grid.setStyleAttribute("borderTop", "none");   
		    cnst_grid.setAutoExpandColumn(BillPeriod.cnst_raz_svc_heads()[2]);   
		    cnst_grid.setBorders(true);   
		    cnst_grid.setStripeRows(true);		
		    cnst_grid.setHeight(200);
		    
		    PeriodList= new ListBox();
			ch=new ChangeHandler()
			{
				@Override
				public void onChange(ChangeEvent event)
				{
					if (PeriodList.getSelectedIndex()>=0)
					{
						BillPeriod bp=GSControl.myInfo.getBillPeriodByName(PeriodList.getItemText(PeriodList.getSelectedIndex())); 
						if (bp!=null)
						{
							if(bp.raz_svc!=null)
							{
								form.visualize(bp);
							}else
							{
								GSControl.ShowWait();
								GSControl.GetGS().get_raz_post_usl(bp.id,new AsyncCallback<Vector<rpc_raz_post_usl>>()
								{
									public void onFailure(Throwable caught)
									{
										GSControl.HideWait();
										GSControl.ErrMsg(caught); 
									}
									public void onSuccess(Vector<rpc_raz_post_usl> res)
									{
										BillPeriod bp=GSControl.myInfo.getBillPeriodByName(form.PeriodList.getItemText(PeriodList.getSelectedIndex()));
										if(bp!=null)
										{
											bp.fill_SVC(res);
											form.visualize(bp);
										}
										GSControl.HideWait();									
									};
								});
							};
						}
					}
				}
			};
			PeriodList.addChangeHandler(ch);
			Label l1= new Label(msgs29.get("rzpusl_bid"       ));
			Label l2= new Label(msgs29.get("rzpusl_const"     ));
			Label l3= new Label(msgs29.get("rzpusl_const_itog")+":  ");
			const_total= new Label();
			Label l4= new Label(msgs29.get("rzpusl_rub"       ));
			Label l5= new Label(msgs29.get("rzpusl_raz"   	  ));
			Label l6= new Label(msgs29.get("rzpusl_raz_itog"  )+":  ");
			raz_total= new Label();
			Label l7= new Label(msgs29.get("rzpusl_rub"       ));			
			this.add(l1);
			this.add(PeriodList);
			this.add(new Html("<br>"));
			this.add(l2);
			this.add(cnst_grid);
			this.add(l3);
			this.add(const_total);
			this.add(l4);
			this.add(new Html("<br><br>"));
			this.add(l5);
			this.add(raz_grid);
			this.add(l6);
			this.add(raz_total);
			this.add(l7);			
	  }
	// =======================================================================
	  protected void visualize(BillPeriod bp)
	  {
		  raz_grid.reconfigure(bp.raz_svc, raz_cm);
		  cnst_grid.reconfigure(bp.cnst_svc,cnst_cm);
		  const_total.setText(" "+bp.const_total+" ");
		  raz_total.setText(" "+bp.raz_total+" ");
	  }
	// =======================================================================
	  public void showForm()
	  {
		  	PeriodList=GSControl.myInfo.CreateBillPerListBox(PeriodList, false);
			ch.onChange(null);
			show();
	  }
	  // =======================================================================
}
