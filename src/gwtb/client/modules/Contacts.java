package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.update_contacts;

import java.util.Date;
import java.util.TreeMap;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;

public class Contacts extends Window {
  private static TreeMap<String, String> msgs11 = null;
  private static Contacts form = null;

  public static void Run() {
    if (msgs11 == null) {
      GSControl.ShowWait();
      GSControl.GetGS().getMsgList(11, new AsyncCallback<TreeMap<String, String>>() {

        public void onFailure(Throwable caught) {
          GSControl.HideWait();
          GSControl.ErrMsg(caught);
        }

        public void onSuccess(TreeMap<String, String> res) {
          msgs11 = res;
          GSControl.HideWait();
          Create();
        }
      });
    }
    else {
      Create();
    }
  }

  // =======================================================================
  private static void Create() {
    if (form == null) {
      form = new Contacts();
    }
    form.showForm();
  }

  // =======================================================================
  // public Label Errmsg;
  public TextBox email;
  public TextBox email2;
  public TextBox phone;
  public TextBox phonevoice;
  public CheckBox sms_on;
  public CheckBox email_on;
  public CheckBox voice_on;
  public CheckBox afert_on;
  public DateField date;
  public Label l3;
  public VerticalPanel wd;

  public Contacts() {
    this.setWidth("700px");
    // this.setSize(500, 300);
    this.setPlain(true);
    this.setModal(GSControl.formsmodal);
    this.setBlinkModal(true);

    this.setHeading(msgs11.get("acc_contact_title"));

    email = new TextBox();
    email.setWidth("150px");
    email2 = new TextBox();
    email.setWidth("100%");
    phone = new TextBox();
    phone.setWidth("270px");
    phonevoice = new TextBox();
    phonevoice.setWidth("270px");
    
    date = new DateField();   
    date.setFieldLabel(msgs11.get("acc_contact_bday"));  
    
    sms_on = new CheckBox();
    // sms_on.setBoxLabel(msgs11.get("acc_contact_accept"));
    sms_on.setText(msgs11.get("acc_contact_accept"));
    sms_on.setStyleName("mytxt3");
    email_on = new CheckBox();
    // email_on.setBoxLabel(msgs11.get("acc_contact_accept"));
    email_on.setText(msgs11.get("acc_contact_accept"));
    email_on.setStyleName("mytxt4");
    voice_on = new CheckBox();
    // voice_on.setBoxLabel(msgs11.get("acc_contact_accept"));
    voice_on.setText(msgs11.get("acc_contact_accept"));
    voice_on.setStyleName("mytxt3");
    afert_on = new CheckBox();
    // afert_on.setBoxLabel(msgs11.get("acc_contact_afert_accept"));
    afert_on.setText(msgs11.get("acc_contact_afert_accept"));
    afert_on.setStyleName("mytxt3");
    afert_on.addStyleName("cw-FlowPanel-checkBox");

    Button bsubmit = new Button(msgs11.get("acc_contact_submit"));

    bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>() {
      @Override
      public void componentSelected(ButtonEvent ce) {
        if (form.afert_on.getValue() == true) {
          GSControl.askdothis(new ClickHandler() // yes
              {
                public void onClick(ClickEvent event) {
                  GSControl.ShowProcessing();
                  GSControl.GetGS().Update_contacts(form.email.getText() + "@" + form.email2.getText(),
                      form.phone.getText(), form.phonevoice.getText(), form.email_on.getValue() ? 1 : 0,
                      form.sms_on.getValue() ? 1 : 0, form.voice_on.getValue() ? 1 : 0,
                      form.afert_on.getValue() ? 1 : 0,
                      form.date.getValue(),	  
                    		  
                    		  new AsyncCallback<update_contacts>() {
                        public void onFailure(Throwable caught) {
                          GSControl.HideProcessing();
                          GSControl.JournalNeedReFresh = true;
                          GSControl.ErrMsg(caught);
                          form.afert_on.setValue(false);
                          form.showForm();
                        }

                        public void onSuccess(update_contacts res) {
                          form.afert_on.setValue(false);
                          GSControl.HideProcessing();
                          GSControl.JournalNeedReFresh = true;
                          GSControl.myInfo.email = res.email;
                          GSControl.myInfo.email_on = res.email_on;
                          GSControl.myInfo.smsphone = res.smsphone;
                          GSControl.myInfo.sms_on = res.sms_on;
                          GSControl.myInfo.afert_on = res.aferta_on;
                          GSControl.myInfo.voicephone = res.voicephone;
                          GSControl.myInfo.voice_on = res.voice_on;
                          GSControl.myInfo.bday=res.bday;
                          GSControl.OkMsg(msgs11.get("acc_contact_saved"));
                          form.showForm();
                        };
                      });
                }
              }, null, null, null); // msg
        }
        else {
          GSControl.ErrMsg(msgs11.get("acc_contact_needafert"));
        }
      }
    });

    Label txt1 = new Label(msgs11.get("acc_contact_txt1"));
    Label txt2 = new Label(msgs11.get("acc_contact_txt2") + " ");
    txt2.setStyleName("mytxtJ");
    Label txt3 = new Label(msgs11.get("acc_contact_txt3"));
    txt3.setStyleName("mytxtJ");

    Label af1 = new Label(msgs11.get("acc_contact_afert1"));
    af1.setStyleName("mytxt3");

    Label sms1 = new Label(msgs11.get("acc_contact_phone"));
    sms1.setStyleName("mytxt3");
    Label sms2 = new Label(msgs11.get("acc_contact_phone2"));
    sms2.setStyleName("mytxt3");
    Label phn1 = new Label(msgs11.get("acc_contact_phone_voice"));
    phn1.setStyleName("mytxt3");
    Label phn2 = new Label(msgs11.get("acc_contact_phone_voice2"));
    phn2.setStyleName("mytxt3");
    Label eml1 = new Label(msgs11.get("acc_contact_email"));
    eml1.setStyleName("mytxt4");
    Label eml2 = new Label(msgs11.get("acc_contact_email2"));
    eml2.setStyleName("mytxt4");

    HTML af2 = new HTML(" <a href=\"" + msgs11.get("acc_contact_afert_lnk") + "\" target=blank>"
        + msgs11.get("acc_contact_afert_cap") + "<a/>");

    // HorizontalPanel hp1= new HorizontalPanel();//hp1.setWidth("100%");
    // HorizontalPanel hp2= new HorizontalPanel();hp2.setWidth("100%");
    // HorizontalPanel hp3= new HorizontalPanel();//hp3.setWidth("100%");

    // FlowPanel hp1= new FlowPanel();hp1.setWidth("100%");
    // hp1.setStylePrimaryName("");
    // FlowPanel hp3= new FlowPanel();//hp3.setWidth("100%");

    FlexTable hp1 = new FlexTable();
    hp1.setWidth("100%");
    FlexTable hp2 = new FlexTable();
    hp2.setWidth("100%");
    FlexTable hp3 = new FlexTable();
    hp3.setWidth("100%");

    Label l1 = new Label("+7");
    l1.setStyleName("mytxt3");
    Label l2 = new Label("+7");
    l2.setStyleName("mytxt3");
    // hp1.add(l1);
    // hp1.add(phonevoice);
    // hp2.add(email);
    // hp3.add(l2);
    // hp3.add(phone);

    hp1.setWidget(0, 0, l1);
    hp1.setWidget(0, 1, phonevoice);
    hp2.setWidget(0, 0, email);
    Label ll = new Label("@");
    ll.setStyleName("mytxt4");
    hp2.setWidget(0, 1, ll);
    hp2.setWidget(0, 2, email2);

    hp3.setWidget(0, 0, l2);
    hp3.setWidget(0, 1, phone);
    /**/

    VerticalPanel wp1 = new VerticalPanel();
    wp1.setHorizontalAlign(HorizontalAlignment.CENTER);
    wp1.add(hp1);
    VerticalPanel wp2 = new VerticalPanel();
    wp2.setHorizontalAlign(HorizontalAlignment.CENTER);
    wp2.add(hp2);
    VerticalPanel wp3 = new VerticalPanel();
    wp3.setHorizontalAlign(HorizontalAlignment.CENTER);
    wp3.add(hp3);
    wp1.add(phn2);
    wp2.add(eml2);
    wp3.add(sms2);

    // Grid grid = new Grid(5, 3);
    FlexTable grid = new FlexTable();

    // CellFormatter cellFormatter = grid.getCellFormatter();
    FlexCellFormatter cellFormatter = grid.getFlexCellFormatter();

    grid.setCellSpacing(1);
    grid.setCellPadding(1);
    cellFormatter.setColSpan(1, 0, 3);
    cellFormatter.setColSpan(3, 0, 3);
    cellFormatter.setWidth(0, 0, "250px");

    grid.setWidth("100%");
    grid.setWidget(0, 0, phn1);
    grid.setWidget(0, 1, voice_on);
    grid.setWidget(0, 2, wp1);
    grid.setWidget(2, 0, eml1);
    grid.setWidget(2, 1, email_on);
    grid.setWidget(2, 2, wp2);
    grid.setWidget(4, 0, sms1);
    grid.setWidget(4, 1, sms_on);
    grid.setWidget(4, 2, wp3);
    // cellFormatter.setStyleName(0, 1, "tdd0");
    // cellFormatter.setStyleName(2, 1, "tdd0");
    // cellFormatter.setStyleName(4, 1, "tdd0");

    cellFormatter.setAlignment(0, 2, HasHorizontalAlignment.ALIGN_LEFT, HasVerticalAlignment.ALIGN_MIDDLE);
    cellFormatter.setAlignment(2, 2, HasHorizontalAlignment.ALIGN_LEFT, HasVerticalAlignment.ALIGN_MIDDLE);
    cellFormatter.setAlignment(4, 2, HasHorizontalAlignment.ALIGN_LEFT, HasVerticalAlignment.ALIGN_MIDDLE);

    wd = new VerticalPanel();
    wd.setHorizontalAlign(HorizontalAlignment.CENTER);

    FlowPanel flowPanel = new FlowPanel();
    flowPanel.setWidth("100%");
    flowPanel.add(txt1);
    wd.add(flowPanel);

    wd.add(new HTML("<hr>"));

    HorizontalPanel afhp1 = new HorizontalPanel();// afhp1.setWidth("100%");
    wd.add(afhp1);
    afhp1.add(af1);
    afhp1.add(af2);
    FlowPanel flowPanel2 = new FlowPanel();
    flowPanel2.setWidth("100%");
    flowPanel2.add(afert_on);
    wd.add(flowPanel2);

    wd.add(new HTML("<hr>"));

    FlowPanel flowPanel3 = new FlowPanel();
    flowPanel3.setWidth("100%");
    flowPanel3.add(txt2);
    wd.add(flowPanel3);

    wd.add(new HTML("<hr>"));
    wd.add(grid);
    wd.add(new HTML("<hr>"));
    
    HorizontalPanel hp4= new HorizontalPanel();hp4.setWidth("100%");
    HorizontalPanel hp5= new HorizontalPanel();hp5.setWidth("100%");
    
    Label llll;
    
    if (GSControl.myInfo.IsCorp!="Y")
    {
    	llll=new Label(msgs11.get("acc_contact_bday"));
    	llll.setStyleName("mytxt3");
    	hp5.add(llll);
    	hp5.add(date);
    	wd.add(hp5);
    };
    wd.add(new HTML("<hr>"));
    wd.add(txt3);
    wd.add(new HTML("<hr>"));
    this.addButton(bsubmit);
    // wd.add(new HTML("<hr>"));
    l3 = new Label();
    l3.setStyleName("mytxt3");
    // Label l4=new Label();l4.setStyleName("mytxt3");
    wd.add(l3);
    // wd.add(l4);
    this.add(wd);
  }

  public void showForm() {
      form.email.setText("");
      form.email2.setText("");
	  
    if (GSControl.myInfo.email != null) {
      String[] ss = GSControl.myInfo.email.split("@", 2);
      if (ss != null) {
        form.email.setText(ss[0]);
        form.email2.setText(ss[1]);
      }

    };
    
    
    form.l3.setText(GSControl.myInfo.account + ": " + GSControl.myInfo.name + "    (" + new Date().toString() + ")");
    form.phone.setText(GSControl.myInfo.smsphone);
    form.phonevoice.setText(GSControl.myInfo.voicephone);
    sms_on.setValue(GSControl.myInfo.sms_on == 1 ? true : false);
    email_on.setValue(GSControl.myInfo.email_on == 1 ? true : false);
    form.voice_on.setValue(GSControl.myInfo.voice_on == 1 ? true : false);
    form.afert_on.setValue(GSControl.myInfo.afert_on == 1 ? true : false);
    form.date.setValue(GSControl.myInfo.bday);
    form.show();
  }
}
