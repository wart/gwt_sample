package gwtb.client.modules;

import gwtb.client.GSControl;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class LKpwdrecover extends Window {
	  private static TreeMap<String, String> msgs31 = null;
	  private static LKpwdrecover form = null;

	  public static void Run() {
	    if (msgs31 == null) {
	      GSControl.ShowWait();
	      GSControl.GetGS().getMsgList(31, new AsyncCallback<TreeMap<String, String>>() {

	        public void onFailure(Throwable caught) {
	          GSControl.HideWait();
	          GSControl.ErrMsg(caught);
	        }

	        public void onSuccess(TreeMap<String, String> res) {
	        	msgs31 = res;
	          GSControl.HideWait();
	          Create();
	        }
	      });
	    }
	    else {
	      Create();
	    }
	  }

	  // =======================================================================
	  
	  private static void Create() {
	    if (form == null) {
	      form = new LKpwdrecover();
	    }
	    form.acc.setValue("");
	    form.email.setValue("");
	    form.show();
	  }
	  // =======================================================================
	  
	  public TextField<String>	acc;
	  public TextField<String>	email;
	  
	   public LKpwdrecover()
	   {
		   this.setWidth("460px");
		   this.setPlain(true);
		   this.setModal(GSControl.formsmodal);
		   this.setBlinkModal(true);
		   this.setHeading(msgs31.get("lkprec_title"));
		   this.setResizable(false);
		   FormLayout lay = new FormLayout();
		   lay.setLabelWidth(200);
		   
		   this.setLayout(lay);
		   
		   

		   FormData formData = new FormData("-24");
		   acc   = new TextField<String>();acc  .setFieldLabel(msgs31.get("lkprec_acc"  ));acc  .setAutoValidate(true);acc  .setAllowBlank(false);  
		   email = new TextField<String>();email.setFieldLabel(msgs31.get("lkprec_email"));email.setAutoValidate(true);email.setAllowBlank(false);
		   
		   
		   this.add(new Html("<br>"));
		   this.add(new Label(msgs31.get("lkprec_txt")));
		   this.add(new Html("<br>"));
		   this.add(acc,formData);
		   this.add(email,formData);
		   Button bsubmit = new Button(msgs31.get("lkprec_submit"));
		   this.addButton(bsubmit);
		    bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>()
		    		{
		    			@Override
		    			public void componentSelected(ButtonEvent ce)
		    			{
		    				if(form.acc.isValid() && form.email.isValid() && (form.acc.getValue().length()>4) &&(form.email.getValue().length()>4))
		    		    	{
		    					GSControl.askdothis(new ClickHandler() // yes
		  		                {
		  		                public void onClick(ClickEvent event) {
		  		                  GSControl.ShowProcessing();
		  		                  GSControl.GetGS().Recovery_LKPass(form.acc.getValue(), form.email.getValue(), 
		  		                		  						   
		  	               		  new AsyncCallback<String>() {
		  		                        public void onFailure(Throwable caught) {
		  		                          GSControl.HideProcessing();
		  		                          GSControl.JournalNeedReFresh = true;
		  		                          GSControl.ErrMsg(caught);
		  		                        }
		  		                        public void onSuccess(String res) {
		  		                          GSControl.HideProcessing();
		  		                          GSControl.JournalNeedReFresh = true;
		  		                          GSControl.OkMsg(res);
		  		                          form.hide();
		  		                        };
		  		                      });
		  		                }
		  		              }, null, null, null);
		    		    	}else
		    		    	{
		    		    		GSControl.ErrMsg(msgs31.get("lkprec_formerr"));
		    		    	}
		    			}
		    		});
	   };
}
