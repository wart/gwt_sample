package gwtb.client.modules;

import gwtb.client.GSControl;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextBox;

public class checktexh extends Window {

  // ============================================================================//
  // * * * TECH CHECK * * * //
  // ============================================================================//
  public static TreeMap<String, String> msgs5 = null; // bron form msgs

  // ============================================================================//
  public static void Run() {
    // Window.alert("LoadMsgs4_bron_form()");
    if (msgs5 == null) {
      // final MessageBox box = MessageBox.wait("Progress","Waiting Server Responce...", "Please wait");
      GSControl.ShowWait();
      GSControl.GetGS().getMsgList(5, new AsyncCallback<TreeMap<String, String>>() {
        public void onFailure(Throwable caught) {
          GSControl.HideWait();
          GSControl.ErrMsg(caught);
        }

        public void onSuccess(TreeMap<String, String> res) {
          msgs5 = res;
          GSControl.HideWait();
          Create();
        };
      });
    }
    else {
      Create();
    }
  }

  // =======================================================================
  static checktexh singchecktexh_form = null;

  private static void Create() {
    // Window.alert("create_bron_form()");
    if (singchecktexh_form == null) {
      singchecktexh_form = new checktexh();
    }
    singchecktexh_form.show();
  }

  // =======================================================================

  Button bsubmit;
  public TextBox phone;
  public VerticalPanel wd;

  public checktexh() {
    this.setSize(450, 280);
    this.setPlain(true);
    this.setModal(GSControl.formsmodal);
    this.setBlinkModal(true);
    this.setHeading(msgs5.get("adslyes_title"));

    bsubmit = new Button(msgs5.get("adslyes_submit"));
    bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>() {
      boolean isDigit(char c) {
        switch (c) {
          case '0':
            ;
          case '1':
            ;
          case '2':
            ;
          case '3':
            ;
          case '4':
            ;
          case '5':
            ;
          case '6':
            ;
          case '7':
            ;
          case '8':
            ;
          case '9':
            return true;
        }
        return false;
      }

      @Override
      public void componentSelected(ButtonEvent ce) {
        String phn = singchecktexh_form.phone.getText();
        int len = phn.length();
        if (len != 10) {
          GSControl.ErrMsg(msgs5.get("adslyes_err1txt"));
          return;
        }
        ;
        for (int j = 0; j < len; ++j) {
          if (isDigit(phn.charAt(j)) != true) {

            GSControl.ErrMsg(msgs5.get("adslyes_err2txt"));
            return;
          }
        }
        ;
        GSControl.GetGS().CheckADSLTech(phn, new AsyncCallback<String>() {
          public void onFailure(Throwable caught) {
            GSControl.ErrMsg(caught);
          }

          public void onSuccess(String res) {
            GSControl.OkMsg(res);
          };
        });
      }
    });

    phone = new TextBox();
    phone.setText("353xxxxxxx");
    wd = new VerticalPanel();
    wd.addText(msgs5.get("adslyes_txt")).setStyleName("mytxt3");
    wd.setHorizontalAlign(HorizontalAlignment.CENTER);
    wd.add(phone);
    this.addButton(bsubmit);
    this.add(wd);
  }
}
