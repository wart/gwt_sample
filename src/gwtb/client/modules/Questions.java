package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.wrkitem;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
public class Questions  extends Window
{

	private static TreeMap<Integer,wrkitem> wrklst6=null;
	private static TreeMap<Integer,wrkitem> wrklst7=null;
	private static TreeMap<Integer,wrkitem> wrklst8=null;
	private static TreeMap<String,String> msgs13=null;
	private static Questions form=null;
	
	public static void Run()
	{
		if (wrklst6==null)
		{
			GSControl.ShowWait();
			GSControl.GetGS().getWRKList(6,
			new AsyncCallback<TreeMap<Integer,wrkitem>>()
			{
				public void onFailure(Throwable caught)
				{
					GSControl.HideWait();
			    	  GSControl.ErrMsg(caught);
				}
				public void onSuccess(TreeMap<Integer,wrkitem> res)
				{
					wrklst6=res;
					GSControl.HideWait();
					Load();
				};
			});
		}else
		{Load();};
	}				
	//============================================================================//	

	private static void Load()
	{
		if (wrklst7==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getWRKList(7,
				new AsyncCallback<TreeMap<Integer,wrkitem>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught);
						
					}
					public void onSuccess(TreeMap<Integer,wrkitem> res)
					{
						wrklst7=res;
						GSControl.HideWait();
						Load2();
					};
				});
		}else
		{Load2();};
	}
	//============================================================================//	
	private static void Load2()
	{
		if (wrklst8==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getWRKList(8,
				new AsyncCallback<TreeMap<Integer,wrkitem>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught);
					}
					public void onSuccess(TreeMap<Integer,wrkitem> res)
					{
						wrklst8=res;
						GSControl.HideWait();
						Load3();
					};
				});
		}else
		{Load3();};
	}			
	//============================================================================//	
	private static void Load3()
	{
		if (msgs13==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(13,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught);
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs13=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{Create();};
	}		
	//=======================================================================	
	static private void Create()
	{
		if (form==null)
		{
			form= new Questions();
		};
		form.hp1.clear();form.hp1.add(new Label(msgs13.get("oth_voprosi_fio")));	form.hp1.add(new Label(GSControl.myInfo.name));
		form.hp2.clear();form.hp2.add(new Label(msgs13.get("oth_voprosi_acc")));	form.hp2.add(new Label(GSControl.myInfo.account));
		form.hp3.clear();form.hp3.add(new Label(msgs13.get("oth_voprosi_email")));	form.hp3.add(new Label(GSControl.myInfo.email));
		form.hp4.clear();form.hp4.add(new Label(msgs13.get("oth_voprosi_phone")));	form.hp4.add(new Label(GSControl.myInfo.smsphone));
		form.show();
	}	
	//=======================================================================	
		public Button bsubmit;
		public TextBox title;
		public TextArea body;
		public VerticalPanel wd;
		public ListBox svc;
		public ListBox type;
		public HorizontalPanel hp6;
		HorizontalPanel hp4;
		HorizontalPanel hp2;
		HorizontalPanel hp1;
		HorizontalPanel hp3;
		HorizontalPanel hp5;
		public Questions()
		{
		//	this.setSize(500, 300);
			this.setWidth(500);
			this.setPlain(true);   
			this.setModal(GSControl.formsmodal);   
			this.setBlinkModal(true);   
			this.setHeading(msgs13.get("oth_voprosi_title")); 					
			
			title = new TextBox();

		
			bsubmit = new Button(msgs13.get("oth_voprosi_submit"));
			
			body = new TextArea();
			body.ensureDebugId("cwBasicText-textarea");
			body.setWidth("100%");
			body.setVisibleLines(7);
			
			wd=new VerticalPanel();wd.setHorizontalAlign(HorizontalAlignment.CENTER);
			
			//wd.setWidth(width);
			
			
			
			
			wd.add(new HTML("<br><a href=\""+msgs13.get("oth_voprosi_faqlnk")+"\" target=blank>"+msgs13.get("oth_voprosi_faqcap")+"<a/><br><br>"));
			
			Label txt1=new Label(msgs13.get("oth_voprosi_txt1"));
				txt1.setStyleName("mytxt");
			wd.add(txt1);
			wd.add(new HTML("<hr>"));
			 hp1= new HorizontalPanel();
			 hp2= new HorizontalPanel();
			hp3= new HorizontalPanel();
			hp4= new HorizontalPanel();
			wd.setHorizontalAlign(HorizontalAlignment.LEFT);
			wd.add(new Label(msgs13.get("oth_voprosi_sved")));

			wd.add(hp1);
			wd.add(hp2);
			wd.add(hp3);
			wd.add(hp4);
			wd.add(new HTML("<hr>"));
			hp5= new HorizontalPanel();
			hp6= new HorizontalPanel();
			hp5.add(new Label(msgs13.get("oth_voprosi_svc")));
			
			wd.add(hp5);wd.add(hp6);
			
			svc=GSControl.buildWRKListBox(wrklst8);
			hp5.add(svc);
			svc.addChangeHandler(new ChangeHandler()
			{

				@Override
				public void onChange(ChangeEvent event) {
					form.change();
				}
			
			});
			change();
			
			wd.setHorizontalAlign(HorizontalAlignment.CENTER);
			wd.add(new HTML("<hr>"));
			wd.add(new Label(msgs13.get("oth_voprosi_theme")));
			wd.add(title);
			wd.add(new Label(msgs13.get("oth_voprosi_body")));
			wd.add(body);
//			wd.add(Errmsg);
			this.addButton(bsubmit);
			this.add(wd);
			
			
			bsubmit.addSelectionListener( 
					 new SelectionListener<ButtonEvent>() {   
					      @Override  
					    public void componentSelected(ButtonEvent ce)
						{
					    	  if(GSControl.myInfo.canSendQuestion()==true)
								{
								  if ((form.title.getText().length()>5)&&
								     (form.body.getText().length()>10))
								  {
									  GSControl.askdothis(new ClickHandler()  //yes
										  {
									          public void onClick(ClickEvent event)
									          {
									        	  GSControl.ShowProcessing();
									        	  
									        	  GSControl.GetGS().SendQuestion(
									        			  form.title.getText(),
									        			  form.body.getText(),
									        			  form.svc.getSelectedIndex(),
									        			  form.type.getSelectedIndex(),
									        			  new AsyncCallback<String>()
													{
										        		public void onFailure(Throwable caught)
										        		{
										        			GSControl.HideProcessing();
										        			GSControl.JournalNeedReFresh=true;
										        			GSControl.ErrMsg(caught);
														}
														public void onSuccess(String tmp)
														{
															GSControl.HideProcessing();
										        			GSControl.JournalNeedReFresh=true;
										        			GSControl.OkMsg(msgs13.get("oth_voprosi_sended"));															
															form.title.setText("");
															form.body.setText("");
														};
													});
									          };
								          },null,null,null);   //msg
								  }else{
									  GSControl.ErrMsg(msgs13.get("oth_voprosi_errdata"));
								  }
								  }else{
									  GSControl.ErrMsg(msgs13.get("oth_voprosi_errdata2"));
								  }			  
						}
					 });
		}
		public void change()
		{
			if(svc.getSelectedIndex()>0)
			{
				type=GSControl.buildWRKListBox(wrklst7);			
			}else{
				type=GSControl.buildWRKListBox(wrklst6);	
			};
			hp6.clear();
			hp6.add(new Label(msgs13.get("oth_voprosi_type")));
			hp6.add(type);			
		}
}
//=======================================================================
