package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.MD_search_userData;
import gwtb.client.rpcdata.abon_profile;
import gwtb.client.rpcdata.search_userData;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TextBox;

public class SearchAbon extends Window
{
	private static TreeMap<String,String> msgs20=null;
	private static SearchAbon form=null;
	public static void Run()
	{
		if (msgs20==null)
		{
			GSControl.ShowWait();
	        GSControl.GetGS().getMsgList(20,
			new AsyncCallback<TreeMap<String,String>>()
			{
				public void onFailure(Throwable caught)
				{
					GSControl.HideWait();
					GSControl.ErrMsg(caught); 
				}
				public void onSuccess(TreeMap<String,String> res)
				{
					msgs20=res;
					GSControl.HideWait();
					Create();
				};
			});
		}else
		{Create();};
	}		
	//=======================================================================	
	public static void Create()
	{
		//Window.alert("create_bron_form()");
		if (form==null)
		{
			form= new SearchAbon();
		};
		form.show();
	}	
	//============================================================================//
	public TextBox acc;
	public TextBox dev;
	public TextBox name;
	private ListStore<MD_search_userData> SearchData=null;
	private ColumnModel cm ;
	public Grid<MD_search_userData> grid;	
	public SearchAbon()
	{
		 this.setSize(600, 500);
		 this.setPlain(true);
		 this.setModal(GSControl.formsmodal);
		 this.setBlinkModal(true);
		 this.setHeading(msgs20.get("abnsrch_title"));		
		 
		 SearchData= new ListStore<MD_search_userData>();
		 
		acc = new TextBox();acc.setWidth("300");
		dev = new TextBox();dev.setWidth("300");
		name= new TextBox();name.setWidth("300");
		Label l1= new Label(msgs20.get("abnsrch_acc"));
		Label l2= new Label(msgs20.get("abnsrch_phn"));
		Label l3= new Label(msgs20.get("abnsrch_name"));
	    FlexTable t = new FlexTable();
	    t.setWidth("100%");
	    t.setWidget(0,0,l1);
	    t.setWidget(1,0,l2);
	    t.setWidget(2,0,l3);
	    t.setWidget(0,1,acc);
	    t.setWidget(1,1,dev);
	    t.setWidget(2,1,name);
	    
	    
	    this.add(t);
	    this.add(new Html("<br>"));
		
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();   
		  
	    ColumnConfig column = new ColumnConfig();   
	    column.setId("account");   
	    column.setHeader(msgs20.get("abnsrch_acc"));   
	    column.setWidth(100);   
	    configs.add(column);   
	    
	    column = new ColumnConfig();   
	    column.setId("dev_id");   
	    column.setHeader(msgs20.get("abnsrch_phn"));   
	    column.setWidth(100);   
	    configs.add(column);     
	    
	    column = new ColumnConfig();   
	    column.setId("name");   
	    column.setHeader(msgs20.get("abnsrch_name"));   
	    column.setWidth(100);   
	    configs.add(column);   
	    
	    cm = new ColumnModel(configs);

	    grid = new Grid<MD_search_userData>(SearchData, cm);
	    grid.setStyleAttribute("borderTop", "none");   
	    grid.setAutoExpandColumn("name");   
	    grid.setBorders(true);   
	    grid.setStripeRows(true);		
	    grid.setHeight(300);
	    this.add(grid);
	    
	    Button search= new Button(msgs20.get("abnsrch_search"));
	    search.addSelectionListener(new SelectionListener<ButtonEvent>() {
	         @Override
	         public void componentSelected(ButtonEvent ce){
	        	 if (((form.acc .getText()!=null)&(!form.acc .getText().equalsIgnoreCase("")))||
	        	     ((form.dev .getText()!=null)&(!form.dev .getText().equalsIgnoreCase("")))||
	        	     ((form.name.getText()!=null)&(!form.name.getText().equalsIgnoreCase(""))))
	        	 {
	        		 GSControl.ShowProcessing();
	        		 GSControl.GetGS().search_user(form.acc .getText(), form.name.getText(), form.dev .getText(), new AsyncCallback<Vector<search_userData>>()
	        				 {
			        		public void onFailure(Throwable caught)
			        		{
			        			GSControl.HideProcessing();
			        			GSControl.ErrMsg(caught);
							}
							public void onSuccess(Vector<search_userData> tmp)
							{
								GSControl.HideProcessing();
								int sz=tmp.size();
								search_userData b;
								form.SearchData.removeAll();
								for (int j=0;j<sz;++j)
								{
									b= tmp.get(j);
									if (b!=null)
										form.SearchData.add(
												new MD_search_userData(b.user_id,b.account,b.name,b.dev_id));
								};
							};});
	        	 };
	         }
	    });
	    Button select= new Button(msgs20.get("abnsrch_select"));
	    select.addSelectionListener(new SelectionListener<ButtonEvent>() {
	         @Override
	         public void componentSelected(ButtonEvent ce)
	         {
	        	 if (form.grid.getSelectionModel()!=null)
	        		 if (form.grid.getSelectionModel().getSelectedItem()!=null)
	        			{
	        			 	GSControl.ShowProcessing();
	        			 	GSControl.GetGS().SelectUser(form.grid.getSelectionModel().getSelectedItem().user_id,
	        			 			
	        			 			 new AsyncCallback<abon_profile>()
	    	        				 {
	        			 				public void onFailure(Throwable caught)
	        			        		{
	        			        			GSControl.HideProcessing();
	        			        			GSControl.ErrMsg(caught);
	        							}
	        							public void onSuccess(abon_profile tmp)
	        							{
	        								GSControl.HideProcessing();
	        								GSControl.SetAbon(tmp);
	        								form.hide();
	        							}
	    	        				 });
	        			 	return;
	        			};
	        			GSControl.OkMsg(msgs20.get("abnsrch_needselect"));
	         }
	    });
	    
	    this.addButton(search);
	    this.addButton(select);
	}
	//============================================================================//
	
//============================================================================//
}
