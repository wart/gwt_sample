package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.DVOclipart;
import gwtb.client.rpcdata.MD_DVOclipart;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.Info;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;

public class DVO  extends Window
{
	//=======================================================================	
	private static TreeMap<String,String> msgs9=null;
	private static DVO form=null;
	//=======================================================================	
	private ListStore<MD_DVOclipart> DVOData=null;
	
	public ListBox Phones;
	EditorGrid<MD_DVOclipart> grid;
	ColumnModel cm;
	Button submit;	
	Button bclean;
	Label l1,l2,l3;
	HTML h1,h2;
	public CheckBox aferta;
	String DVOCHtxtON;
	String DVOCHtxtOFF;
	String phon;
	String DVOCH;
	//=======================================================================	
	public static void Run()
	{
		if (msgs9==null)
		{
				GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(9,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs9=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{Create();};
	}		
	//=======================================================================	
	private static void Create()
	{
		if (form==null)
		{
			form= new DVO();
		};
		form.Phones  =GSControl.buildPhoneListBox(form.Phones,2);
		form.show();
	}
	//=======================================================================	
	private void update() {
		DVOData.removeAll();
		int ph=Phones.getSelectedIndex();
		if(ph>=0)
		{
			Vector<DVOclipart> res = GSControl.myInfo.phones.get(ph).dvo;
			if (res!=null)
			{		
				int sz=res.size();
				int j;
				DVOclipart b;
				for (j=0;j<sz;++j)
				{
					b= res.get(j);
					if (b!=null)
						DVOData.add(new  MD_DVOclipart(b.ID,b.NAME,b.ISON,b.SRVID,b.chst,b.chdate));
				};
				grid.recalculate();				
			};
		};
	}
	//=======================================================================
	public DVO()
	{
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading(msgs9.get("ph_DVO_title"));
		this.setSize(650,470);

		DVOData=new ListStore<MD_DVOclipart>();		
				
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();   
	    ColumnConfig 
	    column = new ColumnConfig(); column.setId("NAME" );column.setHeader(msgs9.get("ph_DVO_tt_1"));column.setWidth(150);configs.add(column);   
	    column = new ColumnConfig(); column.setId("SRVID" );column.setHeader(msgs9.get("ph_DVO_tt_2"));column.setWidth(150);
	    
	    column.setRenderer(new GridCellRenderer<MD_DVOclipart>() {   
			@Override
			public Object render(MD_DVOclipart model, String property,
					ColumnData config, int rowIndex, int colIndex,
					ListStore<MD_DVOclipart> store, Grid<MD_DVOclipart> grid) {
				
				Integer val = (Integer) model.get(property);   
   
	              return "<span class=mytxt"+((val!=0)?"G":"R")+" >"+msgs9.get((val!=0)?"ph_DVO__yes":"ph_DVO__no")+
	              "</span>";   
			}   
	          });   
	    configs.add(column);
	    
	    column = new ColumnConfig(); column.setId("chdate");column.setHeader(msgs9.get("ph_DVO_tt_3"));column.setWidth(170);
	    column.setRenderer(new GridCellRenderer<MD_DVOclipart>() {   
			@Override
			public Object render(MD_DVOclipart model, String property,
					ColumnData config, int rowIndex, int colIndex,
					ListStore<MD_DVOclipart> store, Grid<MD_DVOclipart> grid) {
				Boolean val = (Boolean) model.get("chst_old");
	              return "<span class=mytxt"+((val)?"G":"R")+" >"+
	              msgs9.get((val)?"ph_DVO__yes":"ph_DVO__no")+
	              "("+model.get(property)+")</span>";   
			}   
	          });   
	    	
	    configs.add(column);
	    CheckColumnConfig checkColumn = new CheckColumnConfig("chst",msgs9.get("ph_DVO_tt_4"), 160);
	    CellEditor checkBoxEditor = new CellEditor(new CheckBox());checkColumn.setEditor(checkBoxEditor);configs.add(checkColumn);   	    
	    
	    cm = new ColumnModel(configs);
	    
	    grid = new EditorGrid<MD_DVOclipart>(DVOData, cm);
	    grid.setStyleAttribute("borderTop", "none");   
	    grid.setAutoExpandColumn("NAME");   
	    grid.setBorders(true);   
	    grid.setStripeRows(true);
	    
		aferta  = new CheckBox();aferta.setBoxLabel(msgs9.get("ph_DVO_yes"));
		submit  = new Button  (msgs9.get("ph_DVO_submit"));
		submit.addSelectionListener(
	    		new SelectionListener<ButtonEvent>()
	    		{   
	    			public void componentSelected(ButtonEvent ce)
	    			{  
	    			//	Info.display("componentSelected", "start");
	    				String Adds="";
	    				String Dels="";
	    				DVOCH="";
	    				DVOCHtxtON="";
	    				DVOCHtxtOFF="";			    				
	    			    for (Record r : DVOData.getModifiedRecords())
	    			    {
	    			    	if ((Boolean) r.get("chst"))
	    			    	{
								
								if(DVOCHtxtON.length()>0)DVOCHtxtON+=",";DVOCHtxtON+=r.get("NAME");
								if(Adds      .length()>0)Adds      +=",";Adds+=r.get("ID");
	    			    	}else{
								if(DVOCHtxtOFF.length()>0)DVOCHtxtOFF+=",";DVOCHtxtOFF+=r.get("NAME");
								if(Dels       .length()>0)Dels       +=",";Dels+=r.get("ID");
	    			    	};
	    			    };
						if ((Adds.length()>0)||(Dels.length()>0))
						{
							DVOCH=Adds+";"+Dels;
						};	    			    
	    				
					//	Info.display("componentSelected", "DVOCH " +DVOCH);
						
						int ph=form.Phones.getSelectedIndex();
						if(ph>=0)
						{
							form.phon=GSControl.myInfo.phones.get(ph).phone;
							if (form.phon!=null)
							{
								if(form.phon.length()>0)
								{
									if(DVOCH!=null)
									{
										if(DVOCH.length()>1)
										{
											if (form.aferta.getValue()==false)
											{
												GSControl.ErrMsg(msgs9.get("ph_DVO_ErrAferta"));
												return;
											};								
									//--------
											GSControl.askdothis(new ClickHandler()  //yes
											{
												public void onClick(ClickEvent event)
												{
													GSControl.ShowWait();
									        	  
													GSControl.GetGS().DVOChange(form.phon,form.DVOCH,
															new AsyncCallback<Vector<DVOclipart>>()
															{
																public void onFailure(Throwable caught)
																{
																	GSControl.HideWait();
																	GSControl.ErrMsg(caught); 
																	GSControl.JournalNeedReFresh=true;
																}
																public void onSuccess(Vector<DVOclipart> res)
																{
																	GSControl.HideWait();
																	GSControl.JournalNeedReFresh=true;
																	GSControl.myInfo.phones.get(form.Phones.getSelectedIndex()).dvo=res;
																	GSControl.OkMsg( msgs9.get("ph_DVO_ok")); 
																	form.aferta.setValue(false);
																	update();
																};
															}
													);
									        	  };
											},null,msgs9.get("ph_DVO_askon")+"\n\r"+
									        	  form.DVOCHtxtON+"\n\r\n\r"+
									        	  msgs9.get("ph_DVO_askoff")+"\n\r"+
									        	  form.DVOCHtxtOFF,null);
										}else{
											Info.display("Error", "nothing to change");   
										}
									};//else{Info.display("Error", "4");   }
								};//else{Info.display("Error", "3"); }
							};//else{Info.display("Error", "2"); }
						};//else{Info.display("Error", "1"); }							
	    			}
	    		});
		bclean  = new Button  (msgs9.get("ph_DVO_clean"));
		bclean.addSelectionListener(
	    		new SelectionListener<ButtonEvent>()
	    		{   
	    			public void componentSelected(ButtonEvent ce)
	    			{   
	    				form.update();
	    			}
	    		});
		
	    Phones  =  new ListBox();
	    
		Phones.addChangeHandler(new ChangeHandler(){
			@Override
			public void onChange(ChangeEvent event)
			{
				form.update();
			}
		});			    
	    l1 = new Label(msgs9.get("ph_DVO_txt1"));l1.setAutoWidth(true);
	    l2 = new Label(msgs9.get("ph_DVO_txt2"));l2.setAutoWidth(true);
	    l3 = new Label(msgs9.get("ph_DVO_txt3"));l3.setAutoWidth(true);
	    h1 = new HTML("<a href=\""+msgs9.get("ph_DVO_lnk_lnk")+"\" target=blank>"+msgs9.get("ph_DVO_lnk_cap")+"<a/>");
		h2 = new HTML("<a href=\""+msgs9.get("ph_DVO_af_lnk")+"\" target=blank>"+msgs9.get("ph_DVO_af_cap" )+"<a/>");
	    
	    
	    setLayout(new FitLayout());   
	    
	    FormPanel panel = new FormPanel();   
	    panel.setBorders   (false);   
	    panel.setBodyBorder(false);   
	    panel.setLabelWidth(55);   
	    panel.setPadding   (5);   
	    panel.setHeaderVisible(false);
	    
	    panel.add(l1    , new FormData("100%"));
	    panel.add(new HTML("<br>"), new FormData("100%"));
	    panel.add(l2    , new FormData("100%"));
	    panel.add(Phones, new FormData("100%"));
	    panel.add(new HTML("<br>"), new FormData("100%"));
	    panel.add(l3    , new FormData("100%"));
	    
	    grid.setHeight(200);
	    panel.add(grid, new FormData("100% -200"));	    

	    VerticalPanel vp = new VerticalPanel();
	    vp.setHorizontalAlign(HorizontalAlignment.CENTER);
	    vp.add(h1);
	    vp.add(new HTML("<br>"));
	    vp.add(aferta);
	    vp.add(h2);
	    panel.add(vp, new FormData("100%"));	
		add(panel);
		this.setMaximizable(true);
		addButton(submit);
		addButton(bclean);
		update();
	}
	//=======================================================================
	
	
	//=======================================================================
}
