package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.wrkitem;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;



public class changePhoneTP  extends Window
{
	private static TreeMap<Integer,wrkitem> wrklst2=null; //phone tp list
	private static TreeMap<String,String> msgs7=null;
	private static changePhoneTP form=null;
	
	public static void Run()
	{
		//Window.alert("LoadMsgs4_bron_form()");
		if (wrklst2==null)
		{
			    GSControl.ShowWait();
		        GSControl.GetGS().getWRKList(2,
				new AsyncCallback<TreeMap<Integer,wrkitem>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught);
					}
			
					public void onSuccess(TreeMap<Integer,wrkitem> res)
					{
						wrklst2=res;
						GSControl.HideWait();
						Load();
					};
				});
		}else
		{Load();};
	}			
	//============================================================================//	
	private static void Load()
	{

		if (msgs7==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(7,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught.getMessage());
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs7=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{Create();};
	}		
	
	//============================================================================//
	private static  void Create()
	{
		//Window.alert("create_bron_form()");
		if (form==null)
		{
			form= new changePhoneTP();
		};
		form.Phones=GSControl.buildPhoneListBox(form.Phones,1);
		form.CORTP.setText(form.getPhoneTPName(GSControl.myInfo.phones.get(0).tp));
		form.show();
	}	
	//============================================================================//
    public String getPhoneTPName(int id)
    {
    	int sz=wrklst2.size();
    	for (int j=0;j<sz;++j)
    	  if (wrklst2.get(j).id==id)
    		  return wrklst2.get(j).cap;
    	return "";
    };
	//============================================================================//
		Button bsubmit;
		ListBox TP;
		ListBox Phones;
		Label CORTP;
		public String msggg;
		public CheckBox aferta;
		public VerticalPanel wd;

		public changePhoneTP()
		{
		//	this.setSize(500, 300);  
			this.setWidth(284);
			this.setPlain(true);   
			this.setModal(GSControl.formsmodal);   
			this.setBlinkModal(true);   
			this.setHeading(msgs7.get("ph_TP_title")); 	
			this.setResizable(false);
			
			TP      =     GSControl.buildWRKListBox(wrklst2);
			Phones  = new ListBox();
			CORTP   = new Label();
			aferta  = new CheckBox();aferta.setBoxLabel(msgs7.get("ph_TP_yes"));
			bsubmit = new Button(msgs7.get("ph_TP_submit"));


			bsubmit.addSelectionListener( 
					 new SelectionListener<ButtonEvent>() {   
					      @Override  
					    public void componentSelected(ButtonEvent ce)
						{
					    		if (form.aferta.getValue()==false)
								{
					    			GSControl.ErrMsg(msgs7.get("ph_TP_ErrAferta"));
									return;
								};
								
							
								msggg=msgs7.get("ph_TP_ask")+"\""+form.CORTP.getText()+"\""+msgs7.get("ph_TP_ask1")+"\""+
								form.TP.getItemText(form.TP.getSelectedIndex())+"\""+msgs7.get("ph_TP_ask2")+
								   form.Phones.getItemText(form.Phones.getSelectedIndex())+"?";  
								
								GSControl.askdothis(new ClickHandler()  //yes
								  {
							          public void onClick(ClickEvent event)
							          {
							        	  int phi=form.Phones.getSelectedIndex();
							        	  int tpi=form.TP.getSelectedIndex();
							        	  
							        	  GSControl.ShowProcessing();	
							        	  
							        	  GSControl.GetGS().ChangePhoneTP(GSControl.myInfo.phones.get(phi).phone,wrklst2.get(tpi).id, new AsyncCallback<String>(){
							        		  public void onFailure(Throwable caught)
								        		{
								        			GSControl.HideProcessing();
								        			GSControl.JournalNeedReFresh=true;
								        			GSControl.ErrMsg(caught);
												}
							        			public void onSuccess(String res)
												{
							        				GSControl.HideProcessing();
								        			GSControl.JournalNeedReFresh=true;
								        			GSControl.OkMsg(res+=msgs7.get("ph_TP_ret2")+"\""+
							        						form.TP.getItemText(form.TP.getSelectedIndex())+"\""+
							        						   msgs7.get("ph_TP_ret3")+GSControl.myInfo.date_addmonth);
												};
							        	  });};},null,
							        	  msggg
							        	  ,null);									
						}
					 });
			
			wd= new VerticalPanel();
			wd.setHorizontalAlign(HorizontalAlignment.CENTER);
			wd.add(new Label(msgs7.get("ph_TP_txt2")));
		    wd.add(Phones);
		    wd.add(new Label(msgs7.get("ph_TP_cortp")));
		    wd.add(CORTP);
		    wd.add(new HTML("<hr>"));
			wd.add(new HTML("<a href=\""+msgs7.get("ph_TP_lnk_lnk")+"\" target=blank>"+msgs7.get("ph_TP_lnk_cap")+"<a/>"));
			wd.add(new Label(msgs7.get("ph_TP_txt1")));
		    wd.add(TP);
		    //wd.clear();
		    wd.add(new HTML("<hr>"));
			wd.add(aferta);
			wd.add(new HTML("<a href=\""+msgs7.get("ph_TP_af_lnk")+"\" target=blank>"+msgs7.get("ph_TP_af_cap")+"<a/>"));
			
			this.addButton(bsubmit);
	//		put_User_attention(wd,2,"f",msgs7.get("ph_TP_email"),msgs7.get("ph_TP_SMS"));
			
			
			Phones.addChangeHandler(new ChangeHandler(){
				@Override
				public void onChange(ChangeEvent event) {
					int idx=form.Phones.getSelectedIndex();
					if (idx>=0){form.CORTP.setText(getPhoneTPName(GSControl.myInfo.phones.get(idx).tp));
					}else{form.CORTP.setText("");}}
			});
			this.add(wd);
		};
		//============================================================================//
				
	};	
