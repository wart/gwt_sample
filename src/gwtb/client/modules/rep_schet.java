package gwtb.client.modules;

import gwtb.client.GSControl;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class rep_schet extends Window
{
	private static TreeMap<String,String> msgs21=null;
	static emailinf form=null;
	//=======================================================================	
	public static  void Run()
	{
		if (msgs21==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(21,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs21=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{Create();};
	}		
	//=======================================================================	
	public static void Create()
	{
		if (form==null)
		{
			form= new emailinf();
		};
		form.show();
	}	
	//============================================================================//
	public Button bsubmit;
	public rep_schet()
	{
		
		 //this.setWidth("700px");
		 this.setSize(470, 190);
		 this.setPlain(true);
		 this.setModal(GSControl.formsmodal);
		 this.setBlinkModal(true);
		 this.setHeading(msgs21.get("emailinf_title"));		
	     bsubmit= new Button(msgs21.get("emailinf_commit"));
	     bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>() {
	         @Override
	         public void componentSelected(ButtonEvent ce) {
	         }
	     });
	};
	//============================================================================//
}
