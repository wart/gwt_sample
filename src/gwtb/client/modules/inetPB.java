package gwtb.client.modules;

import gwtb.client.GSControl;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

public class inetPB  extends Window
{
	private static TreeMap<String,String> msgs10=null;
	static inetPB form=null;
	//=======================================================================	
	public static  void Run()
	{
		if (msgs10==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(10,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs10=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{Create();};
	}		
	//=======================================================================	
	public static void Create()
	{
		if (form==null)
		{
			form= new inetPB();
		};
	//	form.Errmsg.setText("");
		form.show();
	}	
	//============================================================================//
	ListBox days;
	TextBox summ;
	int r_summ;
	int r_days;
    public CheckBox aferta;
	public VerticalPanel wd;
	
	public inetPB()
	{
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading(msgs10.get("inet_PB_title"));
		this.setResizable(false);
	//	this.setWidth("600");
		
		days= new ListBox();
		days.addItem("1","1");
		days.addItem("2","2");
		days.addItem("3","3");
		days.addItem("4","4");
		days.addItem("5","5");			
		summ = new TextBox();
		
		

		
		Button bs = new Button(msgs10.get("inet_PB_submit"));
		bs.addSelectionListener( 
				 new SelectionListener<ButtonEvent>()
				 {   
				  	@Override  
					public void componentSelected(ButtonEvent ce)
					{
				  		if(form.aferta.getValue()!=true)
						{
				  			GSControl.ErrMsg(msgs10.get("inet_PB_ErrAferta"));
					    	return;
						};
						try
						{
							form.r_summ=Integer.parseInt(form.summ.getText());
							form.r_days=Integer.parseInt(form.days.getItemText(form.days.getSelectedIndex()));
						
						}catch (Exception ex) {
							GSControl.ErrMsg(msgs10.get("inet_PB_err1"));
					    	return;
					    };
						if (form.r_summ<10)
						{
							GSControl.ErrMsg(msgs10.get("inet_PB_err2"));
					    	return;
						};
						if (form.r_summ>1000)
						{
							GSControl.ErrMsg(msgs10.get("inet_PB_err2"));
					    	return;
						};				
						if (form.r_days<1)
						{
							GSControl.ErrMsg(msgs10.get("inet_PB_err2"));
					    	    return;
						};
					    
						GSControl.askdothis(new ClickHandler()  //yes
						  {
					          public void onClick(ClickEvent event)
					          {
					        	  GSControl.ShowProcessing();	
					        	  GSControl.GetGS().InetPB(form.r_summ,form.r_days,
									new AsyncCallback<String>()
									{
						        		public void onFailure(Throwable caught)
						        		{
						        			GSControl.HideProcessing();
						        			GSControl.JournalNeedReFresh=true;
						        			GSControl.ErrMsg(caught); 
						        			form.summ.setText("");
										}
										public void onSuccess(String res)
										{
											GSControl.HideProcessing();
						        			GSControl.JournalNeedReFresh=true;
											form.summ.setText("");						        			
						        			form.aferta.setValue(false);
						        			 GSControl.OkMsg( res);   
										};
									});
					          };
				          },null,null,null);   //msg
					}
				 });
				  	
		aferta  = new CheckBox();
		aferta.setBoxLabel(msgs10.get("inet_PB_af_ok"));
	
		
		wd= new VerticalPanel();
		
		
		
		wd.setHorizontalAlign(HorizontalAlignment.CENTER);
		wd.add(aferta);
		wd.add(new HTML("<a href=\""+msgs10.get("inet_PB_af_lnk")+"\" target=blank>"+msgs10.get("inet_PB_af_cap")+"<a/>"));
		wd.add(new HTML("<hr>"));
		wd.add(new Label(msgs10.get("inet_PB_txt1")));
		wd.add(new HTML("<hr>"));
		wd.add(new Label(msgs10.get("inet_PB_txt2")));
		wd.add(summ);
		wd.add(new Label(msgs10.get("inet_PB_txt3")));
		wd.add(days);
//		wd.add(new HTML("<hr>"));
		this.addButton(bs);
		
	//	put_User_attention(wd,7,"f",msgs10.get("inet_PB_email"),msgs10.get("inet_PB_SMS"));

		
	this.add(wd);
	
	}
}