package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.BillPeriod;
import gwtb.client.rpcdata.RPC_BillPeriod;

import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
public class corpreport extends Window {
	private static TreeMap<String, String> msgs24 = null;
	static corpreport form = null;
	// =======================================================================
	public static void Run() {
		if (msgs24 == null) {
			GSControl.ShowWait();
			GSControl.GetGS().getMsgList(24,
					new AsyncCallback<TreeMap<String, String>>() {
						public void onFailure(Throwable caught) {
							GSControl.HideWait();
							GSControl.ErrMsg(caught);
						}
						public void onSuccess(TreeMap<String, String> res) {
							msgs24 = res;
							GSControl.HideWait();
							Create();
						};
					});
		} else {
			Create();
		};
	}
	// =======================================================================
	public static void Create() {
		if (form == null) {
			form = new corpreport();
			
			if (GSControl.myInfo.billData==null)
			{
				GSControl.ShowWait();
				GSControl.GetGS().getBillPeriods(new AsyncCallback<Vector<RPC_BillPeriod>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(Vector<RPC_BillPeriod> res)
					{
					
						GSControl.myInfo.billData=BillPeriod.convert(res);
						GSControl.HideWait();
						form.showForm();
					};
				});
				return;
			};
		};
		form.showForm();
	}
	// ============================================================================//
	 public ListBox PeriodList;
	public CheckBox act1;
	public CheckBox act2;
	public CheckBox schet1;
	public CheckBox schet2;
	public corpreport()
	{
		this.setWidth(415);
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading(msgs24.get("corpreport_title")); 	
		this.setLayout(new FlowLayout()); 
		PeriodList= new ListBox();
		FormData formData = new FormData("-10"); 
		act1   = new CheckBox();act1  .setBoxLabel(msgs24.get("corpreport_act1"));
		act2   = new CheckBox();act2  .setBoxLabel(msgs24.get("corpreport_act2"));
		schet1 = new CheckBox();schet1.setBoxLabel(msgs24.get("corpreport_schet1"));
		schet2 = new CheckBox();schet2.setBoxLabel(msgs24.get("corpreport_schet2"));
		this.setLayout(new FlowLayout()); 
		FormLayout lay = new FormLayout();   
		lay.setLabelWidth(75);
	    FieldSet fs1 = new FieldSet();fs1.setHeading(msgs24.get("corpreport_type"));
	    fs1.setLayout(lay);
	    fs1.add(act1,  formData);
	    fs1.add(act2,  formData);
	    fs1.add(schet1,formData);
	    fs1.add(schet2,formData);
	    this.add(PeriodList);
	    this.add(fs1);
	    
	    
	    
		Button bsubmit = new Button(msgs24.get("corpreport_submit"));
		bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>()
			{
			
				@Override
				public void componentSelected(ButtonEvent ce)
				{
					
					int pi=form.PeriodList.getSelectedIndex();
					if(pi>-1)
					{
						BillPeriod bp=GSControl.myInfo.getBillPeriodByName(PeriodList.getItemText(pi));
						com.google.gwt.user.client.Window.open("reporter?repname=schet&bid="+bp.id,"","");
						
					}
					
								
					/*
					GSControl.askdothis(new ClickHandler() // yes
							{
								public void onClick(ClickEvent event) {
									GSControl.ShowProcessing();
									GSControl.GetGS().corpreport("",
											form.act1.getValue()?1:0,
											form.act2.getValue()?1:0,
											form.schet1.getValue()?1:0,
											form.schet2.getValue()?1:0,
											new AsyncCallback<Void>() {
												public void onFailure(
														Throwable caught) {
													GSControl.HideProcessing();
													GSControl.JournalNeedReFresh = true;
													GSControl.ErrMsg(caught);
												}
												public void onSuccess(Void res) {
													GSControl.HideProcessing();
													GSControl.JournalNeedReFresh = true;
													GSControl.OkMsg(msgs24.get("corpreport_submit_ok"));
												};
											});
								};
							}, null, null, null);
							/**/
				}
			}
	);
		this.addButton(bsubmit);
	}
	// ============================================================================//
	  public void showForm()
	  {
		    PeriodList=GSControl.myInfo.CreateBillPerListBox(PeriodList, false);
			show();
	  }	
	// ============================================================================//
}
