package gwtb.client.modules;

import gwtb.client.Desctop;
import gwtb.client.GSControl;
import gwtb.client.rpcdata.abon_profile;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;


public class AuthForm extends Window
{

	protected static AuthForm form=null;
	public static void Run()
	{
		Desctop.Run();
		form=new AuthForm();form.show();
	}
	
	private Button blogon;
	private TextBox flogin;
	private PasswordTextBox fpwd;
	private VerticalPanel dvp;
	public CheckBox ofert;
	

	@SuppressWarnings("deprecation")
	public AuthForm()
	{
	
		this.setWidth(500);
		this.setPlain(true);   
		this.setModal(false);   
		this.setBlinkModal(true);   
		this.setHeading(GSControl.msgs1.get("auth_title")); 			
		

		blogon = new Button(GSControl.msgs1.get("auth_enter"));
		flogin = new TextBox(); 
		fpwd = new PasswordTextBox();
		
		ofert=new CheckBox();ofert.setBoxLabel(GSControl.msgs1.get("auth_ofert"));
	

		blogon.addSelectionListener(
		new SelectionListener<ButtonEvent>()
		{   
			@Override  
		    public void componentSelected(ButtonEvent ce)
			{
		    	  if(ofert.getValue()==true)
		    	  {
		    		  GSControl.ShowWait();
		    		  GSControl.GetGS().auth(flogin.getText(),fpwd.getText(), 
		    		  new AsyncCallback<abon_profile>()
		    		  {
		    			  public void onFailure(Throwable caught)
		    			  {
								GSControl.HideWait();
								GSControl.ErrMsg(caught.getMessage());
		    			  }
		    			  public void onSuccess(abon_profile result)
		    			  {
		    				  
		    				  GSControl.HideWait();		    				  
		    				  GSControl.SetAbon(result);
		    				  form.hide();
		    			  }
		    		  });
		    	  }else
		    	  {
		    		  GSControl.ErrMsg(GSControl.msgs1.get("auth_needchk"));
		    	  }
			}
		});
		
		
		dvp = new VerticalPanel();
		HTML hh=new HTML(GSControl.msgs1.get("auth_text"));hh.setStyleName("mytxtJ");
		
		dvp.add(hh);			
		dvp.setWidth("100%");
		dvp.addStyleName("dialogVPanel");
		dvp.add(new HTML("<b>"+GSControl.msgs1.get("auth_login")+"</b>"));
		dvp.add(flogin);
		dvp.add(new HTML("<br><b>"+GSControl.msgs1.get("auth_pwd")+"</b>"));
		dvp.add(fpwd);
		dvp.add(new HTML("<a href=\""+GSControl.msgs1.get("auth_sogl_lnk")+"\" target=blank>"+GSControl.msgs1.get("auth_sogl_cap")+"<a/>"));
		Hyperlink hl= new Hyperlink(GSControl.msgs1.get("auth_recovery_pwd"),"qq");
//		Label l=new Label(GSControl.msgs1.get("auth_recovery_pwd"));
		
		dvp.add(hl);

		hl.addClickListener(new ClickListener()
		{
			@Override
			public void onClick(Widget sender) {
			LKpwdrecover.Run();	
				
			}});
		
		dvp.setHorizontalAlign(HorizontalAlignment.CENTER);
		dvp.add(ofert);
//		dvp.add(blogon);
		this.add(dvp);	
		this.addButton(blogon);
		//		loginField.setFocus(true);
//		loginField.selectAll();
	}
};