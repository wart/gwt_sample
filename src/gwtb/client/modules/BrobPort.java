package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.BronSetUnSetResult;
import gwtb.client.rpcdata.BronState;
import gwtb.client.rpcdata.wrkitem;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;


public class BrobPort extends Window{
	private static TreeMap<Integer,wrkitem> wrklst3=null;
	private static TreeMap<String,String> msgs4=null;
	
	public static void Run()
	{
		if (wrklst3==null)
		{
			GSControl.ShowWait();
			GSControl.GetGS().getWRKList(3,
			new AsyncCallback<TreeMap<Integer,wrkitem>>()
			{
				public void onFailure(Throwable caught)
				{
					GSControl.HideWait();
					GSControl.ErrMsg(caught); 
				}
				public void onSuccess(TreeMap<Integer,wrkitem> res)
				{
					wrklst3=res;
					GSControl.HideWait();
					LoadMsgs();
				};
			});
		}
		else
		{
			LoadMsgs();
		}
	}			
	//============================================================================//	
	private static void LoadMsgs()
	{
		if (msgs4==null)
		{	
		    GSControl.ShowWait();
	        GSControl.GetGS().getMsgList(4,
			new AsyncCallback<TreeMap<String,String>>()
			{
				public void onFailure(Throwable caught)
				{
					GSControl.HideWait();
					GSControl.ErrMsg(caught); 
				}
				public void onSuccess(TreeMap<String,String> res)
				{
					msgs4=res;
					GSControl.HideWait();
					Create();
				};
			});
	}else
	{Create();};
}			
//=======================================================================	
	private static BrobPort singBronPort_Panel=null;
	private static void Create()
	{
		//Window.alert("create_bron_form()");
		if (singBronPort_Panel==null)
		{
			singBronPort_Panel= new BrobPort();
		};
		singBronPort_Panel.Refresh_State();
	}
//=======================================================================	

//	DialogBox win;
	VerticalPanel WDsetbron;
	VerticalPanel WDunsetbron;
	Button bsubmit;
	ListBox month;
	public BrobPort()
	{
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading(msgs4.get("bron_title"));
		this.setWidth(356);
		bsubmit = new Button(msgs4.get("bron_submit"));
		this.addButton(bsubmit);
	};
	//=======================================================================		
	public void setFormState(BronState res)
	{
		if (res.inbron != 1) {//--- set bron widget
//		if (res.inbron == 1) {//--- set bron widget				
			
			WDsetbron= new VerticalPanel();
			WDsetbron.addStyleName("dialogVPanel");
			WDsetbron.setWidth("100%");
			WDsetbron.setHorizontalAlign(HorizontalAlignment.CENTER);
			HTML txt =  new HTML(msgs4.get("bron_zayavk_on1"));
			
			HTML txt11 =  new HTML(msgs4.get("bron_text"));txt11.setStyleName("mytxt3");
			WDsetbron.add(txt11);
			WDsetbron.add(txt);
			
		    month = GSControl.buildWRKListBox(wrklst3);
		    WDsetbron.add(month);
			//WDsetbron.add(bsubmit);
		    
				
			
			this.setHeading(msgs4.get("bron_title")+" "+msgs4.get("bron_title_on"));
		
			bsubmit.addSelectionListener( 
					 new SelectionListener<ButtonEvent>()
					 {   
					  	@Override  
						public void componentSelected(ButtonEvent ce)
						{
					  		GSControl.askdothis(new ClickHandler()  //yes
							  {
						          public void onClick(ClickEvent event)
						          {
						        	if (singBronPort_Panel.month.getSelectedIndex()>=0){
						        		  Integer vl=wrklst3.get(singBronPort_Panel.month.getSelectedIndex()).id;
						        		  
						        		  
						        	GSControl.ShowProcessing();
						        	GSControl.GetGS().BronSet(vl,new AsyncCallback<BronSetUnSetResult>()
										{
							        		public void onFailure(Throwable caught)
							        		{
							        			GSControl.HideProcessing();
							        			GSControl.JournalNeedReFresh=true;
							        			GSControl.ErrMsg(caught); 
											}
											public void onSuccess(BronSetUnSetResult res)
											{
												GSControl.HideProcessing();
							        			GSControl.JournalNeedReFresh=true;
												String txt="";
												if((res.r1<0)||(res.r2<0))
												{
													txt=msgs4.get("bron_fail")+res.retdesc;
												}else
												{
													txt=msgs4.get("bron_succ_set")+res.r1+","+res.r2;
												}
												//singBronPort_Panel.hide();
												//singBronPort_Panel.set_Error(txt);
							        			//BRONSW(WDsetbron);
												GSControl.OkMsg(txt);   
											};
										});}else{
											GSControl.ErrMsg(msgs4.get("bron_reqerr1")); 
										}
						          };
					          },null,msgs4.get("bron_run")+" "+msgs4.get("bron_title_on")+" "+msgs4.get("bron_zayavk_on1")+" "+
					          					singBronPort_Panel.month.getItemText(singBronPort_Panel.month.getSelectedIndex())+"?",null);
					
						}
					});
			this.add(WDsetbron);
		}
		else//--- Unset bron widget
		{
			this.setHeading(msgs4.get("bron_title")+" "+ msgs4.get("bron_title_off"));
			
			WDunsetbron= new VerticalPanel();
			WDunsetbron.addStyleName("dialogVPanel");
			WDunsetbron.setWidth("100%");
			WDunsetbron.setHorizontalAlign(HorizontalAlignment.CENTER);
				
			HTML txt11 =new HTML(msgs4.get("bron_text"));
			txt11.setStyleName("mytxt3");
			WDunsetbron.add(txt11);
				
			//WDunsetbron.add(bsubmit);
			bsubmit.addSelectionListener( 
						 new SelectionListener<ButtonEvent>()
						 {   
						  	@Override  
							public void componentSelected(ButtonEvent ce)
							{
						  		GSControl.askdothis(new ClickHandler()  //yes
								  {
							          public void onClick(ClickEvent event)
							          {
							        	  GSControl.ShowProcessing();
							        	  
							        	  GSControl.GetGS().BronUnSet(
											new AsyncCallback<BronSetUnSetResult>()
											{
								        		public void onFailure(Throwable caught)
								        		{
								        			GSControl.HideProcessing();
								        			GSControl.JournalNeedReFresh=true;
								        			GSControl.ErrMsg(caught); 
//								        			BRONSW(WDunsetbron);

												}
												public void onSuccess(BronSetUnSetResult res)
												{
								        			GSControl.HideProcessing();
								        			GSControl.JournalNeedReFresh=true;													
													String txt="";
													if((res.r1<0)||(res.r2<0))
													{
														txt=msgs4.get("bron_fail")+res.retdesc;
													}else
													{
														txt=msgs4.get("bron_succ_unset")+res.r1+","+res.r2;
													}
													GSControl.OkMsg( txt);  
//													BRONSW(WDunsetbron);
													
												};
											});
							          };
						          },null,msgs4.get("bron_run")+" "+msgs4.get("bron_title_off")+"?",null);  
							}
						 });
			
			this.add(WDunsetbron);
		};
		this.show();
	}
	//=======================================================================		
	public void Refresh_State()
	{
		GSControl.ShowProcessing();
	
        GSControl.GetGS().getBronState(
			new AsyncCallback<BronState>()
			{
        		public void onFailure(Throwable caught)
        		{
        			GSControl.HideProcessing();
        			GSControl.ErrMsg(caught);
				}
				public void onSuccess(BronState res)
				{
					GSControl.HideProcessing();
					setFormState(res);
				};
			});
	};
}