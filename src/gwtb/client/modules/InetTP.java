package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.user_connection;
import gwtb.client.rpcdata.wrkitem;

import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;


public class InetTP extends Window{
	private static  TreeMap<String,String> msgs8=null;
	private static InetTP form=null;
	private TreeMap<String,Vector<wrkitem>> plans=null;
	//============================================================================//	
	public static void Run()
	{
		if (msgs8==null)
		{
			GSControl.ShowWait();
	        GSControl.GetGS().getMsgList(8,
	        		new AsyncCallback<TreeMap<String,String>>()
	        		{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs8=res;
						GSControl.HideWait();
						Create();
					};
	        		});
		}
		else
		{
			Create();
		}
	}		
//============================================================================//
	public static void Create()
	{
		if (form==null)
		{
			form= new InetTP();
		};
		form.conns=GSControl.buildConnListBox(form.conns);
		form.changeConn();
		form.show();
	}	
//============================================================================//
	ListBox TP;
	ListBox conns;
	Label CORTP;
	public String msggg;
	public CheckBox aferta;
	Button bsubmit;
//	public VerticalPanel wd;
	//============================================================================//
/*    public String getInetTPName(int id)
    {
    	int sz=wrklst1.size();
    	for (int j=0;j<sz;++j)
    	  if (wrklst1.get(j).id==id)
    		  return wrklst1.get(j).cap;
    	return "";
    };
    /**/		
  //============================================================================//	
	public InetTP()
	{
		this.plans=new TreeMap<String,Vector<wrkitem>>();
		
		this.setSize(284, 340);   
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading(msgs8.get("inet_TP_title")); 
		this.setResizable(false);
		TP= new ListBox(false);
		
		conns   =  new ListBox();conns.setWidth("100%");
		CORTP   = new Label();
	    
	    CORTP.setStyleName("mytxtR");
		aferta  = new CheckBox();
		aferta.setBoxLabel(msgs8.get("inet_TP_yes"));

		
		bsubmit = new Button(msgs8.get("inet_TP_submit"));
		
	
		bsubmit.addSelectionListener( 
				 new SelectionListener<ButtonEvent>() {   
					      @Override  
						    public void componentSelected(ButtonEvent ce)
							{
					    	//  MessageBox.alert(GSControl.msgs1.get("message_title"), form.getWidth()+" "+form.getHeight(), null); 

					    	  if (form.aferta.getValue()==false)
					    	  {
					    		  GSControl.ErrMsg(msgs8.get("inet_TP_ErrAferta"));
					    		  return;
					    	  };
								form.msggg=msgs8.get("inet_TP_ask")+" \""+form.CORTP.getText()+"\" "+
								msgs8.get("inet_TP_ask1")+" \""+form.TP.getItemText(form.TP.getSelectedIndex())+"\" "+
								msgs8.get("inet_TP_ask2")+" "+form.conns.getItemText(form.conns.getSelectedIndex())+ "?";
								GSControl.askdothis(new ClickHandler()  //yes
								  {
							          public void onClick(ClickEvent event)
							          {
							        	  GSControl.ShowProcessing();		
							        	  GSControl.GetGS().ChangeInetTP(
							        			  getConnName(),
							        			  getSetTpId(), new AsyncCallback<String>(){
							        		  public void onFailure(Throwable caught)
								        		{
							        			   
								        			GSControl.HideProcessing();
								        			GSControl.JournalNeedReFresh=true;
								        			GSControl.ErrMsg(caught);
												}
							        			public void onSuccess(String res)
												{
							        				GSControl.HideProcessing();
								        			GSControl.JournalNeedReFresh=true;							        				
							        				String [] ss=res.split(":",2);
							        				int i=0;
							        				try
							        				{
							        					i=Integer.parseInt(ss[0]);
							        				}catch(Exception e)
							        				{
							        					
							        				};
							        				GSControl.OkMsg(ss[1]); 
							        				if (i>0)
							        				{
							        					form.hide();
							        				GSControl.JournalNeedReFresh=true;
							        				GSControl.OkMsg( 
							        						msgs8.get("inet_TP_ret")+ss[0]+
							        						", "+ ss[1]+msgs8.get("inet_TP_ret2")+
							        						form.TP.getItemText(form.TP.getSelectedIndex())+" "+
							        						msgs8.get("inet_TP_ret3")+GSControl.myInfo.date_tomorrow			        						
							        						);  
							        		
							        				}
												};
							        	  });
							        
							        	  };},null,msggg
							        	  ,null);							
							}
				 });
		

		
	    setLayout(new FitLayout());   

	    VerticalPanel panel= new VerticalPanel();
	    panel.setLayout(new FitLayout());	    
	    panel.setWidth("100%");
	    panel.setHorizontalAlign(HorizontalAlignment.CENTER);

	    panel.add(new Label(msgs8.get("inet_TP_txt2")));
	    panel.add(conns);
	    panel.add(new HTML("<br>"));
	    panel.add(new Label(msgs8.get("inet_TP_cortp")));
	    panel.add(CORTP);
	    panel.add(new HTML("<hr>"));
	    panel.add(new HTML("<a href=\""+msgs8.get("inet_TP_lnk_lnk")+"\" target=blank>"+msgs8.get("inet_TP_lnk_cap")+"<a/>"));
	    panel.add(new Label(msgs8.get("inet_TP_txt1")));
	    panel.add(TP);
	    panel.add(new HTML("<hr>"));
	    panel.add(aferta);
	    panel.add(new HTML("<a href=\""+msgs8.get("inet_TP_af_lnk")+"\" target=blank>"+msgs8.get("inet_TP_af_cap")+"<a/>"));
	    panel.add(new HTML("<br>"));
	    add(panel);
	    this.addButton(bsubmit);
	    
	  
		conns.addChangeHandler(new ChangeHandler(){
			@Override
			public void onChange(ChangeEvent event) {form.changeConn();};} );
		
	}; 
	//============================================================================//
	public user_connection getConn()
	{
		int idx=this.conns.getSelectedIndex();
		if (idx>=0)
			return GSControl.myInfo.connections.get(idx);
		return null;
	};
	//============================================================================//
	public String getConnName()
	{
		user_connection u =getConn();
		if (u!=null) return u.connection;
		return "";
	}
	//============================================================================//
	public int getSetTpId()
	{
	  String tpn=this.TP.getItemText(this.TP.getSelectedIndex());
		user_connection uc=getConn();
		if(uc!=null)
		{
			Vector<wrkitem> md=plans.get(uc.model);	
			if (md!=null)
			{
				int sz=md.size();
				int j=0;
				while (j<sz)
				{
					wrkitem wi=md.get(j);
					if (wi!=null)
					{
						if(wi.cap==tpn) return wi.id;
						j++;
					};
				};
			};
		};
		return -1;
	}
	//============================================================================//
	public String TPName()
	{
		user_connection uc=getConn();
		if(uc!=null)
		{
			Vector<wrkitem> md=plans.get(uc.model);	
			if (md!=null)
			{
				int sz=md.size();
				int j=0;
				while (j<sz)
				{
					wrkitem wi=md.get(j);
					if (wi!=null)
					{
						if(wi.id==uc.tp) return wi.cap;
						j++;
					};
				};
			};
		};
		return "";
	};
	//============================================================================//
	public void changeConn()
	{
		user_connection uc=getConn();
		if(uc!=null)
		{
			Vector<wrkitem> md=plans.get(uc.model);
			if (md==null)
			{
				GSControl.ShowWait();
		        GSControl.GetGS().getModelPlans(uc.model,
		        		new AsyncCallback<Vector<wrkitem>>()
		        		{
							public void onFailure(Throwable caught)
							{
								GSControl.HideWait();
								GSControl.ErrMsg(caught);  
							}
							public void onSuccess(Vector<wrkitem> res)
							{
								form.plans.put(getConn().model, res);
								GSControl.HideWait();
								changeDo();
							};
		        		});
			}else
			{
				changeDo();
			};
		}else
		{
			GSControl.ErrMsg("111");
		};
	}
	//============================================================================//
	private void changeDo()
	{
		CORTP.setText(TPName());
		this.TP.clear();
		user_connection uc=getConn();
		if(uc!=null)
		{
			Vector<wrkitem> wl=plans.get(uc.model);
			int sz = wl.size();
			if (sz>0)
		    {
				for (int j = 0; j < sz; ++j)
				{
					TP.addItem(wl.get(j).cap);
				};
				TP.setVisible(true);
				bsubmit.setVisible(true);
		    }else
		    {
		    	TP.setVisible(false);
		    	bsubmit.setVisible(false);
		    };
		};
	};
	//============================================================================//
}