package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.BillPeriod;
import gwtb.client.rpcdata.MDModelData;
import gwtb.client.rpcdata.RPC_BillPeriod;
import gwtb.client.rpcdata.period_calls_lg_data;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;

public class call_lg  extends Window
{
	private static TreeMap<String, String> msgs30 = null;
	private static call_lg form = null;

	public static void Run()
	{
		if (msgs30 == null)
		{
			GSControl.ShowWait();
			GSControl.GetGS().getMsgList(30, new AsyncCallback<TreeMap<String, String>>()
			{
				public void onFailure(Throwable caught)
				{
					GSControl.HideWait();
					GSControl.ErrMsg(caught);
				}
				public void onSuccess(TreeMap<String, String> res)
				{
					msgs30 = res;
					GSControl.HideWait();
					Create();
				}
			});
		}
	    else
	    {
	    	Create();
	    }
	}
	// =======================================================================
	private static void Create()
	{
		if (form == null)
	    {
			form = new call_lg();
			if (GSControl.myInfo.billData==null)
			{
				GSControl.ShowWait();
				GSControl.GetGS().getBillPeriods(new AsyncCallback<Vector<RPC_BillPeriod>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(Vector<RPC_BillPeriod> res)
					{
						
						GSControl.myInfo.billData=BillPeriod.convert(res);
						GSControl.HideWait();
						form.showForm();
					};
				});
			}else{
				form.showForm();
			};
			return;	      
	    }
	    form.showForm();
	  }
	  // =======================================================================
	  private ColumnModel loc_cm;
	  private ListStore<MDModelData> loc;
	  private List<ColumnConfig> loc_configs;
	  private Grid<MDModelData> loc_grid;
	  
	  private ColumnModel glob_cm;
	  private ListStore<MDModelData> glob;
	  private List<ColumnConfig> glob_configs;
	  private Grid<MDModelData> glob_grid;

	  public ListBox PeriodList;
	  public ChangeHandler ch;
	  
	  public call_lg()
	  {
		  this.setSize(725, 490);   
			this.setPlain(true);   
			this.setModal(GSControl.formsmodal);   
			this.setBlinkModal(true);   
			this.setHeading(msgs30.get("dtl_mgn_title"));
			this.setMaximizable(true);
			
			
			
			//call_l_heads=(String[])MDModelData.AR("dev","tp","cnt","len","summ");
			//call_g_heads=(String[])MDModelData.AR("dev","cod","mgmn","dest","date","len","summ");    
			
			loc=new ListStore<MDModelData>();
			loc_configs = new ArrayList<ColumnConfig>();   
		    ColumnConfig 
		    column = new ColumnConfig(BillPeriod.call_l_heads()[0]  ,msgs30.get("dtl_mgn_phone"	), 100);loc_configs.add(column);
		    column = new ColumnConfig(BillPeriod.call_l_heads()[1]  ,msgs30.get("dtl_mgn_tp"  	), 100);loc_configs.add(column);
		    column = new ColumnConfig(BillPeriod.call_l_heads()[2]  ,msgs30.get("dtl_mgn_cnt"  	), 100);loc_configs.add(column);
		    column = new ColumnConfig(BillPeriod.call_l_heads()[3]  ,msgs30.get("dtl_mgn_len"  	), 100);loc_configs.add(column);
		    column = new ColumnConfig(BillPeriod.call_l_heads()[4]  ,msgs30.get("dtl_mgn_summ"  	), 100);loc_configs.add(column);
		    loc_cm = new ColumnModel (loc_configs);
		    loc_grid = new Grid<MDModelData>(loc, loc_cm);
		    loc_grid.setStyleAttribute("borderTop", "none");   
		    loc_grid.setAutoExpandColumn(BillPeriod.call_l_heads()[4]);   
		    loc_grid.setBorders(true);   
		    loc_grid.setStripeRows(true);	
		    loc_grid.setHeight(160);
		    loc_grid.setWidth(700);
		  
		  
		    glob=new ListStore<MDModelData>();
		    glob_configs = new ArrayList<ColumnConfig>();   
		    column = new ColumnConfig(BillPeriod.call_g_heads()[0]  ,msgs30.get("dtl_mgn_phone"), 100);glob_configs.add(column);
		    column = new ColumnConfig(BillPeriod.call_g_heads()[1]  ,msgs30.get("dtl_mgn_cod"  ), 100);glob_configs.add(column);
		    column = new ColumnConfig(BillPeriod.call_g_heads()[2]  ,msgs30.get("dtl_mgn_mgmn" ), 100);glob_configs.add(column);
		    column = new ColumnConfig(BillPeriod.call_g_heads()[3]  ,msgs30.get("dtl_mgn_dest" ), 100);glob_configs.add(column);
		    column = new ColumnConfig(BillPeriod.call_g_heads()[4]  ,msgs30.get("dtl_mgn_date" ), 100);glob_configs.add(column);
		    column = new ColumnConfig(BillPeriod.call_g_heads()[5]  ,msgs30.get("dtl_mgn_len"  ), 100);glob_configs.add(column);
		    column = new ColumnConfig(BillPeriod.call_g_heads()[6]  ,msgs30.get("dtl_mgn_summ" ), 100);glob_configs.add(column);
		    glob_cm = new ColumnModel(glob_configs);
		    glob_grid = new Grid<MDModelData>(glob, glob_cm);
		    glob_grid.setStyleAttribute("borderTop", "none");   
		    glob_grid.setAutoExpandColumn(BillPeriod.call_g_heads()[6]);   
		    glob_grid.setBorders(true);   
		    glob_grid.setStripeRows(true);		
		    glob_grid.setHeight(200);
		    PeriodList= new ListBox();
			ch=new ChangeHandler()
			{
				@Override
				public void onChange(ChangeEvent event)
				{
					if (PeriodList.getSelectedIndex()>=0)
					{
						BillPeriod bp=GSControl.myInfo.getBillPeriodByName(PeriodList.getItemText(PeriodList.getSelectedIndex())); 
						if (bp!=null)
						{
							if(bp.call_g!=null)
							{
								form.visualize(bp);
							}else
							{
								GSControl.ShowWait();
								GSControl.GetGS().get_period_calls_lg_data(bp.id,new AsyncCallback<period_calls_lg_data>()
								{
									public void onFailure(Throwable caught)
									{
										GSControl.HideWait();
										GSControl.ErrMsg(caught); 
									}
									public void onSuccess(period_calls_lg_data res)
									{
										BillPeriod bp=GSControl.myInfo.getBillPeriodByName(form.PeriodList.getItemText(PeriodList.getSelectedIndex()));
										if(bp!=null)
										{
											bp.fill_calls(res);
											form.visualize(bp);
										}
										GSControl.HideWait();									
									};
								});
							};
						}
					}
				}
			};
			PeriodList.addChangeHandler(ch);
			Label l1= new Label(msgs30.get("dtl_mgn_bilper"));
			Label l2= new Label(msgs30.get("dtl_mgn_local" ));
			Label l3= new Label(msgs30.get("dtl_mgn_glob"  ));
			this.add(new Html("<br>"));
			this.add(l1);
			this.add(PeriodList);
			this.add(new Html("<br>"));
			this.add(l2);
			this.add(loc_grid);
			this.add(new Html("<br>"));
			this.add(l3);
			this.add(glob_grid);
	
	  }
	// =======================================================================
	  protected void visualize(BillPeriod bp)
	  {
		  loc_grid.reconfigure(bp.call_l, loc_cm);
		  glob_grid.reconfigure(bp.call_g,glob_cm);
	  }
	// =======================================================================
	  public void showForm()
	  {
		    PeriodList=GSControl.myInfo.CreateBillPerListBox(PeriodList, false);
			ch.onChange(null);
			show();
	  }
	  // =======================================================================
}
