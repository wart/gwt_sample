package gwtb.client.modules;

import gwtb.client.GSControl;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class lkpwd extends Window {
	  private static TreeMap<String, String> msgs27 = null;
	  private static lkpwd form = null;

	  public static void Run() {
	    if (msgs27 == null) {
	      GSControl.ShowWait();
	      GSControl.GetGS().getMsgList(27, new AsyncCallback<TreeMap<String, String>>() {

	        public void onFailure(Throwable caught) {
	          GSControl.HideWait();
	          GSControl.ErrMsg(caught);
	        }

	        public void onSuccess(TreeMap<String, String> res) {
	        	msgs27 = res;
	          GSControl.HideWait();
	          Create();
	        }
	      });
	    }
	    else {
	      Create();
	    }
	  }

	  // =======================================================================
	  private static void Create() {
	    if (form == null) {
	      form = new lkpwd();
	    }
	    form.login.setValue("");
	    form.opwd.setValue("");
	    form.npwd.setValue("");
	    form.npwd2.setValue("");
	    form.show();
	  }
	  // =======================================================================
	   public TextField<String> login;
	   public TextField<String> opwd;
	   public TextField<String> npwd;
	   public TextField<String> npwd2;
	   public lkpwd()
	   {
		   this.setWidth("460px");
		   this.setPlain(true);
		   this.setModal(GSControl.formsmodal);
		   this.setBlinkModal(true);
		   this.setHeading(msgs27.get("lkpwd_title"));
		   this.setResizable(false);
		   FormLayout lay = new FormLayout();
		   lay.setLabelWidth(200);
		   
		   this.setLayout(lay);
		   
		   
		   FormData formData = new FormData("-24");
		   login= new TextField<String>();login.setFieldLabel(msgs27.get("lkpwd_login"));  
		   opwd = new TextField<String>();opwd .setFieldLabel(msgs27.get("lkpwd_opwd" )); opwd .setPassword(true);
		   npwd = new TextField<String>();npwd .setFieldLabel(msgs27.get("lkpwd_npwd" )); npwd .setPassword(true);
		   npwd2= new TextField<String>();npwd2.setFieldLabel(msgs27.get("lkpwd_npwd2")); npwd2.setPassword(true);
		   
		   login.setAutoValidate(true);login.setAllowBlank(false);
		   opwd.setAutoValidate(true);opwd.setAllowBlank(false);
		   npwd.setAutoValidate(true);npwd.setAllowBlank(false);
		   npwd2.setAutoValidate(true);npwd2.setAllowBlank(false);
		   
		   npwd2.setValidator(new Validator()
		   {

			@Override
			public String validate(Field<?> arg0, String arg1)
			{
				if(!(form.npwd.getValue().equals(form.npwd2.getValue()))) return msgs27.get("lkpwd_retype_err");
				return null;
			}
			   
		   });
		   
		   this.add(new Html("<br>"));
		   this.add(new Label(msgs27.get("lkpwd_txt")));
		   this.add(new Html("<br>"));
		   this.add(login,formData);
		   this.add(opwd,formData);
		   this.add(npwd,formData);
		   this.add(npwd2,formData);
		    
		   Button bsubmit = new Button(msgs27.get("lkpwd_submit"));
		    this.addButton(bsubmit);
		    bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>() {
		      @Override
		      public void componentSelected(ButtonEvent ce) {
		    	  
		    	if(form.login.isValid() & form.opwd.isValid() & (form.npwd.getValue().length()>0) &(form.npwd2.getValue().length()>0))
		    	{
		    	  
		        if (form.npwd.getValue().equals(form.npwd2.getValue()))
		        {
		          GSControl.askdothis(new ClickHandler() // yes
		              {
		                public void onClick(ClickEvent event) {
		                  GSControl.ShowProcessing();
		                  GSControl.GetGS().LK_PWD_CHANGE(form.login.getValue(), form.opwd.getValue(), 
		                		  						  form.npwd.getValue() , form.npwd2.getValue(), 
	               		  new AsyncCallback<String>() {
		                        public void onFailure(Throwable caught) {
		                          GSControl.HideProcessing();
		                          GSControl.JournalNeedReFresh = true;
		                          GSControl.ErrMsg(caught);
		                        }
		                        public void onSuccess(String res) {
		                          GSControl.HideProcessing();
		                          GSControl.JournalNeedReFresh = true;
		                          GSControl.OkMsg(res);
		                        };
		                      });
		                }
		              }, null, null, null); // msg
		        }else
		        {
		        	GSControl.ErrMsg(msgs27.get("lkpwd_retype_err"));
		        };
		    	}else
		        {
		        	GSControl.ErrMsg(msgs27.get("lkpwd_form_err"));
		        };
		      }
		    });	    
	   }
	  // =======================================================================
}
