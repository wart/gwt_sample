package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.MDModelData;
import gwtb.client.rpcdata.rpc_tp_calc;
import gwtb.client.rpcdata.rpc_tp_calc_inet;
import gwtb.client.rpcdata.rpc_tp_calc_iptv;
import gwtb.client.rpcdata.rpc_tp_calc_iptv_pack;
import gwtb.client.rpcdata.rpc_tp_calc_phone;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.GroupingStore;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Info;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.CheckBoxGroup;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.Radio;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupingView;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.user.client.rpc.AsyncCallback;


public class tp_calc extends Window
{
	  public static TreeMap<String, String> msgs34 = null; // calc form msgs
	  		 static tp_calc singtp_calc_form = null;
	  		  static GroupingStore<MDModelData> gs_phone = null;
	  		  static GroupingStore<MDModelData> gs_inet = null;
	  		  static GroupingStore<MDModelData> gs_iptv = null;
	  		  static GroupingStore<MDModelData> gs_iptv_pack = null;
	  		  static GroupingStore<MDModelData> gs_total = null;
	  		  static rpc_tp_calc data=null;
	  // =======================================================================
	  public static void Run()
	  {
		  if (msgs34 == null)
		  {
			  GSControl.ShowWait();
			  GSControl.GetGS().getMsgList(34, new AsyncCallback<TreeMap<String, String>>() {
				  public void onFailure(Throwable caught) {
					  GSControl.HideWait();
					  GSControl.ErrMsg(caught);
				  }
				  public void onSuccess(TreeMap<String, String> res)
				  {
					  msgs34 = res;
					  GSControl.HideWait();
					  Load();
				  };
		      	});
		  }
		  else
		  {
			  Load();
		  }
	  }
	  // =======================================================================
		final public static String[] heads_phone    (){final String[] rt=MDModelData.AH("categor","name","cost","un_cost","summm","typ"); return rt;};
		final public static String[] heads_inet     (){final String[] rt=MDModelData.AH("tech","name","cost","speed","un_cost"); return rt;};
		final public static String[] heads_iptv     (){final String[] rt=MDModelData.AH("tech","name","cost","descr"); return rt;};
		final public static String[] heads_iptv_pack(){final String[] rt=MDModelData.AH("id","name","cost","descr"); return rt;};
		final public static String[] heads_total    (){final String[] rt=MDModelData.AH("usl","tar","summm"); return rt;};
	  // =======================================================================
	  public static void Load()
	  {
		  if(data==null)
		  {
			  GSControl.ShowWait();
			  GSControl.GetGS().get_tp_calc(new AsyncCallback<rpc_tp_calc>() {
				  public void onFailure(Throwable caught) {
					  GSControl.HideWait();
					  GSControl.ErrMsg(caught);
				  }
				  public void onSuccess(rpc_tp_calc res)
				  {
					  data = res;
					  
					  int sz;

					  gs_phone = new GroupingStore<MDModelData>();
					  sz=res.fphone.size();
					  for (int i=0;i<sz;++i)
					  {
						  rpc_tp_calc_phone r= res.fphone.get(i);
						  if (r!=null)  gs_phone.add(new MDModelData(heads_phone(),r.categor,r.name,r.cost,r.un_cost,r.cost,r.typ));
					  };					  
					  
					  gs_inet = new GroupingStore<MDModelData>();
					  sz=res.finet.size();
					  for (int i=0;i<sz;++i)
					  {
						  rpc_tp_calc_inet r= res.finet.get(i);
						  if (r!=null)  gs_inet.add(new MDModelData(heads_inet(),r.tech,r.name,r.cost,r.speed,r.un_cost));
					  };					  
					  
					  gs_iptv = new GroupingStore<MDModelData>();
					  sz=res.fiptv.size();
					  for (int i=0;i<sz;++i)
					  {
						  rpc_tp_calc_iptv r= res.fiptv.get(i);
						  if (r!=null)  gs_iptv.add(new MDModelData(heads_iptv(),r.tech,r.name,r.cost,r.descr));
					  };					  

					  gs_iptv_pack = new GroupingStore<MDModelData>();
					  sz=res.fiptv_pack.size();
					  for (int i=0;i<sz;++i)
					  {
						  rpc_tp_calc_iptv_pack r= res.fiptv_pack.get(i);
						  if (r!=null)  gs_iptv_pack.add(new MDModelData(heads_iptv_pack(),r.id,r.name,r.cost,r.descr));
					  };					  
					  gs_total = new GroupingStore<MDModelData>();
					  
					  GSControl.HideWait();
					  Create();
				  };
		      	});
		  }else
		  {
			  Create();
		  }
	  }
	  // =======================================================================
	  private static void Create() {
	    // Window.alert("create_bron_form()");
	    if (singtp_calc_form == null) {
	    	singtp_calc_form = new tp_calc();
	    }
	    singtp_calc_form.show();
	  }
	  // =======================================================================

	  FieldSet nfs(String cap)
	  {
		  FieldSet f= new FieldSet();
		  f.setHeading(msgs34.get(cap));
		  f.setLayout(new FlowLayout());
		  f.setCollapsible(true);  
		  return f;
	  }
	  
	  
	  Grid<MDModelData> igr(ListStore<MDModelData> gs,ColumnModel cm,String aec)
	  {
		  Grid<MDModelData> g= new Grid<MDModelData>(gs,cm);
		  GroupingView view = new GroupingView(); 
		    view.setShowGroupedColumn(false); 
		    view.setForceFit(true); 
		    g.setView(view); 
		    g.setBorders(true); 
		    g.setStyleAttribute("borderTop", "none");   
		    if(aec!=null)
		    	g.setAutoExpandColumn(aec);   
		    g.setBorders(true);   
		    g.setStripeRows(true);
		    g.setAutoHeight(true);
//		    g.setHeight(200);		    
		    return g;
	  }
	  /**/

	  public void update_phone(int  K)
	  {
			Info.display("sss"," phone");
			int sz=gs_phone.getCount();
			MDModelData md;
			for(int i=0;i<sz;++i)
				if((md=gs_phone.getAt(i))!=null)
				{
					float Z=0;
					int typ=md.get("typ");
					float dl=0;
					float M=(Float)md.get("un_cost");
					float A=(Float)md.get("cost");
					switch(typ)
					{
						case 0:Z=A+M*K;
							break;
						case 1:
							Z=A+((K>340)?(K-340)*M:0);
							break;
						case 2:
							Z=A;
							break;
						default:
					};
					md.set("summm", Z);
				};
			g_phone.reconfigure(gs_phone,cm_phone);
			Update_total();
	  }
	  
	  public void Update_total()
	  {
		  gs_total.removeAll();
		  float tot=0;
		  if(use_phone.getValue())
			  if(cor_phone!=null)
			  {
				float sm=cor_phone.get("summm");
				gs_total.add(new MDModelData(heads_total(),msgs34.get("tp_calc_usl_mts"),
				cor_phone.get("name")+"("+cor_phone.get("categor")+")",sm));
				tot+=sm;
			  };
		  if(use_inet.getValue())
			  if(cor_inet!=null)
			  {
				float sm=cor_inet.get("cost");
				gs_total.add(new MDModelData(heads_total(),msgs34.get("tp_calc_usl_inet"),
				cor_inet.get("name")+"("+cor_inet.get("tech")+")",sm));
				tot+=sm;
			  };

			  if(use_iptv.getValue())
				  if(cor_iptv!=null)
				  {
					float sm=cor_iptv.get("cost");
				
			    	int sz=packs.size();
			    	for(int i=0;i<sz;++i)
			    	{
			    		CheckBox cb=packs.get(i);
			    		if(cb!=null)
			    			if(cb.getValue())
			    				sm+=(Float)cb.getData("cost");
			    	};

					gs_total.add(new MDModelData(heads_total(),msgs34.get("tp_calc_usl_iptv"),
							cor_iptv.get("name")+"("+cor_iptv.get("tech")+")",sm));
							tot+=sm;
					
				  };		  
	    ltotal.setText(msgs34.get("tp_calc_total")+tot+"\n");
		  
		  g_total.reconfigure(gs_total,cm_total);
	  }
	  
	  
	  CheckBoxGroup cbg_phone;
	  CheckBoxGroup cbg_inet;
	  CheckBoxGroup cbg_iptv;
	  
	  FieldSet 				fs_phone;
	  FieldSet 				fs_inet;
	  FieldSet 				fs_iptv;
	  List<ColumnConfig>	cc_phone;
	  List<ColumnConfig>	cc_inet;
	  List<ColumnConfig>	cc_iptv;
	  List<ColumnConfig>	cc_iptv_pack;
	  List<ColumnConfig>	cc_total;
	  ColumnModel			cm_phone;
	  ColumnModel			cm_inet;
	  ColumnModel			cm_iptv;
	  ColumnModel			cm_iptv_pack;
	  ColumnModel			cm_total;
	  Grid<MDModelData> 	g_phone;
	  Grid<MDModelData> 	g_inet;
	  Grid<MDModelData> 	g_iptv;
	  Grid<MDModelData> 	g_iptv_pack;
	  Grid<MDModelData> 	g_total;
	  Label ltotal;
	  MDModelData			cor_phone;
	  MDModelData			cor_inet;
	  MDModelData			cor_iptv;
	  CheckBox				base_pack;
	  Vector<CheckBox>		packs;
	    CheckBox use_phone;
	    CheckBox use_inet;
	    CheckBox use_iptv;
	  /**/
	    public void reset_packs()
	    {
	    	int sz=packs.size();
	    	for(int i=0;i<sz;++i)
	    	{
	    		CheckBox cb=packs.get(i);
	    		if(cb!=null)
	    			cb.setValue(false);
	    	};
	    }
	  
	  public tp_calc()
	  {
		  cor_phone=null;
		  cor_inet=null;
		  cor_iptv=null;
		    this.setSize(512, 280);
		    this.setPlain(true);
		    this.setModal(GSControl.formsmodal);
		    this.setBlinkModal(true);
		    this.setHeading(msgs34.get("tp_calc_title"));
		    
		    this.add(new Label("\n\n"+msgs34.get("tp_calc_text")+"\n\n"));

		    use_phone= new CheckBox();use_phone.setBoxLabel(" ");//msgs34.get("tp_calc_usl_mts"));
		    use_inet = new CheckBox();use_inet .setBoxLabel(" ");//msgs34.get("tp_calc_usl_inet"));
		    use_iptv = new CheckBox();use_iptv .setBoxLabel(" ");//msgs34.get("tp_calc_usl_iptv"));

		    Listener<FieldEvent> lll=
		    new Listener<FieldEvent>(){

				@Override
				public void handleEvent(FieldEvent be){
					Update_total();
				}
		    	
		    };		    
		    
		    use_phone.addListener(Events.Change,lll);		    
		    use_inet .addListener(Events.Change,lll);		    
		    use_iptv .addListener(Events.Change,lll);		    
		    
		    
		    
		    this.add(use_phone);
		    this.add(fs_phone=nfs("tp_calc_usl_mts"));
		    this.add(new Label("\n"));
		    this.add(use_inet);
		    this.add(fs_inet =nfs("tp_calc_usl_inet"));
		    this.add(new Label("\n"));
		    this.add(use_iptv);
		    this.add(fs_iptv =nfs("tp_calc_usl_iptv"));
		    this.add(new Label("\n"));
		    
		    
		    ltotal= new Label(msgs34.get("tp_calc_total")+"0\n");
		    this.add(ltotal);
		    ltotal.setStyleName("mytxt16");
		    this.setAutoHeight(true);
		    
			cbg_phone= new CheckBoxGroup();
			cbg_inet= new CheckBoxGroup();
			cbg_iptv= new CheckBoxGroup();


		    GridCellRenderer<MDModelData> phone_render = new GridCellRenderer<MDModelData>(){
		    	public CheckBoxGroup cbg=cbg_phone;
				@Override
				public Object render(MDModelData model, String property,
						ColumnData config, int rowIndex, int colIndex,
						ListStore<MDModelData> store, Grid<MDModelData> grid) {
					// TODO Auto-generated method stub
					Radio r=new Radio();
					cbg.add(r);
					r.setName("phone");r.setData("datarow", model);
					r.addListener(Events.Change, new Listener<FieldEvent>()
							{
								@Override
								public void handleEvent(FieldEvent be) {
									cor_phone=((Radio)be.getField()).getData("datarow");
									Update_total();
								};
							});
					r.setBoxLabel((String)model.get("name"));
					return r;
				} 
	    	}; 
	    	
		    GridCellRenderer<MDModelData> inet_render = new GridCellRenderer<MDModelData>(){
		    	public CheckBoxGroup cbg=cbg_inet;
				@Override
				public Object render(MDModelData model, String property,
						ColumnData config, int rowIndex, int colIndex,
						ListStore<MDModelData> store, Grid<MDModelData> grid) {
					// TODO Auto-generated method stub
					Radio r=new Radio();
					cbg.add(r);
					r.setName("inet");r.setData("datarow", model);
					r.addListener(Events.Change, new Listener<FieldEvent>()
							{
								@Override
								public void handleEvent(FieldEvent be) {
									cor_inet=((Radio)be.getField()).getData("datarow");
									Update_total();
								};
							});
					r.setBoxLabel((String)model.get("name"));
					return r;
				} 
	    	}; 
		    
		    GridCellRenderer<MDModelData> iptv_render = new GridCellRenderer<MDModelData>(){
		    	public CheckBoxGroup cbg=cbg_iptv;
				@Override
				public Object render(MDModelData model, String property,
						ColumnData config, int rowIndex, int colIndex,
						ListStore<MDModelData> store, Grid<MDModelData> grid) {
					Radio r=new Radio();
					cbg.add(r);
					r.setName("iptv");r.setData("datarow", model);
					r.addListener(Events.Change, new Listener<FieldEvent>(){
								@Override
								public void handleEvent(FieldEvent be) {
									cor_iptv=((Radio)be.getField()).getData("datarow");
									Update_total();
								};});
					r.setBoxLabel((String)model.get("name"));
					return r;
				} 
	    	}; 		    
		
	    	packs=new Vector<CheckBox>();
	    	
		    GridCellRenderer<MDModelData> iptv_pack_render = new GridCellRenderer<MDModelData>(){
				@Override
				public Object render(MDModelData model, String property,
						ColumnData config, int rowIndex, int colIndex,
						ListStore<MDModelData> store, Grid<MDModelData> grid) {
					CheckBox r=new CheckBox();
					r.setData("cost", model.get("cost"));
					int i=model.get("id");
					singtp_calc_form.packs.add(r);
					if(i==0)singtp_calc_form.base_pack=r;
					
					r.addListener(Events.Change, new Listener<FieldEvent>()
					{
						@Override
						public void handleEvent(FieldEvent be)
						{
							CheckBox c=((CheckBox)be.getField());//.getData("id");
							boolean b=(Boolean)be.getValue();
							if(c==base_pack)
							{
								if(!b)
									singtp_calc_form.reset_packs();
							}else
							{
								if(!((base_pack.getValue())&&(b)))
									c.setValue(false);
							};
							singtp_calc_form.Update_total();
						};
					});
					r.setBoxLabel((String)model.get("name"));
					return r;
				} 
	    	}; 		    
		    

		    
		    
		    
		    
		    
		    //=======================================================		    
			cc_phone = new ArrayList<ColumnConfig>();   
		    ColumnConfig 
		    column = new ColumnConfig("categor"  ,msgs34.get("tp_calc_usl_mts_c1"),100);cc_phone.add(column);
		    column = new ColumnConfig("name"     ,msgs34.get("tp_calc_usl_mts_c2"),100);cc_phone.add(column);
		    column.setAlignment(HorizontalAlignment.LEFT); 
		    column.setRenderer(phone_render);
		    column   = new ColumnConfig("cost"     ,msgs34.get("tp_calc_usl_mts_c3"),100);cc_phone.add(column);
		    column   = new ColumnConfig("un_cost"  ,msgs34.get("tp_calc_usl_mts_c4"),100);cc_phone.add(column);
		    column   = new ColumnConfig("summm"    ,msgs34.get("tp_calc_usl_mts_c5"),100);cc_phone.add(column);
		    cm_phone = new ColumnModel (cc_phone);
		    //=======================================================		    
			cc_inet  = new ArrayList<ColumnConfig>();   
		    column   = new ColumnConfig("tech"   ,msgs34.get("tp_calc_usl_inet_c1"),100);cc_inet.add(column);
		    column   = new ColumnConfig("name"   ,msgs34.get("tp_calc_usl_inet_c2"),100);cc_inet.add(column);
		    column.setAlignment(HorizontalAlignment.LEFT); 
		    column.setRenderer(inet_render);
		    column   = new ColumnConfig("cost"   ,msgs34.get("tp_calc_usl_inet_c3"),100);cc_inet.add(column);
		    column   = new ColumnConfig("speed"  ,msgs34.get("tp_calc_usl_inet_c4"),100);cc_inet.add(column);
		    column   = new ColumnConfig("un_cost",msgs34.get("tp_calc_usl_inet_c5"),100);cc_inet.add(column);
		    cm_inet  = new ColumnModel (cc_inet);
		    //=======================================================		    
			cc_iptv  = new ArrayList<ColumnConfig>();   
		    column   = new ColumnConfig("tech"  ,msgs34.get("tp_calc_usl_iptv_c1"),100);cc_iptv.add(column);
		    column   = new ColumnConfig("name"  ,msgs34.get("tp_calc_usl_iptv_c2"),100);cc_iptv.add(column);
		    column.setAlignment(HorizontalAlignment.LEFT); 
		    column.setRenderer(iptv_render);
		    column   = new ColumnConfig("cost"  ,msgs34.get("tp_calc_usl_iptv_c3"),100);cc_iptv.add(column);
		    column   = new ColumnConfig("descr" ,msgs34.get("tp_calc_usl_iptv_c4"),200);cc_iptv.add(column);
		    cm_iptv  = new ColumnModel (cc_iptv);
		    //=======================================================		    
			cc_iptv_pack = new ArrayList<ColumnConfig>();   
		    column   = new ColumnConfig("name"  ,msgs34.get("tp_calc_usl_iptv_p1"),100);cc_iptv_pack.add(column);
		    column.setAlignment(HorizontalAlignment.LEFT); 
		    column.setRenderer(iptv_pack_render);
		    
		    column   = new ColumnConfig("cost"  ,msgs34.get("tp_calc_usl_iptv_p2"),100);cc_iptv_pack.add(column);
		    column   = new ColumnConfig("descr" ," "							     ,200);cc_iptv_pack.add(column);
		    cm_iptv_pack = new ColumnModel (cc_iptv_pack);
		    //=======================================================		    
			cc_total = new ArrayList<ColumnConfig>();   
		    column   = new ColumnConfig("usl"   ,msgs34.get("tp_calc_usl" ),100);cc_total.add(column);
		    column   = new ColumnConfig("tar"   ,msgs34.get("tp_calc_tp"  ),100);cc_total.add(column);
		    column   = new ColumnConfig("summm" ,msgs34.get("tp_calc_summ"),100);cc_total.add(column);
		    cm_total = new ColumnModel (cc_total);
		    /**///=======================================================		    
		    gs_phone.groupBy("categor"); 
		    gs_inet .groupBy("tech");
		    gs_iptv .groupBy("tech");
		    fs_phone.collapse();
		    fs_inet .collapse();
		    fs_iptv .collapse();
		    fs_phone.add(new Label(msgs34.get("tp_calc_usl_mts_txt")));

		    TextField<Integer> tf= new TextField<Integer>();
		    tf.setAutoValidate(true);
		    tf.addListener(Events.Change,new Listener<FieldEvent>(){

				@Override
				public void handleEvent(FieldEvent be){
					try
					{
					int K=Integer.parseInt((String)be.getField().getValue());
					singtp_calc_form.update_phone(K);
					}catch(Exception e){Info.display("asdasd","asdds");
					};
				}
		    	
		    });
		    
		    fs_phone.add(tf);
		    fs_phone.add(new Button("Ok"));
		    fs_phone.add(g_phone     = igr(gs_phone,cm_phone,null));
		    fs_inet .add(g_inet      = igr(gs_inet ,cm_inet,null));
		    fs_iptv .add(g_iptv      = igr(gs_iptv ,cm_iptv,"descr"));
		    fs_iptv .add(g_iptv_pack = igr(gs_iptv_pack,cm_iptv_pack,"descr"));

		    this.add(g_total  = igr(gs_total ,cm_total,null));
	};
	  // =======================================================================
}