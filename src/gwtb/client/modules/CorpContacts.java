package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.abon_corp_profile;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class CorpContacts extends Window {
	  private static TreeMap<String, String> msgs22 = null;
	  private static CorpContacts form = null;

	  public static void Run() {
	    if (msgs22 == null) {
	      GSControl.ShowWait();
	      GSControl.GetGS().getMsgList(22, new AsyncCallback<TreeMap<String, String>>() {

	        public void onFailure(Throwable caught) {
	          GSControl.HideWait();
	          GSControl.ErrMsg(caught);
	        }

	        public void onSuccess(TreeMap<String, String> res) {
	          msgs22 = res;
	          GSControl.HideWait();
	          Create();
	        }
	      });
	    }
	    else {
	      Create();
	    }
	  }

	  // =======================================================================
	  private static void Create() {
	    if (form == null) {
	      form = new CorpContacts();
	    }
	    form.showForm();
	  }
	  // =======================================================================
	   public TextField<String> f1;
	   public TextField<String> f2;
	   public TextField<String> f3;
	   public TextField<String> d1;
	   public TextField<String> d2;
	   public TextField<String> d3;
	   public TextField<String> e1;
	   public TextField<String> e2;
	   public TextField<String> e3;
	   public TextField<String> p1;
	   public TextField<String> p2;
	   public TextField<String> p3;
	   public TextField<String> x1;
	   public TextField<String> x2;
	   public TextField<String> x3;
	   public TextField<String> m1;
	   public TextField<String> m2;
	   public TextField<String> m3;
	   public DateField 	  bd;
	  public CorpContacts()
	  {
        this.setWidth("500px");
	    // this.setSize(500, 300);
	    this.setPlain(true);
	    this.setModal(GSControl.formsmodal);
	    this.setBlinkModal(true);
	    this.setHeading(msgs22.get("corpcont_title"));
	    this.setResizable(false);
	    FormData formData = new FormData("-10"); 
	    f1= new TextField<String>();f1.setFieldLabel(msgs22.get("corpcont_fio"));  
	    f2= new TextField<String>();f2.setFieldLabel(msgs22.get("corpcont_fio"));
	    f3= new TextField<String>();f3.setFieldLabel(msgs22.get("corpcont_fio"));  
	    d1= new TextField<String>();d1.setFieldLabel(msgs22.get("corpcont_dolj"));  
	    d2= new TextField<String>();d2.setFieldLabel(msgs22.get("corpcont_dolj"));  
	    d3= new TextField<String>();d3.setFieldLabel(msgs22.get("corpcont_dolj"));  
	    e1= new TextField<String>();e1.setFieldLabel(msgs22.get("corpcont_email"));  
	    e2= new TextField<String>();e2.setFieldLabel(msgs22.get("corpcont_email"));  
	    e3= new TextField<String>();e3.setFieldLabel(msgs22.get("corpcont_email"));  
	    p1= new TextField<String>();p1.setFieldLabel(msgs22.get("corpcont_phone"));  
	    p2= new TextField<String>();p2.setFieldLabel(msgs22.get("corpcont_phone"));  
	    p3= new TextField<String>();p3.setFieldLabel(msgs22.get("corpcont_phone"));  
	    x1= new TextField<String>();x1.setFieldLabel(msgs22.get("corpcont_fax"));  
	    x2= new TextField<String>();x2.setFieldLabel(msgs22.get("corpcont_fax"));  
	    x3= new TextField<String>();x3.setFieldLabel(msgs22.get("corpcont_fax"));  
	    m1= new TextField<String>();m1.setFieldLabel(msgs22.get("corpcont_mob"));  
	    m2= new TextField<String>();m2.setFieldLabel(msgs22.get("corpcont_mob"));  
	    m3= new TextField<String>();m3.setFieldLabel(msgs22.get("corpcont_mob"));  
	    bd= new DateField        ();bd.setFieldLabel(msgs22.get("corpcont_bd"));
	    FieldSet fs1 = new FieldSet();fs1.setHeading(msgs22.get("corpcont_fs1"));
	    FieldSet fs2 = new FieldSet();fs2.setHeading(msgs22.get("corpcont_fs2"));
	    FieldSet fs3 = new FieldSet();fs3.setHeading(msgs22.get("corpcont_fs3"));
	    
	    this.setLayout(new FlowLayout()); 
	    
	    FormLayout l1 = new FormLayout();   
	    FormLayout l2 = new FormLayout();
	    FormLayout l3 = new FormLayout();
	    l1.setLabelWidth(175);
	    l2.setLabelWidth(175);
	    l3.setLabelWidth(175);
	    fs1.setLayout(l1);fs1.setCollapsible(true);   
	    fs2.setLayout(l2);fs2.setCollapsible(true);   
	    fs3.setLayout(l3);fs3.setCollapsible(true);   

	  //  f1.setAllowBlank(false);
	    fs1.add(f1, formData); fs2.add(f2, formData);  fs3.add(f3, formData);
	    fs1.add(d1, formData); fs2.add(d2, formData);  fs3.add(d3, formData);
	    fs1.add(bd, formData);
	    fs1.add(e1, formData); fs2.add(e2, formData); fs3.add(e3, formData);
	    fs1.add(p1, formData); fs2.add(p2, formData); fs3.add(p3, formData);
	    fs1.add(x1, formData); fs2.add(x2, formData); fs3.add(x3, formData);
	    fs1.add(m1, formData); fs2.add(m2, formData); fs3.add(m3, formData);
	    this.add(new Html("<br>"));
	    this.add(new Label(msgs22.get("corpcont_txt")));
	    this.add(new Html("<br>"));
	    this.add(fs1);
	    this.add(fs2);
	    this.add(fs3);
	    
	    Button bsubmit = new Button(msgs22.get("corpcont_submit"));
	    this.addButton(bsubmit);
	    bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>() {
	      @Override
	      public void componentSelected(ButtonEvent ce) {
	      //  if (form.afert_on.getValue() == true) {
	          GSControl.askdothis(new ClickHandler() // yes
	              {
	                public void onClick(ClickEvent event) {
	                  GSControl.ShowProcessing();
	                  GSControl.GetGS().Update_CORPcontacts(form.f1.getValue(), form.f2.getValue(), form.f3.getValue(), 
	                		  								form.d1.getValue(), form.d2.getValue(), form.d3.getValue(),
	                		  								form.e1.getValue(), form.e2.getValue(), form.e3.getValue(),
	                		  								form.p1.getValue(), form.p2.getValue(), form.p3.getValue(),
	                		  								form.x1.getValue(), form.x2.getValue(), form.x3.getValue(), 
	                		  								form.m1.getValue(), form.m2.getValue(), form.m3.getValue(), 
	                		  								form.bd.getValue(),
               		  new AsyncCallback<abon_corp_profile>() {
	                        public void onFailure(Throwable caught) {
	                          GSControl.HideProcessing();
	                          GSControl.JournalNeedReFresh = true;
	                          GSControl.ErrMsg(caught);
//	                          form.afert_on.setValue(false);
	                          form.showForm();
	                        }

	                        public void onSuccess(abon_corp_profile res) {
//	                          form.afert_on.setValue(false);
	                          GSControl.HideProcessing();
	                          GSControl.JournalNeedReFresh = true;
	                          if (GSControl.myInfo!=null)
	                        	  GSControl.myInfo.corpData=res;
	                          GSControl.OkMsg(msgs22.get("corpcont_saved"));
	                          form.showForm();
	                        };
	                      });
	                }
	              }, null, null, null); // msg
//	        }else {GSControl.ErrMsg(msgs11.get("acc_contact_needafert"));}
	      }
	    });	    
	    
	  };
	  // =======================================================================
	  public void showForm()
	  {
		  f1.setValue("");
		  f2.setValue("");
		  f3.setValue("");
		  d1.setValue("");
		  d2.setValue("");
		  d3.setValue("");
		  e1.setValue("");
		  e2.setValue("");
		  e3.setValue("");
		  p1.setValue("");
		  p2.setValue("");
		  p3.setValue("");
		  x1.setValue("");
		  x2.setValue("");
		  x3.setValue("");
		  m1.setValue("");
		  m2.setValue("");
		  m3.setValue("");
		  bd.setValue(null);
		  if (GSControl.myInfo!=null)
			  if (GSControl.myInfo.corpData!=null)
			  {
				  abon_corp_profile a=GSControl.myInfo.corpData;
				  f1.setValue(a.f1);
				  f2.setValue(a.f2);
				  f3.setValue(a.f3);
				  d1.setValue(a.d1);
				  d2.setValue(a.d2);
				  d3.setValue(a.d3);
				  e1.setValue(a.e1);
				  e2.setValue(a.e2);
				  e3.setValue(a.e3);
				  p1.setValue(a.p1);
				  p2.setValue(a.p2);
				  p3.setValue(a.p3);
				  x1.setValue(a.x1);
				  x2.setValue(a.x2);
				  x3.setValue(a.x3);
				  m1.setValue(a.m1);
				  m2.setValue(a.m2);
				  m3.setValue(a.m3);
				  bd.setValue(a.bday);
			  };
		  this.show();
	  }
	  // =======================================================================
}
