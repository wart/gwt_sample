package gwtb.client.modules;

import gwtb.client.GSControl;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class emailinfCORP extends Window {
	private static TreeMap<String, String> msgs23 = null;
	static emailinfCORP form = null;
	// =======================================================================
	public static void Run() {
		if (msgs23 == null) {
			GSControl.ShowWait();
			GSControl.GetGS().getMsgList(23,
					new AsyncCallback<TreeMap<String, String>>() {
						public void onFailure(Throwable caught) {
							GSControl.HideWait();
							GSControl.ErrMsg(caught);
						}
						public void onSuccess(TreeMap<String, String> res) {
							msgs23 = res;
							GSControl.HideWait();
							Create();
						};
					});
		} else {
			Create();
		};
	}
	// =======================================================================
	public static void Create() {
		if (form == null) {
			form = new emailinfCORP();
		};
		form.ema.setValue(GSControl.myInfo.email);
		form.accept.setValue(GSControl.myInfo.einf_schet==1);
		form.show();
	}
	// ============================================================================//
	public CheckBox accept;
	public TextField<String>  ema;
	public emailinfCORP() {

		this.setSize(470, 190);
		this.setPlain(true);
		this.setModal(GSControl.formsmodal);
		this.setBlinkModal(true);
		this.setHeading(msgs23.get("corpemailinf_title"));
		
		FormLayout lay = new FormLayout();lay.setLabelWidth(75);this.setLayout(lay);
		
		ema= new TextField<String>();ema.setFieldLabel(msgs23.get("corpemailinf_email")); 
		accept= new CheckBox();	accept.setBoxLabel(msgs23.get("corpemailinf_accept"));
		
	    
	    FormData formData = new FormData("-10"); 
		
    
	    this.add(new Html("<br>"));
	    this.add(new Label(msgs23.get("corpemailinf_txt")));
	    this.add(new Html("<br>"));
	    this.add(ema,formData);
	    this.add(accept);
		
		
		
	
		
		
		Button bsubmit = new Button(msgs23.get("corpemailinf_submit"));

		bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>() {
			@Override
			public void componentSelected(ButtonEvent ce) {

				GSControl.askdothis(new ClickHandler() // yes
						{
							public void onClick(ClickEvent event) {
								GSControl.ShowProcessing();
								GSControl.GetGS().update_CORP_einf(
										
										form.ema.getValue(),form.accept.getValue()?1:0,
										new AsyncCallback<Void>() {
											public void onFailure(
													Throwable caught) {
												GSControl.HideProcessing();
												GSControl.JournalNeedReFresh = true;
												GSControl.ErrMsg(caught);
											}
											public void onSuccess(Void res) {
												GSControl.HideProcessing();
												GSControl.JournalNeedReFresh = true;
												GSControl.myInfo.email=form.ema.getValue();
												GSControl.myInfo.einf_schet=form.accept.getValue()?1:0;
												GSControl.OkMsg(msgs23.get("corpemailinf_commit_ok"));
											};
										});
							};
						}, null, null, null);
			}
		});
		this.addButton(bsubmit);
	}
	// ============================================================================//

}
