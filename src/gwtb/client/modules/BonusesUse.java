package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.BillPeriod;
import gwtb.client.rpcdata.MDUserPeriodBonusInfo;
import gwtb.client.rpcdata.MD_Bonuses;
import gwtb.client.rpcdata.RPC_BillPeriod;
import gwtb.client.rpcdata.UserPeriodBonusInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.GroupingStore;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.grid.*;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.tips.ToolTipConfig;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;

public class BonusesUse extends Window{
	private static TreeMap<String,String> msgs19=null;
	private static BonusesUse form=null;
	//=======================================================================	
	public static  void Run()
	{
		if (msgs19==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(19,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught);
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs19=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{
			Create();
		};
	}		
	//=======================================================================	
	public static void Create()
	{
		if (form==null)
		{
			form= new BonusesUse();
			
			if (GSControl.myInfo.billData==null)
			{
			
				GSControl.ShowWait();
				GSControl.GetGS().getBillPeriods(new AsyncCallback<Vector<RPC_BillPeriod>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(Vector<RPC_BillPeriod> res)
					{
						GSControl.HideWait();
						GSControl.myInfo.billData=BillPeriod.convert(res);
						form.FinishBuildAndShow();
					};
				});
			}else{
				form.FinishBuildAndShow();
			};
			return;
		};
		form.show();
	}	
	//============================================================================//
	public void FinishBuildAndShow()
	{
		PeriodList=GSControl.myInfo.CreateBillPerListBox(PeriodList,true);
		ch=new ChangeHandler()
		{
			@Override
			public void onChange(ChangeEvent event)
			{
				if (PeriodList.getSelectedIndex()>=0)
				{
					BillPeriod bp=GSControl.myInfo.getBillPeriodByName(PeriodList.getItemText(PeriodList.getSelectedIndex())); 
					if (bp!=null)
					{
						if(bp.bonusinf!=null)
						{
							form.visualize(bp.bonusinf);
						}else
						{
							GSControl.ShowWait();
							GSControl.GetGS().getUserPeriodBonusInfo(bp.id,new AsyncCallback<UserPeriodBonusInfo>()
							{
								public void onFailure(Throwable caught)
								{
									GSControl.HideWait();
									GSControl.ErrMsg(caught); 
								}
								public void onSuccess(UserPeriodBonusInfo res)
								{
									BillPeriod bp=GSControl.myInfo.getBillPeriodById(res.bid);
									if(bp!=null)
									{
										bp.bonusinf=MDUserPeriodBonusInfo.Genereate(res);
										form.visualize(bp.bonusinf);
									}
									GSControl.HideWait();									
								};
							});
						};
					}
				}
			}
		};
		PeriodList.addChangeHandler(ch);
		ch.onChange(null);
		show();
	};
	//============================================================================//
	void visualize(MDUserPeriodBonusInfo dat)
	{

		if (dat!=null)
		{

			grid.reconfigure(dat.data, form.cm);
			
			used.setText(dat.used);
			prev.setText(dat.prev);
			total.setText(dat.Total);
		}
	}
	//============================================================================//
	Grid<MD_Bonuses> grid;
	ColumnModel cm;
	Button submit;
	Label used;
	Label prev;
	Label total;
	ListBox PeriodList;
	ChangeHandler ch;
	public BonusesUse()
	{
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading(msgs19.get("bonus_title"));
		this.setSize(500,500);
		this.setResizable(false);
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();   
		
		
		
		
		SummaryColumnConfig<Integer> desc = new SummaryColumnConfig<Integer>("Name", msgs19.get("bonus_col_name"), 65);   
	    desc.setSummaryType(SummaryType.COUNT);   
	    desc.setSummaryRenderer(new SummaryRenderer() { 
	    	@Override
	      public String render(Number value, Map<String, Number> data) {   
	        return msgs19.get("bonus_total");   
	      }
	    });
	    
	    desc.setRenderer(new GridCellRenderer<MD_Bonuses>() {   
			@Override
			public Object render(MD_Bonuses model, String property,
					ColumnData config, int rowIndex, int colIndex,
					ListStore<MD_Bonuses> store, Grid<MD_Bonuses> grid) {
				
				Integer val = (Integer) model.get("id");   
				Label l =new Label((String)model.get(property));
				l.setToolTip(new ToolTipConfig("Information",msgs19.get("bonus_desc"+val)));
	              return l;   
			}   
	          }); 
		
	    configs.add(desc); 
		/*
	    ColumnConfig column = new ColumnConfig(); column.setId("Name");column.setHeader(msgs19.get("bonus_col_name"));
	    column.setRenderer(new GridCellRenderer<MD_Bonuses>() {   
			@Override
			public Object render(MD_Bonuses model, String property,
					ColumnData config, int rowIndex, int colIndex,
					ListStore<MD_Bonuses> store, Grid<MD_Bonuses> grid) {
				
				Integer val = (Integer) model.get("id");   
				Label l =new Label((String)model.get(property));
				l.setToolTip(new ToolTipConfig("Information",msgs19.get("bonus_desc"+val)));
	              return l;   
			}   
	          });
	          column.setWidth(250);configs.add(column);   	    
	    /**/
	    
	    
	    
	       
//	    column = new ColumnConfig();  column.setId("Used"); column.setHeader(msgs19.get("bonus_col_used"));  column.setWidth(150);configs.add(column);
	    
	//    column = new ColumnConfig();  column.setId("Count");column.setHeader(msgs19.get("bonus_col_count"));   
	//    column.setWidth(150);   configs.add(column);

	    
	    SummaryColumnConfig<Double> rate = new SummaryColumnConfig<Double>("Count", msgs19.get("bonus_col_count"), 120);   
	    //rate.setNumberFormat(NumberFormat.getCurrencyFormat());   
//	    rate.setSummaryFormat(NumberFormat.getCurrencyFormat());   
	    rate.setSummaryType(SummaryType.SUM);   
	    rate.setAlignment(HorizontalAlignment.RIGHT);   
	    configs.add(rate);
	    
  /*
	    estimate.setRenderer(new GridCellRenderer<Task>() {   
	      public String render(Task model, String property, ColumnData config, int rowIndex, int colIndex,   
	          ListStore<Task> store, Grid<Task> grid) {   
	        return model.get(property) + " hours";   
	      }   
	    });   
	    estimate.setSummaryType(SummaryType.SUM);   
	    estimate.setSummaryRenderer(new SummaryRenderer() {   
	      public String render(Number value, Map<String, Number> data) {   
	        return value.intValue() + " hours";   
	      }   
	    });   
	       /**/
	    
	    
	    cm = new ColumnModel(configs);
	    /*
	    GroupSummaryView summary = new GroupSummaryView();   
	    summary.setForceFit(true);
	    summary.setShowGroupedColumn(false);
	    summary.setGroupRenderer(new GridGroupRenderer() {
		      public String render(GroupColumnData data) {
		        String f = cm.getColumnById(data.field).getHeader();
		        String l = data.models.size() == 1 ? "Item" : "Items";
		        return f + ": " + data.group + " (" + data.models.size() + " " + l + ")";   
		      }
		    });/**/
	    grid = new Grid<MD_Bonuses>(new GroupingStore<MD_Bonuses>(), cm);  
	    grid.setBorders(true);
//	    grid.setView(summary);
//	    grid.getView().setShowDirtyCells(false);
	    grid.setStyleAttribute("borderTop", "none");
	    grid.setAutoExpandColumn("Name");
	    
	    
	  
	    
	    
	    
	    
	    
	    PeriodList = new ListBox(false);
		used=new Label();
		prev=new Label();	    
		total=new Label();
		grid.setHeight(270);
	    setLayout(new FitLayout());   
	    
	    FormPanel panel = new FormPanel();   
	    panel.setBorders(false);   
	    panel.setBodyBorder(false);   
	    panel.setLabelWidth(55);   
	    panel.setPadding(5);   
	    panel.setHeaderVisible(false); 
	    panel.add(PeriodList, new FormData("100%"));
	    panel.add(new Html("<hr>"),new FormData("100%"));
	    panel.add(new Label(msgs19.get("bonus_used")),new FormData("100%"));
	    panel.add(used,new FormData("100%"));
	    panel.add(new Label(msgs19.get("bonus_prev")),new FormData("100%"));
	    panel.add(prev,new FormData("100%"));
	    panel.add(new Html("<hr>"),new FormData("100%"));
	    panel.add(grid, new FormData("100%"));
	    panel.add(new Label(msgs19.get("bonus_total")),new FormData("100%"));
	    panel.add(total,new FormData("100%"));
	    
	     

	    
	    
	    
	    submit=new Button(msgs19.get("bonus_submit"));
	    submit.addSelectionListener(
	    		new SelectionListener<ButtonEvent>()
	    		{   
	    			public void componentSelected(ButtonEvent ce)
	    			{   
	    				final MessageBox box = MessageBox.prompt(msgs19.get("bonus_input_title"), msgs19.get("bonus_input_text"));   
	    				box.addCallback(new Listener<MessageBoxEvent>()
	    						{   
	    							public void handleEvent(MessageBoxEvent be)
	    							{   
	    								int cnt=-1;
	    							
	    								try
	    								{
	    									cnt=Integer.parseInt(be.getValue());
	    								}catch(Exception e)
	    								{
	    									cnt=-1;
	    								};
	    								if (cnt>0)
	    								{
	    									GSControl.ShowWait();
	    							        GSControl.GetGS().UseBonuses(cnt, 
	    									new AsyncCallback<String>()
	    									{
	    										public void onFailure(Throwable caught)
	    										{
	    											GSControl.HideWait();
	    											GSControl.ErrMsg(caught); 
	    										}
	    										public void onSuccess(String res)
	    										{
	    											GSControl.HideWait();
	    											GSControl.OkMsg(res);
	    											BillPeriod bp=GSControl.myInfo.getBillPeriodByName(PeriodList.getItemText(PeriodList.getSelectedIndex()));;
	    											if(bp!=null)
	    											{
	    												if(bp.bonusinf!=null)
	    												{
	    													bp.bonusinf.data=null;
	    													ch.onChange(null);
	    												}
	    												form.visualize(bp.bonusinf);
	    											}
	    										};
	    									});
	    								
	    								}else{
	    									GSControl.ErrMsg(msgs19.get("bonus_input_error"));
	    								}
	    							}   
	    						});   
	    			}   
	    		});
	    this.addButton(submit);
	    
	    add(panel);
	}
	//============================================================================//
}
//============================================================================//
