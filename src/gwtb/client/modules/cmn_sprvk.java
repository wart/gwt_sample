package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.BillPeriod;
import gwtb.client.rpcdata.MDModelData;
import gwtb.client.rpcdata.RPC_BillPeriod;
import gwtb.client.rpcdata.rpc_cmn_sprvk;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;

public class cmn_sprvk extends Window {
	  private static TreeMap<String, String> msgs28 = null;
	  private static cmn_sprvk form = null;

	  public static void Run() {
	    if (msgs28 == null) {
	      GSControl.ShowWait();
	      GSControl.GetGS().getMsgList(28, new AsyncCallback<TreeMap<String, String>>() {

	        public void onFailure(Throwable caught) {
	          GSControl.HideWait();
	          GSControl.ErrMsg(caught);
	        }

	        public void onSuccess(TreeMap<String, String> res) {
	          msgs28 = res;
	          GSControl.HideWait();
	          Create();
	        }
	      });
	    }
	    else {
	      Create();
	    }
	  }

	  // =======================================================================
	  private static void Create()
	  {
		  if (form == null)
		  {
			  form = new cmn_sprvk();
			  if (GSControl.myInfo.billData==null)
			  {
				  GSControl.ShowWait();
				  GSControl.GetGS().getBillPeriods(new AsyncCallback<Vector<RPC_BillPeriod>>()
				  {
					  public void onFailure(Throwable caught)
					  {
						  GSControl.HideWait();
						  GSControl.ErrMsg(caught); 
					  }
					  public void onSuccess(Vector<RPC_BillPeriod> res)
					  {
						  GSControl.HideWait();
						  GSControl.myInfo.billData=BillPeriod.convert(res);
						  form.showForm();
					  };
				  });
			  }else{
				  form.showForm();
			  };
			  return;	      
		  }
		  form.showForm();
	  }
	  // =======================================================================
	  private ColumnModel sld_cm;
	  private ListStore<MDModelData> sld;
	  private List<ColumnConfig> sld_configs;
	  private Grid<MDModelData> sld_grid;
  
	  
	  private ColumnModel nach_cm;
	  private ListStore<MDModelData> nach;
	  private List<ColumnConfig> nach_configs;
	  private Grid<MDModelData> nach_grid;

	  
	  private ColumnModel pay_cm;
	  private ListStore<MDModelData> pay;
	  private List<ColumnConfig> pay_configs;
	  private Grid<MDModelData> pay_grid;

	  
	  public ListBox	PeriodList;
	  public ChangeHandler ch;
	  
	  public cmn_sprvk()
	  {
			this.setSize(500, 500);   
			this.setPlain(true);   
			this.setModal(GSControl.formsmodal);   
			this.setBlinkModal(true);   
			this.setHeading(msgs28.get("cmn_sprvk_title"));
			this.setMaximizable(true);
			
			
		    
		    sld=new ListStore<MDModelData>();
		    sld_configs = new ArrayList<ColumnConfig>();   
		    ColumnConfig 
		    column = new ColumnConfig(BillPeriod.sld_heads()[0]  ,msgs28.get("cmn_sprvk_in_saldo"	), 100);sld_configs.add(column);
		    column = new ColumnConfig(BillPeriod.sld_heads()[1]  ,msgs28.get("cmn_sprvk_nach"  		), 100);sld_configs.add(column);
		    column = new ColumnConfig(BillPeriod.sld_heads()[2]  ,msgs28.get("cmn_sprvk_opl"   		), 100);sld_configs.add(column);
		    column = new ColumnConfig(BillPeriod.sld_heads()[3]  ,msgs28.get("cmn_sprvk_out_saldo" 	), 100);sld_configs.add(column);
		    sld_cm = new ColumnModel(sld_configs);
		    sld_grid = new Grid<MDModelData>(sld, sld_cm);
		    sld_grid.setStyleAttribute("borderTop", "none");   
		    sld_grid.setAutoExpandColumn(BillPeriod.sld_heads()[3]);   
		    sld_grid.setBorders(true);   
		    sld_grid.setStripeRows(true);				  
		    sld_grid.setHeight(60);
		  
		    
			nach=new ListStore<MDModelData>();
			nach_configs = new ArrayList<ColumnConfig>();   
		    column = new ColumnConfig(BillPeriod.nach_heads()[0]  ,msgs28.get("cmn_sprvk_uslugi"), 100);nach_configs.add(column);
		    column = new ColumnConfig(BillPeriod.nach_heads()[1]  ,msgs28.get("cmn_sprvk_nach"  ), 100);nach_configs.add(column);
		    column = new ColumnConfig(BillPeriod.nach_heads()[2]  ,msgs28.get("cmn_sprvk_nds"   ), 100);nach_configs.add(column);
		    column = new ColumnConfig(BillPeriod.nach_heads()[3]  ,msgs28.get("cmn_sprvk_itogo" ), 100);nach_configs.add(column);
		    nach_cm = new ColumnModel(nach_configs);
		    nach_grid = new Grid<MDModelData>(nach, nach_cm);
		    nach_grid.setStyleAttribute("borderTop", "none");   
		    nach_grid.setAutoExpandColumn(BillPeriod.nach_heads()[3]);   
		    nach_grid.setBorders(true);   
		    nach_grid.setStripeRows(true);		
		    nach_grid.setHeight(120);
		    
		    pay=new ListStore<MDModelData>();
		    pay_configs = new ArrayList<ColumnConfig>();   
		    column = new ColumnConfig(BillPeriod.pay_heads()[0]  ,msgs28.get("cmn_sprvk_uslugi"), 100);pay_configs.add(column);
		    column = new ColumnConfig(BillPeriod.pay_heads()[1]  ,msgs28.get("cmn_sprvk_nach"  ), 100);pay_configs.add(column);
		    column = new ColumnConfig(BillPeriod.pay_heads()[2]  ,msgs28.get("cmn_sprvk_prim"  ), 100);pay_configs.add(column);
		    pay_cm = new ColumnModel(pay_configs);
		    pay_grid = new Grid<MDModelData>(pay, pay_cm);
		    pay_grid.setStyleAttribute("borderTop", "none");   
		    pay_grid.setAutoExpandColumn(BillPeriod.pay_heads()[2]);   
		    pay_grid.setBorders(true);   
		    pay_grid.setStripeRows(true);
		    pay_grid.setHeight(160);
		    
		    PeriodList= new ListBox();
			ch=new ChangeHandler()
			{
				@Override
				public void onChange(ChangeEvent event)
				{
					if (PeriodList.getSelectedIndex()>=0)
					{
						BillPeriod bp=GSControl.myInfo.getBillPeriodByName(PeriodList.getItemText(PeriodList.getSelectedIndex())); 
						if (bp!=null)
						{
							if(bp.sld!=null)
							{
								form.visualize(bp);
							}else
							{
								GSControl.ShowWait();
								GSControl.GetGS().getcmn_sprvk(bp.id,new AsyncCallback<Vector<rpc_cmn_sprvk>>()
								{
									public void onFailure(Throwable caught)
									{
										GSControl.HideWait();
										GSControl.ErrMsg(caught); 
									}
									public void onSuccess(Vector<rpc_cmn_sprvk> res)
									{
										BillPeriod bp=GSControl.myInfo.getBillPeriodByName(form.PeriodList.getItemText(PeriodList.getSelectedIndex()));
										if(bp!=null)
										{
											bp.fill(res);
											form.visualize(bp);
										}
										
										GSControl.HideWait();									
									};
								});
							};
						}
					}
				}
			};
			PeriodList.addChangeHandler(ch);
			Label l1= new Label(msgs28.get("cmn_sprvk_billper"   ));
			Label l2= new Label(msgs28.get("cmn_sprvk_nach_us"   ));
			Label l3= new Label(msgs28.get("cmn_sprvk_pays"   ));
			//setLayout(new FitLayout());
			this.add(new Html("<br>"));
			this.add(l1);
			this.add(PeriodList);
			this.add(new Html("<br>"));
			this.add(sld_grid);
			this.add(new Html("<br>"));
			this.add(l2);
			this.add(nach_grid);
			this.add(new Html("<br>"));
			this.add(l3);
			this.add(pay_grid);
	  };
	// =======================================================================	
	  protected void visualize(BillPeriod bp)
	  {
		  pay_grid .reconfigure(bp.pay , pay_cm);
		  nach_grid.reconfigure(bp.nach,nach_cm);
		  sld_grid .reconfigure(bp.sld , sld_cm);
	  }
	  // =======================================================================	  
	  private void showForm()
	  {
		  PeriodList=GSControl.myInfo.CreateBillPerListBox(PeriodList, false);
			ch.onChange(null);
			show();
	  }
	  // =======================================================================	  
}
