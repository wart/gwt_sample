package gwtb.client.modules;

import gwtb.client.GSControl;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;


public class radiuspwdch extends Window
{
	private static TreeMap<String,String> msgs6=null;
	private static radiuspwdch form=null;
	public static void Run()
	{
		if (msgs6==null)
		{
			GSControl.ShowWait();
	        GSControl.GetGS().getMsgList(6,
			new AsyncCallback<TreeMap<String,String>>()
			{
				public void onFailure(Throwable caught)
				{
					GSControl.HideWait();
					GSControl.ErrMsg(caught); 
				}
				public void onSuccess(TreeMap<String,String> res)
				{
					msgs6=res;
					GSControl.HideWait();
					Create();
				};
			});
		}else
		{Create();};
	}		
	//=======================================================================	
	public static void Create()
	{
		//Window.alert("create_bron_form()");
		if (form==null)
		{
			form= new radiuspwdch();
		};
		form.show();
	}	
//============================================================================//
	private Button bsubmit;
	private TextField<String> login;
	private TextField<String> oldpwd;
	private TextField<String> newpwd;
	private TextField<String> newpwd2;
//	private VerticalPanel wd;
	
	public radiuspwdch()
	{
	//	this.setSize(350, 400);   
		this.setWidth(350);
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading(msgs6.get("inet_pwd_title")); 	
		this.setResizable(false);
		




		bsubmit = new Button(msgs6.get("inet_pwd_submit"));
//		login= new TextBox();
//		oldpwd= new PasswordTextBox();
//		newpwd= new PasswordTextBox();
//		newpwd2= new PasswordTextBox();
		/*
		wd= new VerticalPanel();
		
		wd.setWidth("320px");
		wd.addStyleName("dialogVPanel");
		wd.add(new Label(msgs6.get("inet_pwd_txt2")));
		wd.add(new HTML("<b>"+msgs6.get("inet_pwd_login")+"</b>"));
		wd.add(login);
		wd.add(new HTML("<b>"+msgs6.get("inet_pwd_oldpwd")+"</b>"));
		wd.add(oldpwd);
		wd.add(new HTML("<b>"+msgs6.get("inet_pwd_newpwd")+"</b>"));
		wd.add(newpwd);
		wd.add(new HTML("<b>"+msgs6.get("inet_pwd_newpwd2")+"</b>"));
		wd.add(newpwd2);
		wd.setHorizontalAlign(HorizontalAlignment.CENTER);
		wd.add(new Label(msgs6.get("inet_pwd_txt")));
		

		/**/
		
		   login  = new TextField<String>();login  .setFieldLabel(msgs6.get("inet_pwd_login"));  
		   oldpwd = new TextField<String>();oldpwd .setFieldLabel(msgs6.get("inet_pwd_oldpwd" )); oldpwd .setPassword(true);
		   newpwd = new TextField<String>();newpwd .setFieldLabel(msgs6.get("inet_pwd_newpwd" )); newpwd .setPassword(true);
		   newpwd2= new TextField<String>();newpwd2.setFieldLabel(msgs6.get("inet_pwd_newpwd2")); newpwd2.setPassword(true);
		   login.setAutoValidate(true);login.setAllowBlank(false);
		   oldpwd.setAutoValidate(true);oldpwd.setAllowBlank(false);
		   newpwd.setAutoValidate(true);newpwd.setAllowBlank(false);
		   newpwd2.setAutoValidate(true);newpwd2.setAllowBlank(false);
		   newpwd2.setValidator(new Validator(){

				@Override
				public String validate(Field<?> arg0, String arg1) {
					if(!(form.newpwd.getValue().equals(form.newpwd2.getValue()))) return msgs6.get("inet_pwd_diffnpwd");
					return null;
				}
				   
			   });
		
		   FormLayout lay = new FormLayout();
		   lay.setLabelWidth(200);
		   this.setLayout(lay);
		   FormData formData = new FormData("-24");		   
		   
		   this.add(new Html("<br>"));
		   this.add(new Label(msgs6.get("inet_pwd_txt2")));
		   this.add(new Html("<br>"));
		   this.add(login,formData);
		   this.add(oldpwd,formData);
		   this.add(newpwd,formData);
		   this.add(newpwd2,formData);
		   this.add(new Html("<br>"));
		   this.add(new Label(msgs6.get("inet_pwd_txt")));		
		
		
		
		
		
		
//		wd.add(Errmsg);
		this.addButton(bsubmit);

		bsubmit.addSelectionListener( 
				 new SelectionListener<ButtonEvent>() {   

					      @Override  
					    public void componentSelected(ButtonEvent ce)
						{
	

					    		if (login.getValue().length()==0){GSControl.ErrMsg(msgs6.get("inet_pwd_emplog"));return;};
								if (oldpwd.getValue().length()==0){GSControl.ErrMsg(msgs6.get("inet_pwd_empopwd"));return;};
								if (newpwd.getValue().length()<8 ){GSControl.ErrMsg(msgs6.get("inet_pwd_empnpwd"));return;};				
								if (newpwd.getValue().compareTo(newpwd2.getValue())!=0){GSControl.ErrMsg(msgs6.get("inet_pwd_diffnpwd"));return;};
								
								GSControl.askdothis(new ClickHandler()  //yes
								  {
							          public void onClick(ClickEvent event)
							          {
							        	  GSControl.ShowProcessing();	
							        	  GSControl.GetGS().ChangeRadiusPwd(form.login.getValue(), form.oldpwd.getValue(), 
							        			  form.newpwd.getValue(), form.newpwd2.getValue(),new AsyncCallback<String>()
											{
								        		public void onFailure(Throwable caught)
								        		{
								        			GSControl.HideProcessing();
								        			GSControl.JournalNeedReFresh=true;
								        			GSControl.ErrMsg(caught);
												}
												public void onSuccess(String res)
												{
							        				GSControl.HideProcessing();
								        			GSControl.JournalNeedReFresh=true;	
								        			GSControl.OkMsg(res);   		
												};
											});
							          };
						          },null,msgs6.get("inet_pwd_ask")+"\""+form.login.getValue()+"\"?",null);								    	  
						}
				 });
		
//		this.add(wd);
	}
}
