package gwtb.client.modules;

import gwtb.client.GSControl;
import gwtb.client.rpcdata.wrkitem;

import java.util.TreeMap;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;

public class CorpQuestions  extends Window
{


	private static TreeMap<String,String> msgs25=null;
	private static TreeMap<Integer,wrkitem> wrklst9=null;
	
	private static CorpQuestions form=null;
	//============================================================================//	
	public static void Run()
	{
		if (msgs25==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(25,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught);
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs25=res;
						GSControl.HideWait();
						Load();
					};
				});
		}else
		{Load();};
	}		
	//=======================================================================
	private static void Load()
	{
		if (wrklst9==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getWRKList(9,
				new AsyncCallback<TreeMap<Integer,wrkitem>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught);
						
					}
					public void onSuccess(TreeMap<Integer,wrkitem> res)
					{
						wrklst9=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{Create();};
	}	
	//=======================================================================
	static private void Create()
	{
		if (form==null)
		{
			form= new CorpQuestions();
		};
		form.account.setText(GSControl.myInfo.account);
		form.naming.setText(GSControl.myInfo.name);
		form.show();
	}
	//=======================================================================
	public Label account;
	public Label naming;
	public Label lwho;
	public Label lthm;
	public Label lbod;
	public TextArea body;
	public TextBox title;
	public ListBox who;
	public int sv;
	
	public boolean FormValid()
	{
		return (title.getText().length()>4)&&(body.getText().length()>10);
	}
	
	public CorpQuestions()
	{
		this.setWidth(415);
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading(msgs25.get("CorpQuest_title")); 	
		this.setLayout(new FlowLayout()); 
		account=new Label();
		naming =new Label();
		lwho   =new Label(msgs25.get("CorpQuest_who"));
		lthm   =new Label(msgs25.get("CorpQuest_theme"));
		lbod   =new Label(msgs25.get("CorpQuest_body"));
		
		who=GSControl.buildWRKListBox(wrklst9);
		
		title = new TextBox();
			body = new TextArea();
		body.ensureDebugId("cwBasicText-textarea");
		body.setWidth("100%");
		body.setVisibleLines(7);
		
		VerticalPanel wd=new VerticalPanel();
		wd.setHorizontalAlign(HorizontalAlignment.CENTER);
		wd.setWidth("100%");
		wd.add(account);
		wd.add(naming);
		wd.add(new Html("<br>"));
		wd.add(lwho);
		wd.add(who);
		wd.add(lthm);
		wd.add(title);
		wd.add(lbod);
		wd.add(body);
		this.add(wd);
		Button bsubmit = new Button(msgs25.get("CorpQuest_submit"));
		bsubmit.addSelectionListener(new SelectionListener<ButtonEvent>()
			{
				@Override
				public void componentSelected(ButtonEvent ce)
				{
					int sw=who.getSelectedIndex();
					if (wrklst9.get(sw)!=null)
					{
						form.sv=wrklst9.get(sw).id;
						if (GSControl.myInfo.CorpCanSendQuestion(form.sv))
						{
							if(form.FormValid())
							{
								GSControl.askdothis(new ClickHandler() // yes
								{
									public void onClick(ClickEvent event) {
									GSControl.ShowProcessing();
									GSControl.GetGS().SendCorpQuestion(
											form.title.getText(),
											form.body.getText(),
											form.sv,
											new AsyncCallback<String>() {
												public void onFailure(
														Throwable caught) {
													GSControl.HideProcessing();
													GSControl.JournalNeedReFresh = true;
													GSControl.ErrMsg(caught);
												}
												public void onSuccess(String res) {
													GSControl.HideProcessing();
													GSControl.JournalNeedReFresh = true;
													GSControl.OkMsg(msgs25.get("CorpQuest_submit_ok"));
												};
											});
									};
								}, null, null, null);
							/**/
							}else{
								GSControl.OkMsg(msgs25.get("CorpQuest_form_err"));
							};
						}else{
							GSControl.OkMsg(msgs25.get("CorpQuest_contact_err"));
						}		
					};
				}
			});
		this.addButton(bsubmit);
	}
	//=======================================================================	
}
