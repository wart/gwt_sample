package gwtb.client.modules;

import gwtb.client.GSControl;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;


public class OperAuthForm extends Window
{

	protected static OperAuthForm form=null;
	public static void Run()
	{
		form=new OperAuthForm();form.show();
	}
	
	private Button blogon;
	private TextBox flogin;
	private PasswordTextBox fpwd;
	private VerticalPanel dvp;
	//public CheckBox ofert;
	

	public OperAuthForm()
	{
	
		this.setWidth(150);
		this.setPlain(true);   
		this.setModal(false);   
		this.setBlinkModal(true);   
		this.setHeading(GSControl.msgs1.get("auth_title")); 			
		

		blogon = new Button(GSControl.msgs1.get("auth_enter"));
		flogin = new TextBox(); 
		fpwd = new PasswordTextBox();
		//ofert=new CheckBox();ofert.setBoxLabel(GSControl.msgs1.get("auth_ofert"));
	
//		blogon.addSelectionListener.addClickHandler(handler);
//		fpwd.addKeyUpHandler(handler);
		blogon.addSelectionListener(
		new SelectionListener<ButtonEvent>()
		{   
			@Override  
		    public void componentSelected(ButtonEvent ce)
			{
		    		  GSControl.ShowWait();
		    		  // TODO:
		    	/*	  GSControl.GetGS().auth(flogin.getText(),fpwd.getText(), 
		    		  new AsyncCallback<abon_profile>()
		    		  {
		    			  public void onFailure(Throwable caught)
		    			  {
								GSControl.HideWait();
								GSControl.ErrMsg(caught.getMessage());
		    			  }
		    			  public void onSuccess(abon_profile result)
		    			  {
		    				  GSControl.myInfo=result;
		    				  form.hide();
		    				  GSControl.HideWait();
		    				  GSControl.LoadStages();
		    			  }
		    		  });
		    		  /**/

			}
		});
		
		
		dvp = new VerticalPanel();
		dvp.setWidth("100%");
		dvp.addStyleName("dialogVPanel");
		dvp.add(new HTML("<b>"+GSControl.msgs1.get("auth_login")+"</b>"));
		dvp.add(flogin);flogin.setWidth("174px");
		dvp.add(new HTML("<br><b>"+GSControl.msgs1.get("auth_pwd")+"</b>"));
		dvp.add(fpwd);fpwd.setWidth("174px");
		dvp.add(new HTML("<br>"));
		dvp.setHorizontalAlign(HorizontalAlignment.CENTER);
		this.add(dvp);	
		this.addButton(blogon);

	}
};