package gwtb.client.modules;

import gwtb.client.GSControl;
import java.util.Date;
import java.util.TreeMap;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;


public class Change09  extends Window
{
	
	private static TreeMap<String,String> msgs18=null;
	private static Change09 form=null;
	
	//=======================================================================
	public static void Run()
	{
		if (msgs18==null)
		{
			GSControl.ShowWait();
		        GSControl.GetGS().getMsgList(18,
				new AsyncCallback<TreeMap<String,String>>()
				{
					public void onFailure(Throwable caught)
					{
						GSControl.HideWait();
						GSControl.ErrMsg(caught); 
					}
					public void onSuccess(TreeMap<String,String> res)
					{
						msgs18=res;
						GSControl.HideWait();
						Create();
					};
				});
		}else
		{Create();};
	}

	//=======================================================================
	public static void Create()
	{
		if (form==null)
		{
			form= new Change09();
		};
		form.update_form();
		form.show();
	}	
	//=======================================================================
	public CheckBox aferta;
	public ListBox Phones;
	public CheckBox in09;
	public VerticalPanel wd;
	public Label addr;
	public Label l3;

	
	//-------------------------------------------------------------------
	public Change09()
	{
		
		//this.setSize(500, 300);   
		this.setPlain(true);   
		this.setModal(GSControl.formsmodal);   
		this.setBlinkModal(true);   
		this.setHeading(msgs18.get("ph_09_title")); 		
		this.setWidth("600");
		this.setResizable(false);
		
		addr=new Label(); addr.setStyleName("mytxt");
		in09=new CheckBox();in09.setBoxLabel(msgs18.get("ph_09_accept"));
		Phones=new ListBox();
		
		HorizontalPanel hp= new HorizontalPanel();
		hp.add(Phones);			
		hp.add(in09);
		
		wd=new VerticalPanel();
		wd.setHorizontalAlign(HorizontalAlignment.CENTER);
		Label l=null;
		
		
		wd.add(l= new Label(msgs18.get("ph_09_txt1")));l.setStyleName("mytxt");
		wd.add(new HTML("<hr>"));
		wd.add(l= new Label(msgs18.get("ph_09_af")));l.setStyleName("mytxt");
		wd.add(new HTML(" <a href=\""+msgs18.get("ph_09_af_lnk")+"\" target=blank>"+msgs18.get("ph_09_af_cap" )+"<a/>"));
		wd.add(new HTML("<hr>"));
		
		aferta  = new CheckBox();
		aferta.setBoxLabel(msgs18.get("ph_09_af_ok"));

		//aferta.setStyleName("mytxt");
		wd.add(aferta);
		
		wd.add(new HTML("<hr>"));
		wd.add(l= new Label(msgs18.get("ph_09_txt2")));l.setStyleName("mytxtJ");
		wd.add(new HTML("<hr>"));
		wd.add(hp);
		wd.add(new HTML("<br>"));
		wd.add(addr);

		wd.add(new HTML("<hr>"));
		wd.add(l= new Label(msgs18.get("ph_09_txt3")));l.setStyleName("mytxtJ");
	    Button bsubmit;
	    this.addButton(bsubmit= new Button(msgs18.get("ph_09_submit")));

		bsubmit.addSelectionListener( 
				 new SelectionListener<ButtonEvent>()
				 {   
				  	@Override  
					public void componentSelected(ButtonEvent ce)
					{
				  		if(form.aferta.getValue())
						{
							GSControl.ShowProcessing();						
							GSControl.GetGS().Change09State(form.Phones.getItemText(form.Phones.getSelectedIndex()),
									form.in09.getValue(),
							new AsyncCallback<String>()
							{
								public void onFailure(Throwable caught)
								{
				        			GSControl.HideProcessing();
				        			GSControl.JournalNeedReFresh=true;
				        			GSControl.ErrMsg(caught); 
								}
								public void onSuccess(String res)
								{
									GSControl.HideProcessing();
				        			GSControl.JournalNeedReFresh=true;
			        				GSControl.myInfo.phones.get(Phones.getSelectedIndex()).in09=form.in09.getValue()?1:0;
				        			form.aferta.setValue(false);
				        			GSControl.OkMsg( res);   							
								};
							});
						}else
						{
							GSControl.ErrMsg(msgs18.get("ph_09_ErrAferta")); 
						}
					}
				});


		
		l3=new Label();l3.setStyleName("mytxtJ");
		wd.add(l3);			
		
		this.add(wd);
		Phones.addChangeHandler(new ChangeHandler(){
			@Override
			public void onChange(ChangeEvent event) {form.update_form();}
		});			
	}
	
	public void update_form()
	{
		l3.setText(GSControl.myInfo.account+": "+GSControl.myInfo.name+"    ("+new Date().toString()+")");
		Phones=GSControl.buildPhoneListBox(Phones,0);
		int idx=Phones.getSelectedIndex();
		if (idx>=0)
		{
			addr.setText(GSControl.myInfo.phones.get(idx).addr);
			int s=GSControl.myInfo.phones.get(idx).in09;
			in09.setValue(s==0?false:true);
		}else{
			addr.setText("");
			in09.setValue(false);						
		}
	}
}				
//=======================================================================
