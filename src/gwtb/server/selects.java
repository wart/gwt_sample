package gwtb.server;

import gwtb.client.rpcdata.ADM_TRACE_ITEM;
import gwtb.client.rpcdata.Bonuses;
import gwtb.client.rpcdata.DVOclipart;
import gwtb.client.rpcdata.DVOsrvpart;
import gwtb.client.rpcdata.Jrnl_rec;
import gwtb.client.rpcdata.News;
import gwtb.client.rpcdata.RPC_BillPeriod;
import gwtb.client.rpcdata.ServiceExcept;
import gwtb.client.rpcdata.UserPeriodBonusInfo;
import gwtb.client.rpcdata.period_call_g;
import gwtb.client.rpcdata.period_call_l;
import gwtb.client.rpcdata.period_calls_lg_data;
import gwtb.client.rpcdata.rpc_cmn_sprvk;
import gwtb.client.rpcdata.rpc_raz_post_usl;
import gwtb.client.rpcdata.rpc_tp_calc;
import gwtb.client.rpcdata.rpc_tp_calc_inet;
import gwtb.client.rpcdata.rpc_tp_calc_iptv;
import gwtb.client.rpcdata.rpc_tp_calc_iptv_pack;
import gwtb.client.rpcdata.rpc_tp_calc_phone;
import gwtb.client.rpcdata.search_userData;
import gwtb.client.rpcdata.user_connection;
import gwtb.client.rpcdata.user_phone;
import gwtb.client.rpcdata.wrkitem;
import gwtb.core.PULLS.CP_DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.TreeMap;
import java.util.Vector;


public class selects {
  // --------------------------------------------------------------------------------
  public static void Log(String str) {
    System.err.println(new Date() + " >> " + str);
  }
  // --------------------------------------------------------------------------------
  static public TreeMap<String, String> getMsgList(int listid) throws ServiceExcept
  {
	  return new CHH<TreeMap<String, String> >()
	  {
		  public void call() throws Exception
		  {
			  ret = new TreeMap<String, String> ();
			  while (haveres)
			  {
				  ret.put(rs.getString(1), rs.getString(2));
				  haveres = rs.next();
			  }
		  };
	  }.RunP(SQL.getMsgList, "getMsgList",false,listid);
  }
  // --------------------------------------------------------------------------------
  static public TreeMap<Integer, String> getINETERRS() throws ServiceExcept
  {
	  return new CHH<TreeMap<Integer, String> >()
	  {
		  public void call() throws Exception
		  {
			  ret = new TreeMap<Integer, String> ();
			  while (haveres)
			  {
				  ret.put(rs.getInt(1), rs.getString(2));
				  haveres = rs.next();
			  }
		  };
	  }.RunP(SQL.getINETERRS, "getINETERRS",false);
  }
  // --------------------------------------------------------------------------------
  static public TreeMap<Integer, wrkitem> getWRKList(int listid) throws ServiceExcept
  {
   	  return new CHH<TreeMap<Integer, wrkitem>>()
   	  {
   		  public void call() throws Exception
   		  {
   			  ret = new TreeMap<Integer, wrkitem>();
   			  int rid = 0;
   			  while (haveres)
   			  {
   				  ret.put(rid, new wrkitem(rs.getInt(1), rs.getString(2), rs.getInt(3)));
   				  haveres = rs.next();
   				  ++rid;
   			  }
   		  };
   	  }.RunP(SQL.getWRKList, "getWRKList",false,listid);
  }
  // --------------------------------------------------------------------------------
  static public TreeMap<Integer, user_connection> getUserConnection(Long user_id) throws ServiceExcept
  {
	  return new CHH<TreeMap<Integer, user_connection>>()
	  {
		  public void call() throws Exception
		  {
			  ret = new TreeMap<Integer, user_connection>();
			  int rid = 0;
			  while (haveres)
			  {
				  ret.put(rid, new user_connection(rs.getString(1), rs.getInt(2),rs.getString(3)));
				  haveres = rs.next();
				  ++rid;
			  }
		  };
	  }.RunP(SQL.getUserConnection, "getUserConnection",false,user_id);
  }
  // --------------------------------------------------------------------------------
  static public TreeMap<Integer, user_phone> getUserPhones(Long user_id) throws ServiceExcept
  {
	  return new CHH<TreeMap<Integer, user_phone>>()
	  {
		  public void call() throws Exception,ServiceExcept
		  {
			  ret = new TreeMap<Integer, user_phone>();
			  int rid = 0;
			  while (haveres)
			  {
				  user_phone up = new user_phone(rs.getString(1), rs.getInt(2), rs.getDouble(3), rs.getInt(4), rs.getString(5),rs.getInt(6), rs.getInt(7),rs.getInt(9)
						  ,rs.getInt(10),rs.getInt(11));
				  Log("Get DVO For :" + rs.getString(1));
				  if (up.allowDVO>0)
					  up.dvo =new CHH<Vector<DVOclipart>>()
					  {
					  	public void call() throws Exception
					  	{
					  	  ret = new Vector<DVOclipart>();
						  while (haveres)
						  {
							  ret.add(new DVOclipart(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getString(6)));
							  haveres = rs.next();
						  };
					    };
				       }.RunP(SQL.getuserDVOList, "getuserDVOList",false,rs.getLong(8),rs.getString(1));
				       
				  ret.put(rid, up);
				  haveres = rs.next();
				  ++rid;
			  }
		  };
	  }.RunP(SQL.getUserPhones, "getUserPhones",false,user_id);
  }
  // --------------------------------------------------------------------------------
  static public Vector<DVOsrvpart> getDVOList() throws ServiceExcept
  {
	  return new CHH<Vector<DVOsrvpart>>()
	  {
		  public void call() throws Exception
		  {
			  ret = new Vector<DVOsrvpart>();
			  while (haveres)
			  {
				  ret.add(new DVOsrvpart(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getInt(6),rs.getInt(7), rs.getInt(8)));
				  haveres = rs.next();
			  };
		  };
	  }.RunP(SQL.getDVOList, "getDVOList",false);
  }
  // --------------------------------------------------------------------------------
  static public Vector<DVOclipart> getuserDVOList(Long usr_id, String devid) throws ServiceExcept
  {
	  return new CHH<Vector<DVOclipart>>()
	  {
		  public void call() throws Exception
		  {
			  ret = new Vector<DVOclipart>();
			  while (haveres)
			  {
				  ret.add(new DVOclipart(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getString(6)));
				  haveres = rs.next();
			  };
		  };
	  }.RunP(SQL.getuserDVOList, "getuserDVOList",false,usr_id,devid);
  }/**/
  // --------------------------------------------------------------------------------
  static public Vector<News> getNewsTitles() throws ServiceExcept
  {
	  return new CHH<Vector<News>>()
	  {
		  public void call() throws Exception
		  {
			  ret = new Vector<News>();
			  while (haveres)
			  {
				  ret.add(new News(rs.getInt(1), rs.getString(2)));
				  haveres = rs.next();
			  };
		  };
	  }.RunP(SQL.getNewsTitles, "getNewsTitles",false);
  }
  // --------------------------------------------------------------------------------
  static public String getNewsBody(int id) throws ServiceExcept
  {
	  return new CHH<String>()
	  {
		  public void call() throws Exception
		  {
			  if (haveres)
			  {
				  ret=rs.getString(1);
			  }
		  };
	  }.RunP(SQL.getNewsBody, "getNewsBody",false,id);
  }
  // --------------------------------------------------------------------------------
  static public Vector<Jrnl_rec> getJournal(Long usrId) throws ServiceExcept
  {
   	  return new CHH<Vector<Jrnl_rec>>()
   	  {
   		  public void call() throws Exception
   		  {
   			  ret = new Vector<Jrnl_rec>();
   			  while (haveres)
   			  {
   				  ret.add(new Jrnl_rec(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5)));
   				  haveres = rs.next();
   			  };
   		  };
   	  }.RunP(SQL.getJournal, "getJournal",false,usrId);
  };
  // --------------------------------------------------------------------------------
  public static Vector<Bonuses> getBonuses(Long usrId, Long BID) throws ServiceExcept
  {
	  return new CHH<Vector<Bonuses>>()
	  {
		  public void call() throws Exception
		  {
			  ret = new Vector<Bonuses>();
			  while (haveres)
			  {
				  ret.add(new Bonuses(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
				  haveres = rs.next();
			  };
		  };
	  }.RunP(SQL.getBonuses, "getBonuses",false,usrId,BID);
  }
  // --------------------------------------------------------------------------------
/*  public static Vector<UserBonusPeriodListItem> getUserBonusPeriodList(Long usrid) throws ServiceExcept
  {
	  return new CHH<Vector<UserBonusPeriodListItem>>()
	  {
		  public void call() throws Exception
		  {
			  ret = new Vector<UserBonusPeriodListItem>();
			  while (haveres)
			  {
				  ret.add(new UserBonusPeriodListItem(rs.getString(1), rs.getString(2)));
				  haveres = rs.next();
			  }
		  }
	  }.RunP(SQL.getUserBonusPeriodList, "getUserBonusPeriodList",false,usrid);
  }/**/
  // --------------------------------------------------------------------------------
  public static UserPeriodBonusInfo getUserPeriodBonusCounters(Long usrid, Long BID) throws ServiceExcept {
    UserPeriodBonusInfo bs = new CHH<UserPeriodBonusInfo>()
    {
    	public void call() throws Exception
    	{
            if (haveres) {
                ret = new UserPeriodBonusInfo(null, rs.getString(1), rs.getString(2), rs.getString(3), rs.getLong(4));
              };    		
    	}
    }.RunP(SQL.getUserPeriodBonusCounters, "getUserPeriodBonusCounters",false,usrid,BID);
    if (bs == null)
      throw new ServiceExcept("Internal error: getUserPeriodBonusCounters error getting state");
    return bs;
  }
  // --------------------------------------------------------------------------------
  public static UserPeriodBonusInfo getUserPeriodBonusInfo(Long usrid, Long BID) throws ServiceExcept {
    UserPeriodBonusInfo ret = getUserPeriodBonusCounters(usrid, BID);
    if (ret != null) {
      ret.data = getBonuses(usrid, BID);
    }
    return ret;
  }
  // --------------------------------------------------------------------------------
  public static Vector<ADM_TRACE_ITEM> getADMTrace() throws ServiceExcept
  {
    return new CHH<Vector<ADM_TRACE_ITEM>>()
    {
    	public void call() throws Exception
    	{
    		ret = new Vector<ADM_TRACE_ITEM>();
    		while (haveres)
    		{
    			ret.add(new ADM_TRACE_ITEM(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
    			haveres = rs.next();
    		}
    	}
    }.RunP(SQL.getADMTrace, "getADMTrace",false);
  }
  // --------------------------------------------------------------------------------
  public static Vector<wrkitem> getModelPlans(String Model) throws ServiceExcept 
  {
	    return new CHH<Vector<wrkitem>>()
	    {
	    	public void call() throws Exception
	    	{
	    		ret = new Vector<wrkitem>();
	    		while (haveres)
	    		{
	    			ret.add(new wrkitem(rs.getInt(1), rs.getString(2),null));
	    			haveres = rs.next();
	    		}
	    	}
	    }.RunP(SQL.getModelPlans, "getModelPlans",false,Model);
  }
  //--------------------------------------------------------------------------------
  public static Vector<search_userData> search_user(String account, String name,
		String devid) throws ServiceExcept
	{
		  Vector<search_userData> m = new Vector<search_userData>();
		   String addcond="";
		   int a=0,b=0,c=0;
		   if ((account!=null)&(!account.equals(""))){addcond=" and account=? ";a=1;};
		   if ((name!=null)&(!name.equals(""))){addcond+=" and name like ? ";b=1;};
		   if ((devid!=null)&(!devid.equals(""))){addcond+=" and dev_id = ? ";c=1;};
		    PreparedStatement stmt = null;
		    Connection conn = null;
		    try {
		    	addcond=SQL.find_user+addcond;
		   // 	Log(addcond);
		      conn = CP_DB.getConnection("ENGINE_DB_CONN_PULL");
		      if (conn != null) {
		        stmt = conn.prepareStatement(addcond);
		        if (stmt != null) {
		        	
		        	int cc=1;
		        	if (a==1){stmt.setString(cc++, account);};
		        	if (b==1){stmt.setString(cc++, "%"+name+"%");};
		        	if (c==1){stmt.setString(cc++, devid);};		        	
		        	ResultSet rs = stmt.executeQuery();
		        	boolean haveres = rs.next();
		        	while (haveres) {
		        		m.add(new search_userData(rs.getLong(1),rs.getString(2),rs.getString(3),rs.getLong(4)));
		        		haveres = rs.next();
		        	}
		        }
		        else {
		          Log("search_user err prepare");
		        }
		      }
		    } catch (Exception ex) {
		      Log("search_user " + ex.getMessage());
		      throw new ServiceExcept("Internal error: search_user error");
		    } finally {
		      try {
		        stmt.close();
		      } catch (Exception e1) {
		        Log("search_user2 " + e1.getMessage());
		      }
		    }
		    CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
		    return m;
	}
    //--------------------------------------------------------------------------------
  	public static Vector<rpc_cmn_sprvk> getcmn_sprvk(Long usrId, Long bid)throws ServiceExcept
  	{
	    return new CHH<Vector<rpc_cmn_sprvk>>()
	    {
	    	public void call() throws Exception
	    	{
	    		ret = new Vector<rpc_cmn_sprvk>();
	    		while (haveres)
	    		{
	    			ret.add(new rpc_cmn_sprvk(rs.getString(1),
	    					                  rs.getString(2),
	    					                  rs.getString(3),
	    									  rs.getString(4),
	    									  rs.getInt(5)));
	    			haveres = rs.next();
	    		}
	    	}
	    }.RunP(SQL.getcmn_sprvk, "getcmn_sprvk",false,usrId,bid);
  	}
    //--------------------------------------------------------------------------------  	
	public static Vector<RPC_BillPeriod> getBillPeriods(Long usrId)throws ServiceExcept
	{
	    return new CHH<Vector<RPC_BillPeriod>>()
	    {
	    	public void call() throws Exception
	    	{
	    		ret = new Vector<RPC_BillPeriod>();
	    		while (haveres)
	    		{
	    			ret.add(new RPC_BillPeriod(rs.getLong(1), rs.getString(2),rs.getInt(3)));
	    			haveres = rs.next();
	    		}
	    	}
	    }.RunP(SQL.getBillPeriods, "getBillPeriods",false,usrId);
	}
    //--------------------------------------------------------------------------------  	
	public static period_calls_lg_data get_period_calls_lg_data(Long usrId,Long bid)throws ServiceExcept
	{
		period_calls_lg_data ret=null;
		Connection con=null;
		Exception e=null;
   	    try
   	    {
   	    	con = CP_DB.getConnection("ENGINE_DB_CONN_PULL");
   	    
   	    	ret = new period_calls_lg_data(
   	    	new CHH<Vector<period_call_l>>()
   		    {
   		    	public void call() throws Exception
   		    	{
   		    		ret = new Vector<period_call_l>();
   		    		while (haveres)
   		    		{
   		    			ret.add(new period_call_l(rs.getLong(1), 
   		    									  rs.getString(2),
   		    									  rs.getLong(3), 
   		    									  rs.getString(4),
   		    									  rs.getDouble(5)));
   		    			haveres = rs.next();
   		    		}
   		    	}
   		    }.RunP(con,SQL.get_pcalls_lg_loc, "get_period_calls_lg_data_local",false,usrId,bid),
   	    	new CHH<Vector<period_call_g>>()
   		    {
   		    	public void call() throws Exception
   		    	{
   		    		ret = new Vector<period_call_g>();
   		    		while (haveres)
   		    		{
   		    			ret.add(new period_call_g(rs.getLong(1),
   		    									  rs.getString(2),
   		    									  rs.getString(3),
   		    									  rs.getString(4),
   		    									  rs.getString(5),
   		    									  rs.getString(6),
   		    									  rs.getDouble(7)));
   		    			haveres = rs.next();
   		    		}
   		    	}
   		    }.RunP(con,SQL.get_pcalls_lg_glob, "get_period_calls_lg_data_glob",false,usrId,bid));   	    	
   	    	
   	    }catch(Exception f){e=f;}finally
   	    {
   	    	CP_DB.freeConnection("ENGINE_DB_CONN_PULL", con);	
   	    }
 	    if (ret==null) throw new ServiceExcept("get_period_calls_lg_dataerror ",e);
		return ret;
	}
    //--------------------------------------------------------------------------------  	
	public static Vector<rpc_raz_post_usl> get_raz_post_usl(Long usrId, Long bid)throws ServiceExcept
	{
	    return new CHH<Vector<rpc_raz_post_usl>>()
	    {
	    	public void call() throws Exception
	    	{
	    		ret = new Vector<rpc_raz_post_usl>();
	    		while (haveres)
	    		{
	    			ret.add(new rpc_raz_post_usl(rs.getString(1),
	    					                  rs.getString(2),
	    					                  rs.getDouble(3),
	    					                  rs.getInt(4)));
	    			haveres = rs.next();
	    		}
	    	}
	    }.RunP(SQL.get_raz_post_usl, "get_raz_post_usl",false,usrId,bid);
	}
    //-------------------------------------------------------------------------------- 
	public static rpc_tp_calc get_tp_calc()throws ServiceExcept
	{
		rpc_tp_calc ret = new rpc_tp_calc();
		
		ret.fphone=new CHH<Vector<rpc_tp_calc_phone>>()
	    {
	    	public void call() throws Exception
	    	{
	    		ret = new Vector<rpc_tp_calc_phone>();
	    		while (haveres)
	    		{
	    			ret.add(new rpc_tp_calc_phone(rs.getString(1),
	    					                  	  rs.getString(2),
	    					                      new Float(rs.getDouble(3)),
	    					                      new Float(rs.getDouble(4)),rs.getInt(5)));
	    			haveres = rs.next();
	    		}
	    	}
	    }.RunP(SQL.get_tp_calc_phone, "get_tp_calc_phone",true);

		ret.finet=new CHH<Vector<rpc_tp_calc_inet>>()
	    {
	    	public void call() throws Exception
	    	{
	    		ret = new Vector<rpc_tp_calc_inet>();
	    		while (haveres)
	    		{
	    			ret.add(new rpc_tp_calc_inet(rs.getString(1),
	    					                  	  rs.getString(2),
	    					                      new Float(rs.getDouble(3)),
	    					                      new Float(rs.getDouble(4)),
	    					                      rs.getInt(5)
	    			));
	    			haveres = rs.next();
	    		}
	    	}
	    }.RunP(SQL.get_tp_calc_inet, "get_tp_calc_inet",true);	    

		ret.fiptv=new CHH<Vector<rpc_tp_calc_iptv>>()
	    {
	    	public void call() throws Exception
	    	{
	    		ret = new Vector<rpc_tp_calc_iptv>();
	    		while (haveres)
	    		{
	    			ret.add(new rpc_tp_calc_iptv( rs.getString(1),
	    					                  	  rs.getString(2),
	    					                      new Float(rs.getDouble(3)),
	    					                      rs.getString(4)
	    			));
	    			haveres = rs.next();
	    		}
	    	}
	    }.RunP(SQL.get_tp_calc_iptv, "get_tp_calc_iptv",true);	    

		ret.fiptv_pack=new CHH<Vector<rpc_tp_calc_iptv_pack>>()
	    {
	    	public void call() throws Exception
	    	{
	    		ret = new Vector<rpc_tp_calc_iptv_pack>();
	    		while (haveres)
	    		{
	    			ret.add(new rpc_tp_calc_iptv_pack( rs.getInt(1),
	    					                  	       rs.getString(2),
	    					                  	       new Float(rs.getDouble(3)),
	    					                  	       rs.getString(4)
	    			));
	    			haveres = rs.next();
	    		}
	    	}
	    }.RunP(SQL.get_tp_calc_iptv_pack, "get_tp_calc_iptv_pack",true);	    
	    /**/
	    return ret;
	}
    //--------------------------------------------------------------------------------  	
}
