package gwtb.server.rpc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.TreeMap;

import gwtb.client.rpcdata.ServiceExcept;
import gwtb.client.rpcdata.abon_corp_profile;
import gwtb.core.PULLS.CP_DB;
import gwtb.client.rpcdata.abon_profile;
import gwtb.server.selects;
import gwtb.server.tools;

public class abon_profileS
{
	
	
	static abon_profile getProfile(Long usr_id,Connection conn,TreeMap<String, String> m) throws ServiceExcept
	{
		PreparedStatement stmt = null;
		abon_profile rr=null;
		if (conn!=null)
		{
			try
			{
				if ((stmt=conn.prepareStatement(
              		  "select USER_ID, AB_ID, NAME, ADDR, EMAIL, " +
              		         "SALDO, SMSPHONE, SMSON, EMAILON, account," +
              		         "phonevoice,allowphone,allowafert ,today,tomorrow," +
              		         "nextmonth, einf_bo, einf_schet,  iscorp,  bday " +
              		         " from users_data where USER_ID=?"
                ))!=null)
                {
                    stmt.setLong(1,usr_id);
                    ResultSet rs =stmt.executeQuery();
                    if (rs.next())
                    {
                        rr= new abon_profile(  rs.getLong(1),   rs.getLong(2),     rs.getString(3),  rs.getString(4),  rs.getString(5),
          		  			   				   rs.getFloat(6),    rs.getString(7),  rs.getInt(8),     rs.getInt(9),     rs.getString(10),
          		  			   				   rs.getString(11),  rs.getInt(12),    rs.getInt(13),    rs.getString(14), rs.getString(15),
          		  			   				   rs.getString(16),  rs.getInt(17),    rs.getInt(18),    rs.getString(19), rs.getDate(20));
                    };
                    try
                    {
                    	if (stmt!=null)
                    		stmt.close();
                    }
                    catch(Exception e1)
                    {
                    	System.err.println("getProfile::record get(int id):close");
                    	throw new ServiceExcept("Internal error: getProfile Login DB conn close error "+e1.getLocalizedMessage());
                    };                     
                 };
            }catch (Exception ex)
            {
            	System.err.println("getProfile Login"+ex.getLocalizedMessage());
            	throw new ServiceExcept("Internal error: getProfile  DB conn error "+ex.getLocalizedMessage());
            };
        };
		if (rr!=null)
		{
			rr.corpData=getCORPProfile(usr_id,conn);
			feelDEVS(rr);
			return rr;
		}else
		{
			throw new ServiceExcept(m.get("auth_wr_data"));
		}        
	};
	//--------------------------------------------------------------------------------	
	static abon_corp_profile getCORPProfile(Long usr_id,Connection conn) throws ServiceExcept
	{
		PreparedStatement stmt = null;
		abon_corp_profile rr=null;
		if (conn!=null)
		{
			try
			{
				if ((stmt=conn.prepareStatement("select FIO, FIO2, FIO3, DOLJ, DOLJ2, DOLJ3, EMAIL, EMAIL2, EMAIL3, PHN, PHN2, PHN3, FAX, FAX2, FAX3, MOB, MOB2,MOB3, BD from t_corp_contact where USER_ID=?"))!=null)
                {
                    stmt.setLong(1,usr_id);
                    ResultSet rs =stmt.executeQuery();
                    if (rs.next())
                    {
                        rr= new abon_corp_profile(rs.getString(1),   rs.getString(2),     rs.getString(3),
                        						  rs.getString(4),   rs.getString(5),     rs.getString(6),
                        						  rs.getString(7),   rs.getString(8),     rs.getString(9),
                        						  rs.getString(10),  rs.getString(11),    rs.getString(12),
                        						  rs.getString(13),  rs.getString(14),    rs.getString(15),
                        						  rs.getString(16),  rs.getString(17),    rs.getString(18),rs.getDate(19));
                    };
                    try
                    {
                    	if (stmt!=null)
                    		stmt.close();
                    }
                    catch(Exception e1)
                    {
                    	System.err.println("getCORPProfile::record get(int id):close");
                    	throw new ServiceExcept("Internal error: getCORPProfile Login DB conn close error "+e1.getLocalizedMessage());
                    };                     
                 };
            }catch (Exception ex)
            {
            	System.err.println("getProfile Login"+ex.getLocalizedMessage());
            	throw new ServiceExcept("Internal error: getCORPProfile  DB conn error "+ex.getLocalizedMessage());
            };
        };
		return rr;
	};
	//--------------------------------------------------------------------------------	
	static public abon_profile Login(String lgn,String pwd,TreeMap<String, String> m)throws ServiceExcept
    {
		Connection conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL");
		long usrid=tools.try_auth(lgn, pwd, conn);
		if (usrid>0)tools.registerAuth(usrid,lgn);
			
		abon_profile rr =getProfile(usrid,conn,m);
		
		CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
		return rr;
	};
	//--------------------------------------------------------------------------------	
	static public void feelDEVS(abon_profile d) throws ServiceExcept
	{
	 	d.connections=selects.getUserConnection(d.usr_id_sip);
	  	d.phones=selects.getUserPhones(d.usr_id);
	   	tools.feelURLS(d);
	}
	//--------------------------------------------------------------------------------	
	public static abon_profile SelectUser(Long uid,TreeMap<String, String> m)throws ServiceExcept
	{
		Connection conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL");
		tools.chk_data(uid.toString());
		abon_profile rr = getProfile(uid,conn,m);
		
		CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
		return rr;
	}
}
