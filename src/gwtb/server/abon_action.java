package gwtb.server;

import gwtb.client.rpcdata.BronSetUnSetResult;
import gwtb.client.rpcdata.DVOclipart;
import gwtb.client.rpcdata.ServiceExcept;
import gwtb.client.rpcdata.abon_corp_profile;
import gwtb.client.rpcdata.update_contacts;
import gwtb.core.PULLS.CP_DB;

import java.sql.Connection;
import java.sql.Date;
import java.util.Vector;

public class abon_action{
	Connection conn;
	public int isOper;
	//--------------------------------------------------------------------------------	
	public void killConn()
	{
		if (isOper==0)
		{
			CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
		}else
		{
			CP_DB.freePullRAWConnection(conn);
		}
	}
	//--------------------------------------------------------------------------------	
	public abon_action()
	{
		isOper=0;
	}
	//--------------------------------------------------------------------------------	
	public void Log(String str){
		selects.Log(str);

	}
	//--------------------------------------------------------------------------------
	public String ChangePhoneTP(Long usrId, String devid, int tp) throws ServiceExcept
	{
	    String bsr=new CHH<String>(){public void call() throws Exception{
            ret=st.getString(1);
	    }}.RunC(conn, SQL.ChangePhoneTP, "ChangePhoneTP",true,new OutPrm(1),usrId,devid,tp);		    
	    
	    if (bsr==null){throw new ServiceExcept("Internal error: ChangePhoneTP error");};
	    return bsr;	
	}
//--------------------------------------------------------------------------------
	public BronSetUnSetResult BronSet(Long usr_id, int mon) throws ServiceExcept
	{
	    return new CHH<BronSetUnSetResult>(){public void call() throws Exception{
            int r1=st.getInt(1);
            int r2=st.getInt(4);
            if (r1>0){conn.commit();}else{conn.rollback();};
            ret = new BronSetUnSetResult(r1,r2);
	    }}.RunC(conn, SQL.BronSet, "BronSet",true,new OutPrm(2),usr_id,mon,new OutPrm(2));		    
	}
	//--------------------------------------------------------------------------------
	public BronSetUnSetResult BronUnSet(Long usr_id) throws ServiceExcept
	{
	    BronSetUnSetResult bsr=new CHH<BronSetUnSetResult>(){public void call() throws Exception{
            int r1=st.getInt(1);
            int r2=st.getInt(3);
            if (r1>0){conn.commit();}else{conn.rollback();};
            ret = new BronSetUnSetResult(r1,r2);
	    }}.RunC(conn, SQL.BronUnSet, "BronUnSet",true,new OutPrm(2),usr_id,new OutPrm(2));	
	    if (bsr==null){throw new ServiceExcept("Internal error: BronUnSet error");};
	    return bsr;
	}
	//--------------------------------------------------------------------------------
	public String ChangeRadiusPwd(Long usrid, String login, String opwd, String npwd, String npwd2) throws ServiceExcept
	{
		String bsr=null;
		if (npwd.equalsIgnoreCase(npwd2))
		{
			bsr=CH.STRING(conn, SQL.ChangeRadiusPwd, "ChangeRadiusPwd",true,new OutPrm(1),usrid,login,opwd,npwd);
		}else{throw new ServiceExcept("New password is wrong");};
	    if (bsr==null){throw new ServiceExcept("Internal error: ChangeRadiusPwd error");};
	    return bsr;
	}
	//--------------------------------------------------------------------------------
	public String ChangeInetTP   (Long usrId, String conname, int tp) throws ServiceExcept
	{
		String bsr=CH.STRING(conn, SQL.ChangeInetTP, "ChangeInetTP",true,new OutPrm(1),usrId,conname,tp);	    
	    if (bsr==null){throw new ServiceExcept("Internal error: ChangeInetTP error 3");};
	    return bsr;	
	}
	//--------------------------------------------------------------------------------
	public Vector<DVOclipart> DVOChange(Long usr_id, String devid, String DVOList) throws ServiceExcept
	{
		CH.VOID(conn, SQL.DVOChange, "DVOChange",true,usr_id,devid,DVOList);	    
		return selects.getuserDVOList(usr_id, devid);
	}
	//--------------------------------------------------------------------------------
	public String InetPB(Long usrId, int summ, int days)throws ServiceExcept
	{
		    return CH.STRING(conn, SQL.InetPB, "InetPB",true,new OutPrm(1),usrId,summ,days);
	}
	//--------------------------------------------------------------------------------	
	public update_contacts Update_contacts(Long usrId, String email,String Phone,String Phonevoice,
			Integer e_on,Integer s_on,Integer p_on,Integer af_on,Date bdate)throws ServiceExcept
	{
		CH.VOID(conn, SQL.Update_contacts, "Update_contacts",true,usrId,email,Phone,Phonevoice,e_on,s_on,p_on,af_on,(bdate==null)?new inNull(3):bdate);
		return new update_contacts(email,Phone,Phonevoice,s_on,e_on,p_on,af_on,bdate);
	}
	//--------------------------------------------------------------------------------	
	public void   SendQuestion(String theme, String body,Long usrid,int svc,int typ)throws ServiceExcept
	{
		CH.VOID(conn, SQL.SendQuestion, "SendQuestion",true,theme,body,usrid,svc,typ);
	};
	//--------------------------------------------------------------------------------	
	public String Change09State(Long usrId, String devid, boolean state) throws ServiceExcept
	{
	        return  CH.STRING(conn, SQL.Change09State, "Change09State",false,new OutPrm(1),usrId,devid,state?1:0);
	}
	//--------------------------------------------------------------------------------	
	public String UseBonuses(Long usrId,int count) throws ServiceExcept
	{
	    return  CH.STRING(conn, SQL.UseBonuses, "UseBonuses",false,new OutPrm(1),usrId,count);		
	}
	//--------------------------------------------------------------------------------	
	public void   update_einf(Long usrId, int bo, int schet) throws ServiceExcept
	{
		CH.VOID(conn, SQL.update_einf, "update_einf",false,usrId,bo,schet);			
	}
	//--------------------------------------------------------------------------------
	public abon_corp_profile Update_CORPcontacts(Long usrId, String f1,
			String f2, String f3, String d1, String d2, String d3, String e1,
			String e2, String e3, String p1, String p2, String p3, String x1,
			String x2, String x3, String m1, String m2, String m3, Date date)throws ServiceExcept
	{
		CH.VOID(conn, SQL.Update_CORPcontacts, "Update_CORPcontacts",true,usrId,f1,f2,f3,d1,d2,d3,e1,e2,e3,p1,p2,p3,x1,x2,x3,m1,m2,m3,(date==null)?new inNull(3):date);
		return new abon_corp_profile(f1,f2,f3,d1,d2,d3,e1,e2,e3,p1,p2,p3,x1,x2,x3,m1,m2,m3,date);
	}
	//--------------------------------------------------------------------------------
	public void   update_CORP_einf(Long usrId, String email, int schet)throws ServiceExcept
	{
		CH.VOID(conn, SQL.update_CORPeinf, "update_CORP_einf",false,usrId,schet,email);
	}
	//--------------------------------------------------------------------------------	
	public void   corpreport(Long usrId,String period,int a1,int a2,int s1,int s2) throws ServiceExcept
	{
		CH.VOID(conn, SQL.corpreport, "corpreport",false,usrId,period,a1,a2,s1,s2);		
	}
	//--------------------------------------------------------------------------------	
	public void   SendCorpQuestion(String theme, String body, Long usrId,int typ) throws ServiceExcept
	{
		CH.VOID(conn, SQL.SendQuestionCorp, "SendQuestionCorp",true,theme,body,usrId,typ);	
	}
	//--------------------------------------------------------------------------------	
	public String LK_PWD_CHANGE(Long usrId, String login, String opwd,String npwd, String npwd2) throws ServiceExcept
	{
		if (npwd.equalsIgnoreCase(npwd2))
			return CH.STRING(conn, SQL.LK_PWD_CHANGE, "LK_PWD_CHANGE",true,new OutPrm(1),usrId,login,opwd,npwd);
		throw new ServiceExcept("New password is wrong");
	}
	//--------------------------------------------------------------------------------	
	public String block_mgmn_set(Long usrId, Long devId, int len)throws ServiceExcept
	{
		return CH.STRING(conn, SQL.block_mgmn_set, "block_mgmn_set",true,new OutPrm(1),usrId,devId,len);
	}
	//--------------------------------------------------------------------------------	
	public String block_mgmn_reset(Long usrId, Long devId)throws ServiceExcept
	{
		return CH.STRING(conn, SQL.block_mgmn_reset, "block_mgmn_reset",true,new OutPrm(1),usrId,devId);
	}
	//--------------------------------------------------------------------------------	
	public String block_apus_set(Long usrId, Long devId, int len)throws ServiceExcept
	{
		return CH.STRING(conn, SQL.block_apus_set, "block_apus_set",true,new OutPrm(1),usrId,devId,len);
	}
	//--------------------------------------------------------------------------------	
	public String block_apus_reset(Long usrId, Long devId)throws ServiceExcept
	{
		return CH.STRING(conn, SQL.block_apus_reset, "block_apus_reset",true,new OutPrm(1),usrId,devId);
	}
	//--------------------------------------------------------------------------------	
}


