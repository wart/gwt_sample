package gwtb.server;

import gwtb.client.rpcdata.ServiceExcept;

import java.sql.Connection;

public class CH
{
    // --------------------------------------------------------------------------------
	public static void VOID(Connection a,String sql,String name,boolean exp1,Object ... args) throws ServiceExcept
	{
		new CHH<Void>()
		{
			
			@Override
			public void call() throws Exception {
				// TODO Auto-generated method stub
				
			}
		}.RunCA(a, sql, name, exp1, args);
	}
    // --------------------------------------------------------------------------------
	public static String STRING(Connection a,String sql,String name,boolean exp1,Object ... args) throws ServiceExcept
	{
		return new CHH<String>()
		{
			@Override
			public void call() throws Exception {
				ret=st.getString(1);
			};
		}.RunCA(a, sql, name, exp1, args);
	}
    // --------------------------------------------------------------------------------
	public static Long LONG(Connection a,String sql,String name,boolean exp1,Object ... args) throws ServiceExcept
	{
		return new CHH<Long>()
		{
			@Override
			public void call() throws Exception {
				ret=st.getLong(1);
			};
		}.RunCA(a, sql, name, exp1, args);
	}
    // --------------------------------------------------------------------------------
	public static Integer INT(Connection a,String sql,String name,boolean exp1,Object ... args) throws ServiceExcept
	{
		return new CHH<Integer>()
		{
			@Override
			public void call() throws Exception {
				ret=st.getInt(1);
			};
		}.RunCA(a, sql, name, exp1, args);
	}	
    // --------------------------------------------------------------------------------
	public static Float FLOAT(Connection a,String sql,String name,boolean exp1,Object ... args) throws ServiceExcept
	{
		return new CHH<Float>()
		{
			@Override
			public void call() throws Exception {
				ret=st.getFloat(1);
			};
		}.RunCA(a, sql, name, exp1, args);
	}
    // --------------------------------------------------------------------------------
	public static Boolean BOOL(Connection a,String sql,String name,boolean exp1,Object ... args) throws ServiceExcept
	{
		return new CHH<Boolean>()
		{
			@Override
			public void call() throws Exception {
				ret=st.getBoolean(1);
			};
		}.RunCA(a, sql, name, exp1, args);
	}
    // --------------------------------------------------------------------------------
}
