package gwtb.server;


public class SQL
{
	//----------------------- ABON_ACTION
	final public static String ChangePhoneTP       = "{call ?:=kaspersky_service.spus_tp.lc_spus_zayavk(?,?,?)}";
	final public static String BronSet             = "{call ?:=kaspersky_service.inet_bron.zayavka_broneb(?,?,?)}";
	final public static String BronUnSet           = "{call ?:=kaspersky_service.inet_bron.zayavka_bron_offb(?,?)}";


	final public static String ChangeInetTP        = "{call ?:=kaspersky_service.inet_tp.create_zayavk_lc(?,?,?)}";

	final public static String DVOChange           = "{call kaspersky_service.lc_dvo_svc.na_setchanges(?,?,?)}";
	final public static String InetPB              = "{call ?:=kaspersky_service.Oren_RegPayBalB(?,?,?)}";
	final public static String Update_contacts     = "{call kaspersky_service.abon_tool.update_contacts(?+0,?,?,?,?,?,?,?,?)}";
	final public static String Update_CORPcontacts = "{call kaspersky_service.abon_tool.update_corp_contacts(?+0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	final public static String ChangeRadiusPwd     = "{call ?:=kaspersky_service.pwd_tool.RADIUS_PWD_CHANGE(?,?,?,?)}";	
	final public static String LK_PWD_CHANGE       = "{call ?:=kaspersky_service.pwd_tool.LK_PWD_CHANGE(?,?,?,?)}";
	public static final String Recovery_LKPass 	   = "{call ?:=kaspersky_service.pwd_tool.Recovery_LKPass(?,?)}";
	
	final public static String SendQuestion        = "{call kaspersky_service.mail.private_office_question(?,?,?,?,?)}";
	final public static String SendQuestionCorp    = "{call kaspersky_service.mail.private_office_CorpQuestion(?,?,?,?)}";
	final public static String Change09State       = "{call ?:=kaspersky_service.in09_action.change09state(?,?,?)}";
	final public static String UseBonuses          = "{call ?:=kaspersky_service.abon_tool.use_bonuses(?,?)}";
	final public static String update_einf 		   = "{call kaspersky_service.abon_tool.update_einf(?,?,?)}";
	final public static String update_CORPeinf	   = "{call kaspersky_service.abon_tool.update_corp_einf(?,?,?)}";
	
	final public static String corpreport          = "{call kaspersky_service.abon_tool.corpreport(?,?,?,?,?,?)}";
	public static final String block_apus_reset    = "{call ?:=kaspersky_service.block_AM.block_apus_reset(?,?)}";
	public static final String block_apus_set      = "{call ?:=kaspersky_service.block_AM.block_apus_set(?,?,?)}";
	public static final String block_mgmn_reset    = "{call ?:=kaspersky_service.block_AM.block_mgmn_reset(?,?)}";
	public static final String block_mgmn_set      = "{call ?:=kaspersky_service.block_AM.block_mgmn_set(?,?,?)}";
	
	
	
	
	/*
	DVO
	select * from t_services where dev_id=3537620757
select * from t_users where user_id in (
27603618,5635091668)

--select * from t_volume where dev_id=3532560588
select * 
  from tu_ats_dvo d,
  t_svc_ref sr
 where ats_id=14
   and d.svc_id=sr.svc_id
   
 select * from n_const_svc
	
	/**/
	
	//----------------------- SELECTS
	final public static String getMsgList        = "select key,val from kaspersky_service.site_texts where grp=?";
	final public static String getINETERRS       = "select id,descr from kaspersky_service.t_inet_error";
	final public static String getWRKList        = "select id,name,data1 from kaspersky_service.lc_wrk_lists where list=? order by id";
	final public static String getModelPlans     = "select id,name from kaspersky_service.avail_inet_tp where model=? order by id";
	final public static String getUserConnection = "select connection,tp,model from kaspersky_service.user_connections where ab_id=?";
	final public static String getUserPhones     = "select phone,tp,speed,inbron,addr,in09,costed,user_id,allowDVO,bloked_apus,bloked_mgmn from kaspersky_service.user_phones  where user_id=?";
	final public static String getDVOList        = "select ID,NAME,RSG,RSS,CSG,CSS,CSGN,CSSN from kaspersky_service.v_lc_dvo_svc";
	final public static String getuserDVOList    = "select id,name,ison,srvid,ch_state,ch_date from kaspersky_service.user_dvo t where user_id=(0+?) and dev_id=(0+?) and ison>0";
	final public static String getNewsTitles     = "select * from kaspersky_service.site_actual_news";
	final public static String getNewsBody       = "select body from kaspersky_service.site_news where id=?";
	final public static String getJournal        = "select to_char(cdate,'dd.mm.yyyy')dt,ztype,dev,ztextin,ztextout from kaspersky_service.site_jrnl t where user_id=(0+?) order by to_char(cdate,'yyyymmddhh24miss') desc";
	final public static String getBonuses        = "select bonus_id,name,bonus,-bonus_used  bonus_used from kaspersky_service.avail_bonuses t where user_id=? and billing_id=?";
	final public static String getUserPeriodBonusCounters = "select prev,used,total,billing_id from kaspersky_service.user_bonus_counter where user_id=? and billing_id=?";
	final public static String getADMTrace 		 = "select * from (select src,mess,note,mess_date from o1.adm_trace where src is not null or mess is not null or note is not null order by adm_trace_id desc) where rownum<100";
	final public static String find_user         = "select user_id, account, name, dev_id from kaspersky_service.search_user where rownum<50 ";

	
	
	final public static String getcmn_sprvk 	  = "select f1, f2, f3, f4, frm  from user_spravk where user_id=? and bid=?";
	final public static String get_raz_post_usl	  = "select dev, svc, summ, frm  from user_raz_const where user_id=? and bid=?";
	final public static String get_pcalls_lg_loc  = "select dev, tp, cnt, len, summ from user_apus where user_id=? and bid=?";
	final public static String get_pcalls_lg_glob = "select dev, cod, mgmn, dest, datatime, len, summ  from user_mgmn where user_id=? and bid=?";
	
	final public static String get_tp_calc_phone    =	"select categor,name,cost,un_cost,typ from tp_calc_phone";
	final public static String get_tp_calc_inet		=	"select tech, name, cost, un_cost,speed from tp_calc_inet";
	final public static String get_tp_calc_iptv		=	"select tech, name, cost, descr from tp_calc_iptv";
	final public static String get_tp_calc_iptv_pack=	"select id, name, cost, descr from tp_calc_iptv_pack";

	
	public static final String getBillPeriods 	 = "select id,name,bonus from user_periods t where user_id=?";
	//----------------------- TOOLS
	final public static String isbillPeriod      = "{call ?:=O1.BILLPER.ISBILLPERIODOPEN(?)}";
	final public static String try_auth          = "{call ?:=kaspersky_service.abon_tool.get_user_id_web(?,?)}";	
	final public static String feelURLS          = "{call    kaspersky_service.abon_tool.generate_enter_url(?,?,?,?)}";
	final public static String getBalIP          = "{call ?:=kaspersky_service.oren_chk_bal_ip(?)}";
	final public static String CheckADSLTech     = "{call ?:=kaspersky_service.adsl_yes_no_S(?)}";
	final public static String chk_data          = "{call    kaspersky_service.abon_tool.chk_data(?)}";	
	final public static String GetBronState      = "{call ?:=kaspersky_service.inet_bron.bronstate(?)}";
	final public static String registerAuth 	 = "{call    kaspersky_service.abon_tool.registerAuth(?,?)}";

	
		


	

}
