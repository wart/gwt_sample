package gwtb.server;

import gwtb.client.rpcdata.ServiceExcept;
import gwtb.core.PULLS.CP_DB;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public abstract class CHH<T>
{
	public Connection conn;
	public CallableStatement st;
	public PreparedStatement ps;
	public ResultSet rs;
	protected T ret;
	boolean haveres;
	abstract public void call() throws Exception,ServiceExcept;
	
	//====================================================
	T RunC(Connection a,String sql,String name,boolean exp1,Object ... args) throws ServiceExcept
	{
		return RunCA(a,sql,name,exp1,args);
	}
	//====================================================
 	T RunCA(Connection a,String sql,String name,boolean exp1,Object[] args) throws ServiceExcept
	{
		conn=a;
	    try 
	    {
               st= conn.prepareCall(sql);
               if(args!=null)
               if (args.length>0)
               {
            	 for (int i=0; i<args.length;++i)
            	 if (args[i]!=null)
            	 {
            		String cn= args[i].getClass().getSimpleName();
            		if (cn.equalsIgnoreCase("String" )){st.setString(i+1, (String) args[i]);};
            		if (cn.equalsIgnoreCase("Integer")){st.setInt   (i+1, (Integer)args[i]);};
            		if (cn.equalsIgnoreCase("Double" )){st.setDouble(i+1, (Double) args[i]);};
            		if (cn.equalsIgnoreCase("Long"   )){st.setLong  (i+1, (Long)   args[i]);};
            		if (cn.equalsIgnoreCase("Float"  )){st.setFloat (i+1, (Float)  args[i]);};
            		if (cn.equalsIgnoreCase("Date"   )){st.setDate  (i+1, (Date)   args[i]);};
            		if (cn.equalsIgnoreCase("inNull" )){((inNull)args[i]).init(st, i+1);};
            		if (cn.equalsIgnoreCase("OutPrm" )){((OutPrm)args[i]).init(st, i+1);};
            		
            	 }else{st.setString(i+1, null);};
               };
               st.executeQuery();
               call();
               conn.commit();
	    }catch (Exception ex) {throw new ServiceExcept("Internal error: "+name+" error ",ex);
	    }finally 
	    {
	            try
	            { if(st!=null)
	                  st.close();
	            }catch(Exception e1)
	            {
	            	if (exp1)
	            	{
	            		throw new ServiceExcept("Internal error: "+name+" error",e1);
	            	}else{
	            		selects.Log(name+" "+e1.getMessage());
	            	};
	            }
	    }
	    return ret;
	}	
	//====================================================
 	T RunP(String sql,String name,boolean exp1,Object ... args) throws ServiceExcept
 	{
 		return RunPA(sql,name,exp1,args);
 	}
	//====================================================
 	T RunP(Connection a,String sql,String name,boolean exp1,Object ... args) throws ServiceExcept
 	{
 		return RunPA(a,sql,name,exp1,args);
 	}
	//====================================================
 	
 	T RunPA(String sql,String name,boolean exp1,Object[] args) throws ServiceExcept
 	{
   	    Connection con = CP_DB.getConnection("ENGINE_DB_CONN_PULL");
   	    
   	    RunPA(con,sql,name,exp1,args);
   	    
 	    CP_DB.freeConnection("ENGINE_DB_CONN_PULL", con);	    
 		return ret;
 	}
	//====================================================
 	T RunPA(Connection a,String sql,String name,boolean exp1,Object[] args) throws ServiceExcept
 	{
 		if (a != null)
 		{
 			try
 			{
 				ps = a.prepareStatement(sql);
 				if (ps != null)
 				{
 					if (args.length>0)
 					{
 						for (int i=0; i<args.length;++i)
 						{
 							String cn= args[i].getClass().getSimpleName();
 							if (cn.equalsIgnoreCase("String" )){ps.setString(i+1, (String) args[i]);};
 							if (cn.equalsIgnoreCase("Integer")){ps.setInt   (i+1, (Integer)args[i]);};
 							if (cn.equalsIgnoreCase("Double" )){ps.setDouble(i+1, (Double) args[i]);};
 							if (cn.equalsIgnoreCase("Long"   )){ps.setLong  (i+1, (Long)   args[i]);};
 							if (cn.equalsIgnoreCase("Float"  )){ps.setFloat (i+1, (Float)  args[i]);};
 							if (cn.equalsIgnoreCase("Date"   )){ps.setDate  (i+1, (Date)   args[i]);};
 						};
 					};
 	        	    rs = ps.executeQuery();
 	        	    haveres = rs.next();
 	        	    
 	        	    
 	        	    call();
 				}
 				else 
 				{
 					throw new ServiceExcept("Internal error: "+name+" error1");
 				};
 			}
 			catch (Exception ex)
 			{
 				throw new ServiceExcept("Internal error: "+name+" error2",ex);
 			}finally
 			{
 				try
 				{
 					ps.close();
 				}catch (Exception e1)
 				{
 					if (exp1)
 					{
 						throw new ServiceExcept("Internal error: "+name+" error3",e1);
 					}else
 					{
 						selects.Log("Internal error: "+name+" error4"+e1.getMessage());
 					};
 				}
 			}
 		}
		else 
		{
			throw new ServiceExcept("Internal error: "+name+" error5");
		};
 	    return ret;
 	}
	//====================================================

	//====================================================
}
