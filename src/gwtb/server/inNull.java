package gwtb.server;

import gwtb.client.rpcdata.ServiceExcept;

import java.sql.CallableStatement;
import java.sql.SQLException;

public class inNull {
	public int type;
	public static final int STR=1;
	public static final int NUM=2;
	public static final int DATE=3;
	public inNull(int i)
	{
		type=i;
	}
	public void init(CallableStatement st, int idx) throws SQLException, ServiceExcept
	{
		switch (type)
		{
			case 1:st.setNull(idx, java.sql.Types.VARCHAR);break;
			case 2:st.setNull(idx, java.sql.Types.NUMERIC);break;
			case 3:st.setNull(idx, java.sql.Types.DATE);break;
			default: throw new ServiceExcept("Wrong inNull type");
		}
	}
}
