package gwtb.server;

import gwtb.client.GreetingService;
import gwtb.client.rpcdata.ADM_TRACE_ITEM;
import gwtb.client.rpcdata.BronSetUnSetResult;
import gwtb.client.rpcdata.BronState;
import gwtb.client.rpcdata.DVOclipart;
import gwtb.client.rpcdata.Jrnl_rec;
import gwtb.client.rpcdata.News;
import gwtb.client.rpcdata.RPC_BillPeriod;
import gwtb.client.rpcdata.ServiceExcept;
import gwtb.client.rpcdata.UserPeriodBonusInfo;
import gwtb.client.rpcdata.abon_profile;
import gwtb.client.rpcdata.period_calls_lg_data;
import gwtb.client.rpcdata.rpc_cmn_sprvk;
import gwtb.client.rpcdata.rpc_raz_post_usl;
import gwtb.client.rpcdata.rpc_tp_calc;
import gwtb.client.rpcdata.search_userData;
import gwtb.client.rpcdata.abon_corp_profile;
import gwtb.client.rpcdata.update_contacts;
import gwtb.client.rpcdata.wrkitem;
import gwtb.core.PULLS.CP_DB;
import gwtb.server.rpc.abon_profileS;

import java.sql.Connection;
import java.util.Date;
import java.util.TreeMap;
import java.util.Vector;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


//=========================================================
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService
{
	@SuppressWarnings("unchecked")
	private ServiceExcept PutSTOut(String msg) 
	{
		String s;
		TreeMap<String, String> msgs=(TreeMap<String, String>)this.getServletContext().getAttribute("MsgList_1");
		if (msgs!=null){
			s=msgs.get("session_timeout");}
		else{
		    s="Session timeout";};
		return new ServiceExcept(msg+": "+s);	
	}
	@SuppressWarnings("unchecked")
	private ServiceExcept PutIntErr1()
	{
		String s;
		TreeMap<String, String> msgs=(TreeMap<String, String>)this.getServletContext().getAttribute("MsgList_1");
		if (msgs!=null){
			s=msgs.get("innternal_error1");}
		else{
		    s="Internal error";};
		    return new ServiceExcept(s);	
	}
	//=========================================================
 	public abon_profile auth(String Login,String pwd) throws ServiceExcept
	{
		TreeMap<String, String> msgs=(TreeMap<String, String>)getMsgList(2);
		if ((Login==null     )||(pwd==null     )) throw new ServiceExcept(msgs.get("auth_wr_empty"));
		if ((Login.equals(""))||(pwd.equals(""))) throw new ServiceExcept(msgs.get("auth_wr_empty"));
			
		abon_profile a= abon_profileS.Login(Login,pwd,msgs);
		if (a!=null)
		{
			this.getThreadLocalRequest().getSession().setAttribute("abon_profile", a);
			
			abon_action aa= new abon_action();
			aa.conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL");
			this.getThreadLocalRequest().getSession().setAttribute("abon_action", aa);
			return a;
		}
		throw new ServiceExcept(msgs.get("auth_wr_xz"));
	}
	//=====================================================
	@SuppressWarnings("unchecked")
	public TreeMap<String, String> getMsgList(int listid) throws ServiceExcept{
		TreeMap<String, String> msgs=(TreeMap<String, String>)this.getServletContext().getAttribute("MsgList_"+listid);
		if (msgs==null)
		{
			msgs=selects.getMsgList(listid);
			if (msgs!=null)
			{			
			  this.getServletContext().setAttribute("MsgList_"+listid,msgs);
			}else{
				throw PutIntErr1();
			}
		}
		return msgs;
	}
	//=====================================================
	@SuppressWarnings("unchecked")
	public TreeMap<Integer, String> getINETERRS() throws ServiceExcept
	{
		TreeMap<Integer, String> ineterrs=(TreeMap<Integer, String>)this.getServletContext().getAttribute("T_INET_ERROR");
		if (ineterrs==null)
		{
			ineterrs=selects.getINETERRS();
			if (ineterrs!=null)
			{			
			  this.getServletContext().setAttribute("T_INET_ERROR",ineterrs);
			}else{
				throw PutIntErr1();
			}
		}
		return ineterrs;
	}
	//=========================================================
	@Override
	public BronState getBronState() throws ServiceExcept {
		abon_profile aa=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (aa!=null)
		{
			return tools.GetBronState(aa.usr_id);	
		};
		throw PutSTOut("GetBronState error");
	}
	//=========================================================
	@Override
	public BronSetUnSetResult BronSet(int mon) throws ServiceExcept{
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				BronSetUnSetResult bsusr=aa.BronSet(ap.usr_id,mon);
				bsusr.FillDescr(getINETERRS(),1);
				return bsusr;
			};
			throw PutSTOut("BronSet error2");
		};
		throw PutSTOut("BronSet error");
	}
	//=========================================================
	@Override
	public BronSetUnSetResult BronUnSet() throws ServiceExcept{
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				BronSetUnSetResult bsusr=aa.BronUnSet(ap.usr_id);
				bsusr.FillDescr(getINETERRS(),0);
				return bsusr;
			};
			throw PutSTOut("BronUnSet error");
		};
		throw PutSTOut("BronUnSet error");
	}
	//=========================================================
	@Override
	public String CheckADSLTech(String phone) throws ServiceExcept {
		return tools.CheckADSLTech(phone);
	}
	//=========================================================
	@Override
	public String ChangeRadiusPwd(String login, String opwd, String npwd,
			String npwd2) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.ChangeRadiusPwd(ap.usr_id,login,opwd,npwd,npwd2);
			};
			throw PutSTOut("ChangeRadiusPwd error 2");
		};
		throw PutSTOut("ChangeRadiusPwd error");
	}
	//=========================================================	
	@SuppressWarnings("unchecked")
	@Override
	public TreeMap<Integer, wrkitem> getWRKList(int listid) throws ServiceExcept {
		TreeMap<Integer, wrkitem> msgs=(TreeMap<Integer, wrkitem>)this.getServletContext().getAttribute("WrkList_"+listid);
		if (msgs==null)
		{
			msgs=selects.getWRKList(listid);
			if (msgs!=null)
			{			
			  this.getServletContext().setAttribute("WrkList_"+listid,msgs);
			}else{
				throw PutIntErr1();
			}
		}
		return msgs;
	}
	//=========================================================	
	@Override
	public String ChangePhoneTP(String devid, int tp) throws ServiceExcept{
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.ChangePhoneTP(ap.usr_id,devid,tp);
			};
			throw PutSTOut("ChangePhoneTP error 2");
		};
		throw PutSTOut("ChangePhoneTP error");
	}
	//=========================================================
	@Override
	public String ChangeInetTP(String connID, int tp) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.ChangeInetTP(ap.usr_id,connID,tp);
			};
			throw PutSTOut("ChangeInetTP error 2");
		};
		throw PutSTOut("ChangeInetTP error");
	}
	//=========================================================
	@Override
	public Vector<DVOclipart> DVOChange(String devid, String DVOLIST)
			throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.DVOChange(ap.usr_id, devid, DVOLIST);
			};
			throw PutSTOut("DVOChange error 2");
		};
		throw PutSTOut("DVOChange error");
	}
	//=========================================================
	@Override
	public String InetPB(int summ, int days) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.InetPB(ap.usr_id, summ, days);	
			};
			throw PutSTOut("InetPB error 2");
		};
		throw PutSTOut("InetPB error");
	}
	//=========================================================
	@Override
	public update_contacts Update_contacts(String email,String Phone,String Phonevoice,
			Integer e_on,Integer s_on,Integer p_on,Integer af_on,Date bday) throws ServiceExcept
	{
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				java.sql.Date nd=null;
				if (bday!=null) nd=new java.sql.Date(bday.getTime());
				
				return aa.Update_contacts(ap.usr_id, email, Phone,Phonevoice,e_on,s_on,p_on,af_on,nd);
			};
			throw PutSTOut("Update_contacts error2");
		};
		throw PutSTOut("Update_contacts error");	
	}
	//=========================================================
	@Override
	public String getNewsBody(int id) throws ServiceExcept {
		return selects.getNewsBody(id);
	}
	//=========================================================
	@Override
	public Vector<News> getNewsTitles() throws ServiceExcept {
		return selects.getNewsTitles();
	}
	//=========================================================
	@Override
	public String SendQuestion(String theme, String body,int svc,int typ) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				aa.SendQuestion(theme, body,ap.usr_id,svc,typ);
			};
		};		
		return "";
	}
	//=========================================================
	@Override
	public Vector<Jrnl_rec> getJournal() throws ServiceExcept {
		abon_profile aa=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (aa!=null)
		{
			return selects.getJournal(aa.usr_id);	
		};
		throw PutSTOut("getJournal error");	
	}
	//=========================================================
	public	float getBalIP() throws ServiceExcept {
		return tools.getBalIP(this.getThreadLocalRequest().getRemoteAddr());
	}
	//=========================================================
	@Override
	public String Change09State(String devid, boolean state)
			throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.Change09State(ap.usr_id,devid,state);
			};
			throw PutSTOut("getJournal error 2");
		};
		throw PutSTOut("getJournal error");	
	}
	//=========================================================
	@Override
	public UserPeriodBonusInfo getUserPeriodBonusInfo(Long BID) throws ServiceExcept {
		abon_profile aa=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (aa!=null)
		{
			return selects.getUserPeriodBonusInfo(aa.usr_id,BID);	
		};
		throw PutSTOut("getUserPeriodBonusInfo error");	
	}
	//=========================================================	
	@Override
	public String UseBonuses(int count) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.UseBonuses(ap.usr_id,count);	
			};
			throw PutSTOut("UseBonuses error2");
		};
		throw PutSTOut("UseBonuses error");	
	}
	//=========================================================
	/*
	@Override
	public Vector<UserBonusPeriodListItem> getUserBonusPeriodList()
			throws ServiceExcept {
		abon_profile aa=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (aa!=null)
		{
			return selects.getUserBonusPeriodList(aa.usr_id);	
		};
		throw PutSTOut("getUserBonusPeriodList error");	
	}/**/
	//=========================================================	
	@Override
	public Vector<ADM_TRACE_ITEM> getADMTrace() throws ServiceExcept {
		return selects.getADMTrace();
	}
	//=========================================================	
	@Override
	public Vector<wrkitem> getModelPlans(String Model) throws ServiceExcept {
		return selects.getModelPlans(Model);
	}
	//=========================================================	
	@Override
	public void update_einf(int bo, int schet) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				aa.update_einf(ap.usr_id,bo,schet);
				return;
			};
			throw PutSTOut("update_einf error2");
		};
		throw PutSTOut("update_einf error");		
	}
	//=========================================================	
	@Override
	public Vector<search_userData> search_user(String account, String name,
			String devid) throws ServiceExcept {
		return selects.search_user(account,name,devid);
	}
	//=========================================================	
	@Override
	public abon_profile SelectUser(Long uid) throws ServiceExcept {
		TreeMap<String, String> msgs=(TreeMap<String, String>)getMsgList(2);
		abon_profile a= abon_profileS.SelectUser(uid,msgs);
		if (a!=null)
		{
			this.getThreadLocalRequest().getSession().setAttribute("abon_profile", a);
			return a;
		}
		throw PutSTOut("auth_wr_xz");
	}
	//=========================================================	
	@Override
	public String authOper(String Login, String pwd) throws ServiceExcept
	{
		Connection conn=CP_DB.getPullRAWConnection("ENGINE_DB_CONN_PULL",Login,pwd);
		if (conn==null)
		{
			throw new ServiceExcept("");
		};
		abon_action aa= new abon_action();
		aa.conn=conn;
		aa.isOper=1;
		this.getThreadLocalRequest().getSession().setAttribute("abon_action", aa);		
		return "Ok";
	}
	//=========================================================		
	@Override
	public abon_corp_profile Update_CORPcontacts(String f1, String f2,
			String f3, String d1, String d2, String d3, String e1, String e2,
			String e3, String p1, String p2, String p3, String x1, String x2,
			String x3, String m1, String m2, String m3, Date bday)
			throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				java.sql.Date nd=null;
				if (bday!=null) nd=new java.sql.Date(bday.getTime());
				return aa.Update_CORPcontacts(ap.usr_id,f1,f2,f3,d1,d2,d3,e1,e2,e3,p1,p2,p3,x1,x2,x3,m1,m2,m3,nd);
			};
			throw PutSTOut("Update_CORPcontacts error2");
		};
		throw PutSTOut("Update_CORPcontacts error");	
	}
	//=========================================================	
	@Override
	public void update_CORP_einf(String email, int schet) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				aa.update_CORP_einf(ap.usr_id,email,schet);
				return;
			};
			throw PutSTOut("update_CORP_einf error2");
		};
		throw PutSTOut("update_CORP_einf error");		
	}
	//=========================================================	
	@Override
	public void corpreport(String period, int a1, int a2, int s1, int s2)
			throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				aa.corpreport(ap.usr_id,period,a1,a2,s1,s2);
				return;
			};
			throw PutSTOut("corpreport error2");
		};
		throw PutSTOut("corpreport error");		
	}
	//=========================================================
	@Override
	public String SendCorpQuestion(String theme, String body, int typ)
			throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				aa.SendCorpQuestion(theme, body,ap.usr_id,typ);
			};
		};		
		return "";
	}
	//=========================================================	
	@Override
	public String LK_PWD_CHANGE(String login, String opwd, String npwd,
			String npwd2) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.LK_PWD_CHANGE(ap.usr_id,login,opwd,npwd,npwd2);
			};
			throw PutSTOut("LK_PWD_CHANGE error2");
		};
		throw PutSTOut("LK_PWD_CHANGE error");		
	}
	//=========================================================	
	@Override
	public Vector<rpc_cmn_sprvk> getcmn_sprvk(Long bid) throws ServiceExcept {
		abon_profile aa=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (aa!=null)
		{
			return selects.getcmn_sprvk(aa.usr_id,bid);	
		};
		throw PutSTOut("getcmn_sprvk error");	
	}
	//=========================================================
	@Override
	public Vector<RPC_BillPeriod> getBillPeriods() throws ServiceExcept {
		abon_profile aa=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (aa!=null)
		{
			return selects.getBillPeriods(aa.usr_id);	
		};
		throw PutSTOut("getBillPeriods error");	
	}
	//=========================================================
	@Override
	public period_calls_lg_data get_period_calls_lg_data(Long bid)throws ServiceExcept
	{
		abon_profile aa=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (aa!=null)
		{
			return selects.get_period_calls_lg_data(aa.usr_id,bid);	
		};
		throw PutSTOut("get_period_calls_lg_data error");	
	}
	//=========================================================
	@Override
	public Vector<rpc_raz_post_usl> get_raz_post_usl(Long bid)throws ServiceExcept
	{
		abon_profile aa=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (aa!=null)
		{
			return selects.get_raz_post_usl(aa.usr_id,bid);	
		};
		throw PutSTOut("get_raz_post_usl error");	
	}
	//=========================================================
	@Override
	public String Recovery_LKPass(String account, String email)throws ServiceExcept
	{
			return tools.Recovery_LKPass(account,email);
	}
//=========================================================
	@Override
	public String block_apus_reset(Long devId) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.block_apus_reset(ap.usr_id,devId);
			};
			throw PutSTOut("block_apus_reset error2");
		};
		throw PutSTOut("block_apus_reset error");	
	}
	//=========================================================
	@Override
	public String block_apus_set(Long devId, int len) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.block_apus_set(ap.usr_id,devId,len);
			};
			throw PutSTOut("block_apus_set error2");
		};
		throw PutSTOut("block_apus_set error");	
	}
	//=========================================================
	@Override
	public String block_mgmn_reset(Long devId) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.block_mgmn_reset(ap.usr_id,devId);
			};
			throw PutSTOut("block_mgmn_reset error2");
		};
		throw PutSTOut("block_mgmn_reset error");	
	}
	//=========================================================
	@Override
	public String block_mgmn_set(Long devId, int len) throws ServiceExcept {
		abon_profile ap=(abon_profile)this.getThreadLocalRequest().getSession().getAttribute("abon_profile");
		if (ap!=null)
		{
			abon_action aa=(abon_action)this.getThreadLocalRequest().getSession().getAttribute("abon_action");
			if (ap!=null)
			{
				return aa.block_mgmn_set(ap.usr_id,devId,len);
			};
			throw PutSTOut("block_mgmn_set error2");
		};
		throw PutSTOut("block_mgmn_set error");	
	}
	//=========================================================
	@Override
	public rpc_tp_calc get_tp_calc() throws ServiceExcept {
		return selects.get_tp_calc();
	}
}
//=========================================================
 