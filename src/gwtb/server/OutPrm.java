package gwtb.server;

import java.sql.CallableStatement;
import java.sql.SQLException;

import gwtb.client.rpcdata.ServiceExcept;

public class OutPrm {
	public int type;
	public static final int STR=1;
	public static final int NUM=2;
	public OutPrm(int i)
	{
		type=i;
	}
	public void init(CallableStatement st, int idx) throws SQLException, ServiceExcept
	{
		switch (type)
		{
			case 1:st.registerOutParameter(idx, java.sql.Types.VARCHAR);break;
			case 2:st.registerOutParameter(idx, java.sql.Types.NUMERIC);break;
			default: throw new ServiceExcept("Wrong OutPrm type");
		}
	}
}
