package gwtb.server;

import gwtb.client.rpcdata.BronState;
import gwtb.client.rpcdata.ServiceExcept;
import gwtb.client.rpcdata.abon_profile;
import gwtb.core.PULLS.CP_DB;

import java.sql.Connection;

public class tools {

	// --------------------------------------------------------------------------------
	  static public boolean isbillPeriod(int bid) throws ServiceExcept
	  {
	    Boolean bsr=false;
		Connection conn;
		if ((conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL"))!=null)
		{
			bsr=CH.BOOL(conn, SQL.isbillPeriod, "isbillPeriod",true,new OutPrm(1),bid);
			CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
		}    
	    return bsr;	    
	  }
	//--------------------------------------------------------------------------------

	// --------------------------------------------------------------------------------
	  static public Long try_auth(String l, String p, Connection conn) throws ServiceExcept
	  {
		return CH.LONG(conn, SQL.try_auth, "try_auth",false,new OutPrm(1),l,p);
	  }
	// --------------------------------------------------------------------------------
	  public static void feelURLS(final abon_profile d) throws ServiceExcept
	  {
			Connection conn;
			if ((conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL"))!=null)
			{
		        new CHH<Void>(){public void call() throws Exception{
		            d.url_lc_asr = st.getString(3);
		            d.url_lc_sip = st.getString(4);
		        }}.RunC(conn, SQL.feelURLS, "feelURLS",false,d.usr_id, d.usr_id_sip,new OutPrm(1),new OutPrm(1));
				CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
			}
	  }
	// --------------------------------------------------------------------------------
	  static public float getBalIP(String IP) throws ServiceExcept
	  {
	    Float bsr=0f;
		Connection conn;
		if ((conn=CP_DB.getConnection("ENGINE_SIPDB_CONN_PULL"))!=null)
		{
			bsr=CH.FLOAT(conn, SQL.getBalIP, "getBalIP",false,new OutPrm(2),IP);
			CP_DB.freeConnection("ENGINE_SIPDB_CONN_PULL", conn);
		}    
	    return bsr;
	  }
	//--------------------------------------------------------------------------------
	  static public String CheckADSLTech  (String phone) throws ServiceExcept 
	  {
			String bsr=null;
			Connection conn;
			if ((conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL"))!=null)
			{
				bsr=CH.STRING(conn, SQL.CheckADSLTech, "CheckADSLTech",true,new OutPrm(1),phone);
				CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
			}    
	    	if (bsr==null){throw new ServiceExcept("Internal error: CheckADSLTech error");};
	    	return bsr;
	    }
	//--------------------------------------------------------------------------------
	  public static void chk_data(String uid) throws ServiceExcept 
		{
			Connection conn;
			if ((conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL"))!=null)
			{
				CH.VOID(conn, SQL.chk_data, "chk_data",true,uid);
				CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
			}
		}
	//--------------------------------------------------------------------------------
	  static public BronState GetBronState(Long usr_id) throws ServiceExcept
	  {
		Integer bs=0;
		Connection conn;
		if ((conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL"))!=null)
		{
			bs=CH.INT(conn, SQL.GetBronState, "GetBronState",false,new OutPrm(2),usr_id);
			CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
		}    
        if (bs == null)
	      throw new ServiceExcept("Internal error: GetBronState error getting state");
	    return new BronState(bs);
	  }
	// --------------------------------------------------------------------------------	  
	public static void registerAuth(long usrid, String lgn) throws ServiceExcept{
		Connection conn;
		if ((conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL"))!=null)
		{
			CH.VOID(conn, SQL.registerAuth, "registerAuth",true,usrid,lgn);
			CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
		}    		
	}
	// --------------------------------------------------------------------------------	  

	public static String Recovery_LKPass(String account, String email) throws ServiceExcept {
		String bsr=null;
		Connection conn;
		if ((conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL"))!=null)
		{
			bsr=CH.STRING(conn, SQL.Recovery_LKPass, "Recovery_LKPass",true,new OutPrm(1),account,email);
			CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
		}    
		if (bsr==null){throw new ServiceExcept("Internal error: Recovery_LKPass error");};
		return bsr;

	}
	

	
}
