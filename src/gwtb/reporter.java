package gwtb;
import gwtb.client.rpcdata.abon_profile;
import gwtb.core.PULLS.CP_DB;
import gwtb.server.template.Builder;
import gwtb.server.template.Node;
import gwtb.server.template.Parcer;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.Connection;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//--------------------------------------------------------------------
public class reporter extends HttpServlet
{
	//----------------------------------------------------------------
	private static final long serialVersionUID = 1L;
	@Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
	//----------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private TreeMap<String,Object> getParams(String Pref,HttpServletRequest rt)
	{
		TreeMap<String,Object> ret=new TreeMap<String,Object>(); 
		Object[] ar=rt.getParameterMap().entrySet().toArray();
		int sz=ar.length;
		for (int i=0;i<sz;++i)
		{
			
			Map.Entry<String, String> s=(Map.Entry<String, String>)ar[i];
			ret.put(Pref+s.getKey(),rt.getParameter(s.getKey()));
		};
		return ret;
	}
	//----------------------------------------------------------------
	private Node GetTemplate(String Name) throws Exception
	{
		Node n=null;
		if(Name!=null)
		{
			Name=Name.replace(".","");
			ServletContext sc = this.getServletContext();
			if(sc!=null)
			{
				n=(Node)sc.getAttribute("REPORT."+Name);
				if(n==null)
				{
					StringBuffer src= new StringBuffer();
					URL url=sc.getResource("/reports/"+Name+".htm");System.out.println("REPORT PATH: "+url.toString());
					InputStream is=sc.getResourceAsStream("/reports/"+Name+".htm");
					InputStreamReader isr = new InputStreamReader(is, "windows-1251");  
					int i;  
					while ((i = isr.read()) != -1)	src.append((char) i);
					isr.close();
					Parcer c= new Parcer();
					n=c.ParceTemplate(src.toString());
					if(n!=null)			this.getServletContext().setAttribute("REPORT."+Name, n);
				};
			};
		};
		return n;
	};
	//----------------------------------------------------------------
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response)
    {
    	abon_profile ap=(abon_profile) request.getSession().getAttribute("abon_profile");
    	//response.addHeader("Content-type", );
    	response.setContentType("text/html; charset=windows-1251");
    	response.setCharacterEncoding("windows-1251");
		StringBuffer sb= new StringBuffer(); 
		
		Long usr_id=new Long(0);
    	try {
    		OutputStreamWriter osw=new OutputStreamWriter(response.getOutputStream(), "windows-1251");
    		
    		if(request.getParameter("RAW")!=null)
    			if(request.getParameter("RAW").equalsIgnoreCase("yes"))
    			{
    				usr_id= Long.parseLong(request.getParameter("uid"));
    			}
    		if(usr_id==0)
    			if(ap!=null)
    			{
    				usr_id=ap.usr_id;
    			};
    		if(usr_id!=0)
    		{
        		TreeMap<String,Object> IN= getParams("IN.",request);
        		if(IN!=null)
        		{
        			IN.remove("IN.uid");// if try hack, but maybe not needs, checkit..
        			IN.put("IN.uid",usr_id);
        			


        			String s=request.getRequestURL().toString();
        			s=s.substring(0,s.lastIndexOf("/"));
        			IN.put("IN.path", s);        			
        			
        			Node n=GetTemplate(request.getParameter("repname"));
        			if(n!=null)
        			{
        				//Node.Debug_prn(sb,n,true);
        				Connection conn;
        				if ((conn=CP_DB.getConnection("ENGINE_DB_CONN_PULL"))!=null)
        				{
        					try {
								Builder.run(n,IN,sb,conn,false);

							} catch (Exception e) {
								osw.write("Report building error: "+e.getMessage());
								e.printStackTrace();
							}
        					CP_DB.freeConnection("ENGINE_DB_CONN_PULL", conn);
        				}else
        					osw.write("DB connection error.");
        			}else	osw.write("unknown report or error.");
        		}else 		osw.write("param report error.");
    		}else osw.write("your session time expired, try re login.");
			osw.write(sb.toString());
			osw.flush();    		
		}catch (Exception e)
		{
			e.printStackTrace();
		}
    }      
	//----------------------------------------------------------------
}
