package gwtb;



import gwtb.client.rpcdata.ServiceExcept;
import gwtb.core.PULLS.CP_DB;
import gwtb.server.abon_action;
import gwtb.server.selects;
import javax.servlet.ServletContext;
	import javax.servlet.ServletContextAttributeEvent;
	import javax.servlet.ServletContextAttributeListener;
	import javax.servlet.ServletContextEvent;
	import javax.servlet.ServletContextListener;
	import javax.servlet.http.HttpSession;
	import javax.servlet.http.HttpSessionAttributeListener;
	import javax.servlet.http.HttpSessionBindingEvent;
	import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

	public class Listener
	  implements ServletContextListener, ServletContextAttributeListener, 
	             HttpSessionListener, HttpSessionAttributeListener
	{
	  private ServletContext context = null;
	  @SuppressWarnings("unused")
	private String name = null;
	  @SuppressWarnings("unused")
	private Object value = null;
	  @SuppressWarnings("unused")
	private HttpSession session = null;

	  public void contextInitialized(ServletContextEvent event)
	  {
		  if (context==null)
		  {
			  context = event.getServletContext();
			  if (context!=null)
			  {
//				  Initializator.Initializate_context(context);
					System.err.println("Initializator::Initializate_context start");
					CP_DB.initPull(	context.getInitParameter("ENGINE_DB_PULLNAME"),
							context.getInitParameter("ENGINE_DB_URL"),
							context.getInitParameter("ENGINE_DB_USER_NAME"),
							context.getInitParameter("ENGINE_DB_PWD"));
					CP_DB.initPull(	context.getInitParameter("ENGINE_SIPDB_PULLNAME"),
							context.getInitParameter("ENGINE_SIPDB_URL"),
							context.getInitParameter("ENGINE_SIPDB_USER_NAME"),
							context.getInitParameter("ENGINE_SIPDB_PWD"));					
					try {
						context.setAttribute("MSGS_AUTH",selects.getMsgList(2));
						context.setAttribute("MSGS_AUTH2",selects.getMsgList(1));
					//	context.setAttribute("DVO_LIST",tools.getDVOList());
					} catch (ServiceExcept e) {
						selects.Log("public void contextInitialized(ServletContextEvent event):"+e.getLocalizedMessage());
						e.printStackTrace();
					}
					System.err.println("Initializator::Initializate_context done");
				  //context.setAttribute(name, object)
			  }
		  }
	  }

	  public void contextDestroyed(ServletContextEvent event)
	  {
	    context = event.getServletContext();
	  }

	  public void attributeAdded(ServletContextAttributeEvent event)
	  {
	    name  = event.getName();
	    value = event.getValue();
	  }

	  public void attributeRemoved(ServletContextAttributeEvent event)
	  {
	    name  = event.getName();
	    value = event.getValue();
	  }

	  public void attributeReplaced(ServletContextAttributeEvent event)
	  {
	    name  = event.getName();
	    value = event.getValue();
	  }

	  public void sessionCreated(HttpSessionEvent event)
	  {
	    session  = event.getSession();
	  }

	  public void sessionDestroyed(HttpSessionEvent event)
	  {
		  abon_action aa=(abon_action) event.getSession().getAttribute("abon_action");
		  if(aa!=null)aa.killConn();
	  }

	  public void attributeAdded(HttpSessionBindingEvent event)
	  {
	    name  = event.getName();
	    session = event.getSession();
	  }

	  /**
	   * 
	   * @param event
	   */
	  public void attributeRemoved(HttpSessionBindingEvent event)
	  {
	    String name  = event.getName();
	    if (name!=null)
	    	if (name.equalsIgnoreCase("abon_action"))
	    	{
	  		  abon_action aa=(abon_action) event.getSession().getAttribute("abon_action");
			  if(aa!=null)aa.killConn();
	    	}
	  }

	  public void attributeReplaced(HttpSessionBindingEvent event)
	  {
	    name  = event.getName();
	    session = event.getSession();
	  }
	}
